package com.cloud.apis.attendance.pojo.vo;

import com.cloud.apis.user.pojo.vo.UserInfoVo;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/7
 */
@Data
public class FaceVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long userId;
    private UserInfoVo user;
    private String model;
    private LocalDateTime createTime;
}

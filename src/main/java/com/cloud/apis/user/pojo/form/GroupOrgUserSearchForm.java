package com.cloud.apis.user.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/1
 */
@Data
public class GroupOrgUserSearchForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long userId;
    private Long[] orgIds;
    @NotNull(message = "搜索名称不能为空")
    private String searchName;
    private Integer pageNum;
}

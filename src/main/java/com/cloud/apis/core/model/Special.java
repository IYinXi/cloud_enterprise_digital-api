package com.cloud.apis.core.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 特殊考勤日程表
 * attendance_special
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("attendance_special")
public class Special implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 考勤特殊日
     */
    private Date data;

    /**
     * 特殊日说明
     */
    private String mark;

    /**
     * （true为特殊工作日，false为节假日）
     */
    @TableField("is_work")
    private Boolean isWork;

    /**
     * 是否启用
     */
    @TableField("is_enable")
    private Boolean isEnable;

    /**
     * 创建日期
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    private static final long serialVersionUID = 1L;
}
package com.cloud.apis.oss.utils;

import cn.hutool.core.util.IdUtil;
import com.cloud.apis.oss.consts.CosConsts;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.model.StorageClass;
import com.qcloud.cos.region.Region;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * COS工具类
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/24
 */
@Slf4j
@Component
public class CosUtil {
    /**
     * 腾讯云appID
     */
    @Value("${tencent.cloud.appId}")
    private String appId;

    /**
     * 腾讯云SecretId
     */
    @Value("${tencent.cloud.secretId}")
    private String secretId;

    /**
     * 腾讯云SecretKey
     */
    @Value("${tencent.cloud.secretKey}")
    private String secretKey;

    /**
     * 腾讯云存储桶region
     */
    @Value("${tencent.cloud.region}")
    private String region;

    /**
     * 腾讯云存储桶id
     */
    @Value("${tencent.cloud.bucket}")
    private String bucket;

    public CosUtil() {
        log.info("【CosUtil】");
    }

    /**
     * 获取COS客户端
     * @return COSClient
     */
    private COSClient getCosClient() {
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        ClientConfig clientConfig = new ClientConfig(new Region(region));
        clientConfig.setHttpProtocol(HttpProtocol.https);
        COSClient cosClient = new COSClient(cred, clientConfig);
        return cosClient;
    }

    /**
     * 上传文件
     * @param files
     * @param type
     * @return HashMap
     * @throws IOException
     */
    public HashMap uploadFile(MultipartFile[] files, CosConsts type) throws IOException {
        HashMap map=new HashMap();
        ArrayList<String> urls = new ArrayList<>();
        ArrayList<String> pathes = new ArrayList<>();
        for (MultipartFile file : files) {
            String path = null; //文件将要存放的相对路径
            String fileName = file.getOriginalFilename();
            //根据传入的type判断放入哪个文件夹
            //用户的头像路径
            if (type == CosConsts.AVATER) {
                path = "/user/avater/" + IdUtil.simpleUUID() + fileName.substring(fileName.lastIndexOf("."));
            }
            //用户的转岗证明材料
            if(type == CosConsts.TRANSFER){
                path = "/transfer/" + IdUtil.simpleUUID() + fileName.substring(fileName.lastIndexOf("."));
            }
            //用户的离职证明
            if(type==CosConsts.OUT){
                path = "/out/" + IdUtil.simpleUUID() + fileName.substring(fileName.lastIndexOf("."));
            }
            //用户的报销证明
            if(type==CosConsts.REIMURSEMENT){
                path = "/reimbursement/" + IdUtil.simpleUUID() + fileName.substring(fileName.lastIndexOf("."));
            }
            //用户的补贴证明
            if(type==CosConsts.SUBSIDY){
                path = "/subsidy/" + IdUtil.simpleUUID() + fileName.substring(fileName.lastIndexOf("."));
            }
            //用户请假证明
            if(type==CosConsts.LEAVE){
                path = "/leave/" + IdUtil.simpleUUID() + fileName.substring(fileName.lastIndexOf("."));
            }
            if(type == CosConsts.TRAVLE){
                path = "/travle/" + IdUtil.simpleUUID() + fileName.substring(fileName.lastIndexOf("."));
            }
            //元数据信息
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentLength(file.getSize());
            meta.setContentEncoding("UTF-8");
            meta.setContentType(file.getContentType());

            //创建请求
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, path, file.getInputStream(), meta);
            // 设置存储类型, 默认是标准(Standard), 低频(Standard_IA)
            putObjectRequest.setStorageClass(StorageClass.Standard);

            //获取Client对象
            COSClient client = getCosClient();
            //发出请求，上传文件
            PutObjectResult putObjectResult = client.putObject(putObjectRequest);
            //上传结束后关闭Client
            client.shutdown();
            //刚刚上传文件的外网访问地址
            //map.put("url","https://" + bucket + ".cos." + region + ".myqcloud.com" + path);
            urls.add("https://" + bucket + ".cos." + region + ".myqcloud.com" + path);
            //文件的相对路径
            //map.put("path",path);
            pathes.add(path);
        }
        map.put("urls",urls);
        map.put("pathes",pathes);
        return map;
    }

    /**
     * 删除文件
     * @param pathes
     */
    public void deleteFile(String[] pathes) {
        COSClient client = getCosClient();
        for (String path : pathes) {
            client.deleteObject(bucket, path);
        }
        client.shutdown();
    }
}

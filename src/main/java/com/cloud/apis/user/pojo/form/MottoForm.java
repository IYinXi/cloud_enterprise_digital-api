package com.cloud.apis.user.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 座右铭表单
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/22
 */
@Data
public class MottoForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "座右铭不能为空")
    @Range(min = 0, max = 20, message = "座右铭太长")
    @ApiModelProperty(value = "座右铭", required = true)
    private String motto;
}

# ”云上智企“智慧化企业系统-后端服务器

云上企业数字化智慧系统是一款用于现代化企业管理的企业管理系统，属于企业信息化系统中的一部分，该系统主要用于企业的内部管理使用对象和管理对象都是企业的内部员工及其操作的产物，企业可以拉取并且进行相关的部署从而实现企业内部系统的搭建，由于本项目属于开源项目，因此可以更好的受大众监督并且无需担心安全问题。本系统采取一种弹性的组织架构和自定义的用户角色划分，通过内置必要数据方便用户的操作和理解，用户可以根据自身的组织架构自定义并且可以根据自身的角色划分自定义角色并且分派权限使用系统。

![image-20240519124818249](assets/image-20240519124818249.png)

## 项目简介

### 两大核心支柱

#### 一种通用的企业的组织架构设计

根据上述相关的核心点，接下来第一步将探索一种通用的企业组织架构设计。

##### 企业组织架构的通用演化过程

企业的规模会随着企业的不断的壮大而壮大，其本质是企业的组织架构的不断扩展，信息化管理平台的核心点始终是围绕着对企业的组织架构进行管理的，因此一套优秀的管理系统应当能够随着企业的不断扩大而进行应用。所以首先应当了解企业组织架构的通用演化过程。可以分为以下的相关架构。

##### 企业初期架构

企业初期的架构一般是在创立企业之初的架构模式，对于企业创立之初其组织架构是非常简易的，通过对相关材料的阅览和了解，企业初期的通用架构如下：

<img src="assets/image-20240409012017676.png" alt="image-20240409012017676" style="zoom:67%;" />

##### 企业发展期架构

随着企业的不断发展和扩大，企业规模来到了发展期的，相对于初期来说，达到发展期普遍需要2-3年的时间，在发展期向之后的稳定期的跃迁过程中停留的时间最长，甚至大部分的企业到发展期之后会有很长时间的停留，因此发展期的架构在当下是十分广泛的一种组织架构的设置，占当下企业组织架构的绝大部分。

<img src="assets/image-20240409012421376.png" alt="image-20240409012421376" style="zoom:67%;" />

##### 企业稳定期架构

随着企业规模的不断扩大，这个时候企业来到了企业稳定期，这样的企业已经形成了一定的规模，达到稳定期普遍需要5-10年左右的时间，达到稳定期的企业基本上已经成形，大部分这样的企业后续不会有太大的变化，同样也是当下主流的企业组织架构，但是其基数并没有发展期的企业组织架构的数量多，当前发展期的企业的组织架构依然是多数。

<img src="assets/image-20240409013424822.png" alt="image-20240409013424822" style="zoom:67%;" />

##### 企业腾飞期架构

企业腾飞期的架构说明当前企业的规模已经非常巨大了，这样的企业旗下有很多的公司，公司下又有很多的子公司，甚至子公司下还有子公司，这样的企业已经形成一定的地位和规模，也就是能够排名在前的超大型企业，其组织架构的描述通常如下。

<img src="assets/image-20240409014011470.png" alt="image-20240409014011470" style="zoom:67%;" />

#### 通用组织架构的设计实现

为了能够适应企业的组织架构的变化来灵活的实现企业的管理系统的后期发展，自定义企业的组织架构变得十分的重要，根据上述的企业组织架构的通用演化过程来看实际上企业的组织架构就是一颗树状的数据结构，从根开始不断的向下发展演化，因此对于企业的组织架构来说实际上可以按照数的结构进行设计来实行对企业的组织架构的自定义和扩展从而实现企业管理系统的弹性发展。

那么对于企业的组织架构其根本将存储于持久化的数据库中，因此数据的设计就显得十分的重要和关键。目前基于关系型数据库的存储方式依然是主流并且具有数学验证的方式，因此，对于持久化数据的存储将无可厚非的采用基于关系型理论的数据库，基于关系型理论的数据库其核心是对数据表结构的设计，因此，要想实现通用的组织架构的设计，其数据表的设计能够实现树状结构，而对于这样的表设计需求实际上是非常简单的，只需要添加一个父级id的字段即可实现。

最后的实现该通用组织架构设计的结果是对于数据库的表的设计，这里采用的关系型数据库是MySQL，相关的表结构和建表语句如下：

tb_organization:

![image-20240409015235727](assets/image-20240409015235727.png)

```SQL
CREATE TABLE `tb_organization` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '组织架构id',
  `parent_id` bigint DEFAULT NULL COMMENT '父级id',
  `name` varchar(255) NOT NULL COMMENT '组织名称',
  `abbreviation` varchar(255) NOT NULL COMMENT '简称',
  `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用(0,否;1,是)',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `type_id` bigint NOT NULL COMMENT '组织类型id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tb_organization_unique` (`abbreviation`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb3 COMMENT='组织架构表';
```

### 企业组织架构下的角色划分

有了企业的通用的组织架构的划分，此时将完成第二个核心点也就是明确使用系统的使用者，对于使用者来说其关键是对使用者的权限进行规划，其本质是通过相关的权限实现对企业管理系统的相关功能的控制，因此对于这一点来说可以按照RBAC权限模型来进行具体的划分

#### RBAC权限模型

采用RBAC权限设计进行设计：用户-角色-权限，一个用户可以拥有多个角色，一个角色可以分配到多个权限。用户到角色到权限都是多对多的关系，所以设计的RBAC权限模型是：

- 用户表
- 用户-角色表
- 角色表
- 角色-权限表
- 权限表

相关的ER模型如下：
![RBAC](assets/RBAC.png)

权限就是可以操作API接口的能力，一个角色赋予多个权限就相当于这个角色下可以操作一组API接口的能力，一个用户对应多个角色，那么这个用户可以操作对应多组的API接口的能力，所以角色应当首先设定，角色的另一层含义就是参与系统中的角色。

上述是对RBAC权限模型的基本理论阐述，在系统中要实现RBAC实际上就是实现系统的认证与授权的过程，认证可以理解为登录时输入密码和账户名后能够登录到系统中，授权可以理解为登录到系统中能够使用系统的权限也就是功能有那些这是基于自身的角色来规划的。

#### 角色规划

对于一个系统来说，对角色的划分实际上可以考虑两类，第一类是核心角色或者说是基础角色，这类角色是系统中必须存在的，第二类是扩展角色或者说是自定义角色，这类角色是可以进行自定义并且根据需求分配相关的权限。

##### 核心角色

首先根据本系统的企业的组织架构的数据结构的设计考虑，由于本系统是采用一种弹性的可自定义的企业组织架构设计模式，根据相关的流程，在系统第一次部署并使用的时候应该内置一个系统管理员角色root。该root将定义企业的组织架构，相关自定义角色和权限分配并且创建其他用户的账号。是必不可少的关键角色。所以第一个核心角色是**系统管理员**。

##### 自定义角色

上述根据相关的分析划分出了对应的核心角色，接下来进行相关的分析划分出自定义的角色，对于自定义的角色来说，根据企业自身的需求不同可划分不同的角色实际上角色的划分依然是以企业的组织架构所挂钩的，根据企业的组织架构进行角色的划分从何实现对企业的组织架构的管理。

由于本系统的企业的组织架构是依据一种通用的企业组织架构进行搭建的并且根据相关的调查企业发展期架构以及企业稳定期架构是当前大部分企业的组织架构模式，按照此模式实际上也可划分出一类通用的角色等级划分，可以发现的是企业初期架构包含在企业的发展期架构中，企业的发展期架构包含在企业的稳定期架构中，企业的稳定期架构包含在企业的腾飞期架构中，按照最大包含原则和当下企业组织架构的主流模式，可以根据企业的稳定期架构对企业的角色进行划分，实际上企业的这种自定义的角色也可以理解为企业的职级划分，根据上述推断相关的角色划分如下所示：

![image-20240309210409914](assets/image-20240309210409914.png)

本系统将内置上述自定义的角色采用一种推荐模式，当前root可以根据自身的企业需求对上述相关角色进行名称改变比如P1，P2之类的或者是根据组织架构比如发展期下可以为将副总经理改为总经理，是原来公司总经理的角色不可用并且分配相关的权限。

#### 总结

根据上述的角色规划，可以设计为如下的角色：

- 0：系统管理员
- 1：实习生
- 2：普通员工
- 3：部门经理
- 4：中心经理
- 5：副总经理
- 6：总经理
- 7：人事
- 8：行政
- 9：财务

#### RBAC模型的设计与实现

上述的角色划分将基于RBAC模型进行设计因此相关的数据库表结构也就是该模块的数据结构的设计如下：

1,tb_user:

关于tb_user的表结构的设计还需要考虑到和其他表的关联设计，在本结构中其关键的内容是主键id。

2,tb_role:

![image-20240409115912121](assets/image-20240409115912121.png)

```sql
CREATE TABLE `tb_role` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `name` varchar(255) NOT NULL COMMENT '角色名称',
  `abbreviation` varchar(255) NOT NULL COMMENT '简称',
  `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用(0,否;1,是)',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tb_role_unique` (`abbreviation`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3 COMMENT='角色表(职级表)';
```

3、tb_permission

![image-20240409120032174](assets/image-20240409120032174.png)

```sql
-- cloud_enterprise_digital.tb_permission definition

CREATE TABLE `tb_permission` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `name` varchar(255) NOT NULL COMMENT '权限名称',
  `code` varchar(255) NOT NULL COMMENT '权限代码',
  `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用(0,否;1,是）',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tb_permission_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='权限表';
```

4、tb_user_role

![image-20240409120813188](assets/image-20240409120813188.png)

```sql
-- cloud_enterprise_digital.tb_user_role definition

CREATE TABLE `tb_user_role` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` bigint NOT NULL COMMENT '用户id',
  `role_id` bigint NOT NULL COMMENT '角色id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tb_user_role_unique` (`user_id`,`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='用户-角色表';
```

5、tb_role_permission

![image-20240409120706507](assets/image-20240409120706507.png)

```sql
CREATE TABLE `tb_role_permission` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `role_id` bigint NOT NULL COMMENT '角色id',
  `permission_id` bigint NOT NULL COMMENT '权限id',
  `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tb_role_permission_unique` (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='角色-权限表';
```

### 三端架构

![image-20240409133433868](assets/image-20240409133433868-1716089003550-3.png)

### 服务平台

基于VUE2.0的B/S架构，部署后只要有浏览器即可访问，方便快捷。

版本：Vue2.0

技术栈：

- VueCLi
- ElementUI
- WebSocket
- EventBus
- CSS
- HTML
- JavaScript
- QQMap

开发工具：

- VSCode和IDEA专业版

![image-20240519112925597](assets/image-20240519112925597.png)

![image-20240519112116232](assets/image-20240519112116232.png)

![image-20240519112816418](assets/image-20240519112816418.png)

![image-20240519112855268](assets/image-20240519112855268.png)

### 应用平台

基于微信小程序的移动端，借助于微信的强大的即时通讯和企业微信功能为基础，无需下载便捷化的使用该信息平台提供的功能类似于微信+该平台的功能集合体。

版本：VUE2.0

技术栈：

- Uni-App
- Uni-ui
- WebSocket
- QQMap
- Less
- Scass
- HTML
- JavaScript

<img src="assets/image-20240519112950309.png" alt="image-20240519112950309" style="zoom: 50%;" />

<img src="assets/image-20240519113042929.png" alt="image-20240519113042929" style="zoom: 50%;" />

<img src="assets/image-20240519113117294.png" alt="image-20240519113117294" style="zoom:50%;" />



### 应用服务器

基于SpringBoot系列构建的分层架构服务，可以随时扩展为微服务架构下的分布式项目，由于是内部使用涉及到的人数不多，当然如果是大型企业可以需要采用分布式微服务部署。

版本：

- JDK8以上(JDK17)
- SpringBoot2.7.14

技术栈：

```xml
 		<!-- SpringMVC框架整合 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!-- SpringTest框架整合 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
        </dependency>
        <!-- SpringValidation框架整合 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-validation</artifactId>
        </dependency>
        <!-- Lombok框架整合 -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
        <!-- MySQL数据库驱动 -->
        <dependency>
            <groupId>com.mysql</groupId>
            <artifactId>mysql-connector-j</artifactId>
            <scope>runtime</scope>
        </dependency>
        <!-- Druid框架整合 -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>1.2.16</version>
        </dependency>
        <!-- MyBatis框架整合 -->
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>2.3.1</version>
        </dependency>
        <!-- MyBatisPlus框架整合 -->
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.4.2</version>
        </dependency>
        <!-- PageHelper框架整合 -->
        <dependency>
            <groupId>com.github.pagehelper</groupId>
            <artifactId>pagehelper-spring-boot-starter</artifactId>
            <version>1.4.1</version>
        </dependency>
        <!-- knife4j框架整合 -->
        <dependency>
            <groupId>com.github.xiaoymin</groupId>
            <artifactId>knife4j-openapi2-spring-boot-starter</artifactId>
            <version>4.1.0</version>
        </dependency>
        <!-- fastjson框架整合 -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.75</version>
        </dependency>
        <!-- JJWT（Java JWT）框架整合 -->
        <dependency>
            <groupId>io.jsonwebtoken</groupId>
            <artifactId>jjwt</artifactId>
            <version>0.9.1</version>
        </dependency>
        <!-- Hutool框架整合 -->
        <dependency>
            <groupId>cn.hutool</groupId>
            <artifactId>hutool-all</artifactId>
            <version>5.6.3</version>
        </dependency>
        <!-- Sa-Token权限认证框架整合 -->
        <dependency>
            <groupId>cn.dev33</groupId>
            <artifactId>sa-token-spring-boot-starter</artifactId>
            <version>1.34.0</version>
        </dependency>
        <dependency>
            <groupId>cn.dev33</groupId>
            <artifactId>sa-token-redis-jackson</artifactId>
            <version>1.35.0.RC</version>
        </dependency>
        <dependency>
            <groupId>cn.dev33</groupId>
            <artifactId>sa-token-spring-aop</artifactId>
            <version>1.35.0.RC</version>
        </dependency>
        <!-- redis连接池框架整合-->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-pool2</artifactId>
        </dependency>
        <!-- Spring data redis框架整合-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-redis</artifactId>
        </dependency>
        <!-- 腾讯云对象存储框架整合 -->
        <dependency>
            <groupId>com.qcloud</groupId>
            <artifactId>cos_api</artifactId>
            <version>5.6.133</version>
        </dependency>
        <!-- 邮箱发送框架整合 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-mail</artifactId>
        </dependency>
        <!-- RabbitMQ框架整合 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-amqp</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.amqp</groupId>
            <artifactId>spring-rabbit-test</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- Spring data MongoDB框架整合 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-mongodb</artifactId>
        </dependency>
        <!-- WebSocket框架整合 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-websocket</artifactId>
        </dependency>
        <!--PinYin框架整合-->
        <dependency>
            <groupId>io.github.biezhi</groupId>
            <artifactId>TinyPinyin</artifactId>
            <version>2.0.3.RELEASE</version>
        </dependency>
        <!--百度智能云框架整合-->
        <dependency>
            <groupId>com.baidu.aip</groupId>
            <artifactId>java-sdk</artifactId>
            <version>4.16.7</version>
        </dependency>
```

## 后端项目部署

### 开发工具

IDEA Ultimate：[Download IntelliJ IDEA – The Leading Java and Kotlin IDE (jetbrains.com)](https://www.jetbrains.com/idea/download/?section=windows)

Dbeaver：[DBeaver Community | Free Universal Database Tool](https://dbeaver.io/)

MongoDBCompass：[MongoDB Compass | MongoDB](https://www.mongodb.com/products/tools/compass)

Another Redis Desktop Manager：[Another Redis Desktop Manager | 更快、更好、更稳定的Redis桌面(GUI)管理客户端，兼容Windows、Mac、Linux，性能出众，轻松加载海量键值 (goanother.com)](https://goanother.com/cn/)

### 开发环境搭建

JDK17 Oracle：[Java Archive Downloads - Java SE 17 (oracle.com)](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html)

MySQL8：[MySQL](https://www.mysql.com/cn/)

Redis：[Redis - The Real-time Data Platform](https://redis.io/)

RabbitMQ：[RabbitMQ: One broker to queue them all | RabbitMQ](https://www.rabbitmq.com/)

MongoDB：[MongoDB: 助力加速创新 | MongoDB](https://www.mongodb.com/zh-cn)

Git：[Git - Downloads (git-scm.com)](https://git-scm.com/download)

Apache Maven：[Maven – Welcome to Apache Maven](https://maven.apache.org/)

### Windows下环境工具和环境链接
链接：https://pan.baidu.com/s/1fm9ljkOFoVbR6Zsix-j4PA
提取码：ua6r
### 项目修改

application.yml，其中XXX为自定义内容

```yml
#服务器配置
server:
  #端口号
  port: 9080
#日志配置
logging:
  #日志级别
  level:
    #日志输出级别为debug级别
    com.cloud.apis: debug
#spring配置
spring:
  #数据源配置
  datasource:
    #数据库连接地址
    url: jdbc:mysql://localhost:3306/cloud_enterprise_digital?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai&nullCatalogMeansCurrent=true&maxAllowedPacket=1048576&noAccessToProcedureBodies=true
    #数据库用户名
    username: XXXXXXXXXXXXXXX
    #数据库密码
    password: XXXXXXXXXXXXXXX
    # Druid连接池的配置
    druid:
      # 初始化连接数量
      initial-size: 5
      # 最大激活数量
      max-active: 20
  # Redis配置
  redis:
    # Redis数据库索引（默认为0）
    database: 0
    # Redis服务器地址
    host: localhost
    # Redis服务器连接端口
    port: 6379
    # Redis服务器连接密码（默认为空）
    password: ~
    # 连接超时时间
    timeout: 10s
    # 连接池配置
    lettuce:
      pool:
        # 连接池最大连接数
        max-active: 200
        # 连接池最大阻塞等待时间（使用负值表示没有限制）
        max-wait: -1ms
        # 连接池中的最大空闲连接
        max-idle: 10
        # 连接池中的最小空闲连接
        min-idle: 0
  #servlet配置
  servlet:
    #上传文件配置
    multipart:
      # 上传文件最大大小
      max-request-size: 500MB
      # 单个文件最大大小
      max-file-size: 500MB
  #邮箱配置
  mail:
    #smtp服务主机
    host: smtp.163.com
    #服务协议
    protocol: smtp
    # 编码集
    default-encoding: UTF-8
    #发送邮件的账户
    username: XXXXXXXXXXXXXXX
    #授权码
    password: XXXXXXXXXXXXXXX
  #mongodb配置
  data:
    mongodb:
      # 数据库地址
      host: localhost
      # 数据库端口
      port: 27017
      # 数据库名称
      database: emos
      # 认证数据库
      authentication-database: admin
      # 认证用户名
      username: admin
      # 认证密码
      password: XXXXXXXXXXXXXXX
#mybatis-plus配置
mybatis-plus:
  # 扫描mapper.xml文件路径
  mapper-locations: classpath*:mappers/**/*.xml
#knife4j配置
knife4j:
  #是否开启
  enable: true
#sa-token权限配置
sa-token:
  # token名称 (同时也是cookie名称)
  token-name: token
  # token有效期，单位s 默认30天, -1代表永不过期2592000S
  timeout: 2592000
  # token临时有效期 (指定时间内无操作就视为token过期) 单位: 秒
  active-timeout: -1
  # 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录)
  is-concurrent: true
  # 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token)
  is-share: true
  # token风格
  token-style: uuid
  # 是否输出操作日志
  is-log: false
#自定义配置
custom-config:
  # 默认密码
  password: XXXXXXXXXXXXXXX
  # 默认分页
  default-query-page-size: 10
  # 系统密钥
  secret: XXXXXXXXXXXXXXX
  #保存用户临时提交的照片
  image-folder: D:/WorkFile/emos/image
#腾讯云配置
tencent:
  # 腾讯云COS配置
  cloud:
    # 腾讯云APPID
    appId: XXXXXXXXXXXXXXX
    # 腾讯云SecretId
    secretId: XXXXXXXXXXXXXXX
    # 腾讯云SecretKey
    secretKey: XXXXXXXXXXXXXXX
    # 腾讯云COS存储桶所在区域
    region: XXXXXXXXXXXXXXX
    # 腾讯云COS存储桶名称
    bucket: XXXXXXXXXXXXXXX
# 百度智能云
baidu:
  # 百度人脸识别appId
  appId: XXXXXXXXXXXXXXX
  # 百度云人脸识别key
  key: XXXXXXXXXXXXXXX
  # 百度云人脸识别密钥
  secret: XXXXXXXXXXXXXXX
```

腾讯云对象存储服务（可以自己搭建MiniIO+FTP的服务器代替）

腾讯位置服务或者其他开源的位置服务，目的是为了解析经纬度获取位置详情

百度智能云人脸识别服务（可以使用PythonFlask框架+tensorflow卷积神经网络框架自定义人脸识别服务器）

### 数据库文件

在项目的DB文件夹中

![image-20240519124523446](assets/image-20240519124523446.png)

### 运行部署

在Windows环境下

<img src="assets/image-20240519124851076.png" alt="image-20240519124851076" style="zoom:50%;" />

运行BAT文件即可运行服务端程序，如下：

![image-20240519124818249](assets/image-20240519124818249.png)

当然是打成JAR包的形式，同样对于Liunx云服务器来说可以直接使用Docker部署后采用端口映射访问。
package com.cloud.apis.message.pojo.form;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2023/9/12
 */
@Data
@ApiModel
public class DeleteMessageRefByIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotBlank
    private String id;
}


package com.cloud.apis.core.component;

import cn.dev33.satoken.stp.StpInterface;
import com.cloud.apis.user.service.impl.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * StpInterface实现类
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/20
 */
@Slf4j
@Component
public class StpInterfaceComponent implements StpInterface {
    @Autowired
    private UserService userService;

    public StpInterfaceComponent() {
        log.info("【StpInterfaceComponent】");
    }

    /**
     * 返回一个用户所拥有的权限集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginKey) {
        long userId = Long.parseLong(loginId.toString());
        ArrayList<String> permissions = userService.searchUserPermission(userId);
        return permissions;
    }

    /**
     * 返回一个用户所拥有的角色标识集合
     */
    @Override
    public List<String> getRoleList(Object o, String s) {
        ArrayList<String> list = new ArrayList<>();
        return list;
    }
}

package com.cloud.apis.dict.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.cloud.apis.approval.pojo.form.ApprovalListForm;
import com.cloud.apis.approval.pojo.vo.ApprovalVo;
import com.cloud.apis.core.model.Dict;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.web.R;
import com.cloud.apis.dict.pojo.form.*;
import com.cloud.apis.dict.service.IDictService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 字典相关API接口控制器
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/21
 */
@RestController
@RequestMapping("/dict")
@Api(tags = "字典相关API接口")
@Slf4j
public class DictController {
    @Autowired
    private IDictService dictService;

    public DictController() {
        log.info("【DictController】");
    }
    @PostMapping ("/type")
    @ApiOperation(value = "根据字典类型查询字典数据")
    @ApiOperationSupport(order = 1)
    @SaCheckLogin
    public R getDictData(@Valid @RequestBody TypeForm form) {
        log.debug("字典相关API接口控制器【DictController】-->调用根据字典类型查询字典数据功能【R getDictData(@Valid @RequestBody TypeForm form)】");
        return R.ok().put("data", dictService.getDictByType(form.getType()));
    }
    @PostMapping("/root")
    @ApiOperation(value = "查询顶层字典数据")
    @ApiOperationSupport(order = 2)
    @SaCheckLogin
    public R getDictRoot() {
        log.debug("字典相关API接口控制器【DictController】-->调用查询顶层字典数据功能【R getDictRoot()】");
        return R.ok().put("data", dictService.getDictRootList());
    }

    @PostMapping("")
    @ApiOperation("根据顶层字典分页查询子数据")
    @ApiOperationSupport(order = 3)
    @SaCheckLogin
    public R getDictList(@Valid @RequestBody DictListForm form){
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<Dict> pageData = dictService.listDict(form);
        return R.ok().put("data",pageData);
    }
    @PostMapping("/add")
    @ApiOperation("添加字典数据")
    @ApiOperationSupport(order = 4)
    @SaCheckLogin
    public R addDict(@Valid @RequestBody AddDictDataForm form){
        form.setEnable(true);
        return R.ok().put("data",dictService.addDict(form));
    }

    @PostMapping("/id")
    @ApiOperation("根据id查询字典数据")
    @ApiOperationSupport(order = 5)
    @SaCheckLogin
    public R getDictById(@Valid @RequestBody FindByDictIdForm form){
        return R.ok().put("data",dictService.getDictById(form.getId()));
    }

    @PutMapping("/update")
    @ApiOperation("修改字典数据")
    @ApiOperationSupport(order = 6)
    @SaCheckLogin
    public R updateDict(@Valid @RequestBody UpdateDictForm form){
        form.setEnable(true);
        dictService.updateDict(form);
        return R.ok("更新成功");
    }
    @PutMapping("/enable")
    @ApiOperation("字典数据是否启用")
    @ApiOperationSupport(order = 7)
    @SaCheckLogin
    public R enableDict(@Valid @RequestBody EnableDictForm form){
        dictService.setDictEnable(form);
        if(form.isEnable()){
            return R.ok("启用成功");
        }else {
            return R.ok("禁用成功");
        }
    }
    @PostMapping("/search")
    @ApiOperation("根据字典名称查询字典数据")
    @ApiOperationSupport(order = 8)
    @SaCheckLogin
    public R searchDict(@Valid @RequestBody SearchDictForm form){
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<Dict> pageData = dictService.listSearchResult(form);
        return R.ok().put("data",pageData);
    }
}

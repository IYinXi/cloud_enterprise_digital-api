-- 使用数据库
USE cloud_enterprise_digital;
-- 删除字典表
DROP TABLE dict_common;
-- 删除组织架构表
DROP TABLE tb_organization;
-- 删除岗位表
DROP TABLE tb_post;
-- 删除用户表
DROP TABLE tb_user;
-- 删除角色表
DROP TABLE tb_role;
-- 删除用户-角色表
DROP TABLE tb_user_role;
-- 删除权限表
DROP TABLE tb_permission;
-- 删除角色-权限表
DROP TABLE tb_role_permission;
-- 删除管理表
DROP TABLE tb_admin;
-- 删除审批表
DROP TABLE tb_approval;
-- 删除记录表
-- 删除转岗记录表
DROP TABLE document_transfer;
-- 删除离职记录表
DROP TABLE document_dimission;
-- 删除报销记录表
DROP TABLE document_reimbursement;
-- 删除用印记录表
DROP TABLE document_seal;
-- 删除补助记录表
DROP TABLE document_subsidy;
-- 删除工资记录表
DROP TABLE document_salary;
-- 删除请假记录表
DROP TABLE document_leave;
-- 删除出差记录表
DROP TABLE document_travle;
-- 删除工作表
-- 删除工作日报表
DROP TABLE work_daily;
-- 删除日程安排表
DROP TABLE work_date;
-- 删除考勤表
-- 删除考勤地点表
DROP TABLE attendance_address;
-- 删除考勤签到表
DROP TABLE attendance_checkin;
-- 删除特殊考勤表
DROP TABLE attendance_special;
-- 删除系统参数表
-- 删除参数模型表
DROP TABLE model_sys;
-- 删除人脸模型表
DROP TABLE model_face;
-- 删除数据库
DROP DATABASE cloud_enterprise_digital;

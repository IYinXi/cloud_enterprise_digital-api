package com.cloud.apis.organization.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/2
 */
@Data
public class TransferForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long transferId;
    private Long oldOrganizationId;
    private Long oldPostId;
    @NotNull(message = "请选择所转岗的组织")
    @Range(min = 1, message = "组织id不能为空")
    private Long nowOrganizationId;
    @NotNull(message = "请选择所转岗的岗位")
    @Range(min = 1,message = "请提交正确的岗位id")
    private Long nowPostId;
    private String reason;
    private String resources;
    private boolean isEnable;
}

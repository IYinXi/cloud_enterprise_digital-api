package com.cloud.apis.user.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 为角色添加权限
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class RolePermissionForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "角色id不能为空")
    private Long roleId;
    private Long[] permissionIds;
}

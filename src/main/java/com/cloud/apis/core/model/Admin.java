package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 管理表
 * tb_admin
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_admin")
public class Admin implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 管理id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 管理者id
     */
    @TableField("admin_id")
    private Long adminId;

    /**
     * 管理组织id
     */
    @TableField("organization_id")
    private Long organizationId;

    /**
     * 是否启用
     */
    @TableField("is_enable")
    private Boolean isEnable;

    /**
     * 创建日期
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 修改日期
     */
    @TableField("modified_time")
    private Date modifiedTime;
}
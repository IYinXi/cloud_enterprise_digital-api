package com.cloud.apis.dict.service.impl;

import com.cloud.apis.core.exception.CustomException;
import com.cloud.apis.core.model.Dict;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.utils.PageInfoToPageDataConverterUtil;
import com.cloud.apis.core.web.ServiceCode;
import com.cloud.apis.dict.db.dao.DictMapper;
import com.cloud.apis.dict.pojo.form.*;
import com.cloud.apis.dict.service.IDictService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 字典相关API接口业务层实现
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/25
 */
@Slf4j
@Service
public class DictService implements IDictService {
    @Autowired
    private DictMapper dictMapper;
    @Value("${custom-config.default-query-page-size}")
    private Integer defaultQueryPageSize;

    public DictService() {
        log.info("【DictService】");
    }

    @Override
    public ArrayList<Dict> getDictByType(String type) {
        log.debug("字典相关API接口业务层【DictService】-->调用根据字典类型【{}】查询字典数据", type);
        ArrayList<Dict> dicts = dictMapper.selectDictByType(type);
        if (dicts.size() == 0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "未查询到字典数据");
        } else {
            return dicts;
        }
    }

    @Override
    public Dict getDictByAbbreviation(String abbreviation) {
        log.debug("字典相关API接口业务层【DictService】-->调用根据字典简称【{}】查询字典数据", abbreviation);
        Dict dicts = dictMapper.selectDictByAbbreviation(abbreviation);
        if (dicts == null) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "未查询到字典数据");
        } else {
            return dicts;
        }
    }

    @Override
    public ArrayList<Dict> getDictRootList() {
        log.debug("字典相关API接口业务层【DictService】-->调用查询顶层字典数据");
        return dictMapper.selectDictRootList();
    }

    @Override
    public PageData<Dict> listDict(DictListForm form) {
        return listPage(form.getPageNum(), defaultQueryPageSize, form);
    }

    @Override
    public PageData<Dict> listPage(Integer pageNum, Integer pageSize, DictListForm form) {
        PageHelper.startPage(pageNum, pageSize);
        List<Dict> list = dictMapper.list(form.getRootId());
        PageInfo<Dict> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }

    @Override
    public Dict addDict(AddDictDataForm form) {
        if (dictMapper.selectDictByAbbreviation(form.getAbbreviation()) != null) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "字典简称已存在");
        }
        if(form.getParentId()==0){
            if(!dictMapper.selectDictByType(form.getType()).isEmpty()){
                throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "字典类型已存在");
            }
        }
        int num = dictMapper.insertDictData(form);
        if (num == 0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "添加字典数据失败");
        }
        return dictMapper.selectDictById(form.getId());
    }

    @Override
    public Dict getDictById(Long id) {
        return dictMapper.selectDictById(id);
    }

    @Override
    public void updateDict(UpdateDictForm form) {
        Dict dict = dictMapper.selectDictByAbbreviation(form.getAbbreviation());
        if (dict != null && !dict.getId().equals(form.getId())) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "字典简称已存在");
        }

        int num = dictMapper.updateDict(form);
        if (num == 0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "修改字典数据失败");
        }
    }

    @Override
    public void setDictEnable(EnableDictForm form) {
        if (dictMapper.selectDictById(form.getId()) == null) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "未查询到字典数据");
        }
        if (form.getParentId() == 0) {
            if (!form.isEnable() &&!dictMapper.selectDictByRootId(form.getId()).isEmpty()){
                throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "该字典数据下有子数据，不能禁用");
            }
        }

        int num = dictMapper.updateDictEnable(form);
        if (num == 0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "修改字典数据失败");
        }
    }

    @Override
    public PageData<Dict> searchByName(Integer pageNum, Integer pageSize, SearchDictForm form) {
        PageHelper.startPage(pageNum, pageSize);
        List<Dict> list = dictMapper.searchByName(form);
        PageInfo<Dict> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }

    @Override
    public PageData<Dict> listSearchResult(SearchDictForm form) {
        return searchByName(form.getPageNum(), defaultQueryPageSize, form);
    }
}

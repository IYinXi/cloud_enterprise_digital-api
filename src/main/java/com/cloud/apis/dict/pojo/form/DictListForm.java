package com.cloud.apis.dict.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/26
 */
@Data
public class DictListForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer pageNum;
    @NotNull(message = "根节点id不能为空")
    private Long rootId;
}

package com.cloud.apis.core.exception;

import cn.dev33.satoken.exception.NotLoginException;
import com.cloud.apis.core.web.R;
import com.cloud.apis.core.web.ServiceCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.mail.MailException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.UncheckedIOException;
import java.util.Objects;
import java.util.Set;


/**
 * 全局异常处理
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/20
 */
@Slf4j
@RestControllerAdvice
public class GlobalException {
    public GlobalException() {
        log.info("【GlobalException】");
    }

    /**
     * 捕获处理自定义异常
     */
    @ExceptionHandler
    public R yuedongExceptionHandler(CustomException e){
        log.debug("自定义异常详情",e);
        return R.error(e.getCode(),e.getMessage());
    }

    /**
     * 捕获处理格式异常信息
     */
    @ExceptionHandler
    public R handleBindException(BindException e) {
        log.debug("格式异常详情",e);
        String message = Objects.requireNonNull(e.getFieldError()).getDefaultMessage() +
                "！";
        return R.error(ServiceCode.ERROR_BAD_REQUEST.getValue(), message);
    }

    /**
     * 捕获处理验证异常信息
     */
    @ExceptionHandler
    public R handleConstraintViolationException(ConstraintViolationException e) {
        log.debug("验证异常详情",e);
        StringBuilder stringBuilder = new StringBuilder();
        Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
        for (ConstraintViolation<?> constraintViolation : constraintViolations) {
            stringBuilder.append(constraintViolation.getMessage());
        }
        String message = stringBuilder.toString();
        return R.error(ServiceCode.ERROR_BAD_REQUEST.getValue(), message);
    }

    /**
     * 捕获处理未登录异常
     */
    @ExceptionHandler
    public R handleNotLoginException(NotLoginException notLoginException){
        log.debug("未登录异常详情",notLoginException);
        return R.error(ServiceCode.ERROR_UNAUTHORIZED.getValue(),"未登录");
    }

    /**
     * 捕获处理Http请求方法异常
     */
    @ExceptionHandler
    public R handHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e){
        log.debug("HTTP请求方法错误详情",e);
        return R.error(ServiceCode.ERROR_BAD_REQUEST.getValue(),"HTTP请求方法错误");
    }

    /**
     * 捕获处理邮箱发送异常
     */
    @ExceptionHandler
    public R handMailException(MailException e){
        log.debug("邮件异常详情",e);
        return R.error(ServiceCode.ERROR_BAD_REQUEST.getValue(),"邮件发送错误");
    }
    @ExceptionHandler
    public R handDuplicateKeyException(DuplicateKeyException e){
        log.debug("数据不存在异常详情",e);
        return R.error(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前数据已存在");
    }

    /**
     * 捕获处理未知异常
     */
    @ExceptionHandler
    public R notKonwExceptionHandler(Throwable throwable){
        log.debug("未知异常详情",throwable);
        return R.error(ServiceCode.ERROR_BAD_REQUEST.getValue(),"OK");
    }

}

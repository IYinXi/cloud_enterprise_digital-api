package com.cloud.apis.document.pojo.form;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/6
 */
@Data
public class setTravleForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long userId;
    private String address;
    private String reason;
    private String resources;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Boolean isEnable;
}

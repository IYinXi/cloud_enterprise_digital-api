package com.cloud.apis.user.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class RolePermissionIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    @NotNull(message = "id不能为空")
    private Long roleId;
    @NotNull(message = "id不能为空")
    private Long permissionId;
}

package com.cloud.apis.user.pojo.vo;

import com.cloud.apis.core.model.Dict;
import com.cloud.apis.core.model.Organization;
import com.cloud.apis.core.model.Post;
import com.cloud.apis.organization.pojo.vo.OrganizationInfoVo;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * 用户信息VO
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/21
 */
@Data
public class UserInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户id
     */
    private Long id;
    /**
     * 证件照路径
     */
    private String avater;
    /**
     * 姓名
     */
    private String name;
    /**
     * 性别(来自通用字典数据)
     */
    private Long sex_id;
    /**
     * 生日
     */
    private LocalDateTime birthday;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 民族id(来自通用字典数据）
     */
    private Long ethnicId;
    /**
     * 学历id(来自通用字典数据)
     */
    private Long degreeId;
    /**
     * 毕业院校
     */
    private String graduate;
    /**
     * 身份id(来自通用字典数据）
     */
    private Long identityId;
    /**
     * 户籍地id(来自通用字典数据)
     */
    private Long domicileId;
    /**
     * 家庭住址
     */
    private String address;
    /**
     * 账户（也就是用户的国内手机号)
     */
    private String account;
    /**
     * 密码
     */
    private String password;
    /**
     * 所在组织id（来自于组织架构表）
     */
    private Long organizationId;
    /**
     * 岗位id(来自于岗位表）
     */
    private Long jobId;
    /**
     * 电子邮箱
     */
    private String eMail;
    /**
     * 微信号
     */
    private String vx;
    /**
     * 其他联系方式
     */
    private String contact;
    /**
     * 自我介绍
     */
    private String description;
    /**
     * 座右铭
     */
    private String motto;
    /**
     * 是否离职（0，离职；1，在职）
     */
    private Boolean isIncumbency;
    /**
     * 是否注册（0，未登记；1，已登记）
     */
    private Boolean isRegistration;
    /**
     * 入职日期
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime modifiedTime;
    /**
     * 离职时间
     */
    private LocalDateTime outTime;
    /**
     * 性别(来自通用字典数据）
     */
    private Dict sex;
    /**
     * 民族(来自通用字典数据）
     */
    private Dict ethnic;
    /**
     * 学历id(来自通用字典数据)
     */
    private Dict degree;
    /**
     * 身份(来自通用字典数据）
     */
    private Dict identity;
    /**
     * 户籍地id(来自通用字典数据)
     */
    private Dict domicile;
    /**
     * 所在公司（来自于组织架构表）
     */
    private OrganizationInfoVo organization;
    /**
     * 岗位(来自于岗位表）
     */
    private Post job;
}

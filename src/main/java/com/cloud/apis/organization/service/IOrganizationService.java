package com.cloud.apis.organization.service;

import com.cloud.apis.core.model.Organization;
import com.cloud.apis.organization.pojo.form.AddOrganizationForm;
import com.cloud.apis.organization.pojo.form.UpdateOrganizationForm;
import com.cloud.apis.organization.pojo.vo.GroupOrganizationVo;
import com.cloud.apis.organization.pojo.vo.OrganizationInfoVo;
import com.cloud.apis.organization.pojo.vo.OrganizationTreeVo;

import java.util.ArrayList;

/**
 * 组织架构API相关接口业务层接口
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/25
 */
public interface IOrganizationService {
    /**
     * 根据组织类型获取组织信息
     */
    ArrayList<Organization> getOrganizationByType(String type);

    /**
     * 根据父级id获取组织信息
     */
    ArrayList<Organization> getOrganizationByParentId(Long id);

    /**
     * 获取系列组织架构树
     */
    ArrayList<OrganizationTreeVo> getOrganizationStruct();
    /**
     * 获取当前所有组织架构
     */
    OrganizationTreeVo getAllOrganizationTree();
    /**
     * 根据组织id获取子组织内容
     */
    OrganizationTreeVo getOrganizationTreeById(Long id);

    /**
     * 根据组织id获取组织信息
     */
    OrganizationInfoVo getOrganizationById(Long id);

    /**
     * 更新组织信息
     */
    void updateOrganization(UpdateOrganizationForm form);

    /**
     * 添加组织
     */
    OrganizationInfoVo addOrganization(AddOrganizationForm form);

    /**
     * 删除组织
     */
    void deleteOrganization(Long id);
    /**
     * 获取所有分组的组织
     */
    ArrayList<GroupOrganizationVo> listGroupOrganization();
    /**
     * 获取所有组织，不包括当前用户所在的组织
     */
    ArrayList<OrganizationInfoVo> listExpectCurIds(Long id);
    /**
     * 获取所有组织
     */
    ArrayList<OrganizationInfoVo>getOrganizationList();
}

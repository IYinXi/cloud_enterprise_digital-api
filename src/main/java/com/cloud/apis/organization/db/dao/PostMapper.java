package com.cloud.apis.organization.db.dao;

import com.cloud.apis.core.model.Post;
import com.cloud.apis.organization.pojo.form.*;
import com.cloud.apis.organization.pojo.vo.PostInfoVo;
import com.cloud.apis.organization.pojo.vo.TransferInfoVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * 岗位相关DAO层
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/21
 */
@Mapper
public interface PostMapper {
    /**
     * 根据岗位id查询岗位信息
     */
    Post selectPostById(Long id);
    /**
     * 根据组织id查询对应的岗位信息
     */
    ArrayList<Post> selectPostByOrganizationId(ArrayList<Long> ids);
    /**
     * 根据组织id查询对应的岗位信息
     */
    List<PostInfoVo> list(Long organizationId);
    /**
     * 根据岗位简称查询岗位详情
     */
    PostInfoVo selectPostByAbbreviation(String abbreviation);
    /**
     * 添加岗位
     */
    int insertPost(AddPostForm form);
    /**
     * 根据岗位id查询岗位详情
     */
    PostInfoVo selectPostInfoById(Long id);
    /**
     * 更新岗位
     */
    int updatePost(UpdatePostForm form);
    /**
     * 启用或禁用岗位
     */
    int setPostEnable(EnablePostForm form);
    /**
     * 根据岗位名称查询岗位信息
     */
    List<PostInfoVo> searchByName(SearchPostForm form);
    /**
     * 根据组织id查询岗位信息
     */
    ArrayList<Post> listPostByOrganizationExpectUserId(Long organizationId, Long userId);
    /**
     * 创建转岗记录
     */
    int setTransfer(TransferForm form);
    /**
     * 根据转岗记录id查询转岗记录详情
     */
    TransferInfoVo selectTransferInfoById(Long transferId);
    /**
     * 更新转岗记录
     */
    int updateTransferInfo(UpdateTransferForm form);
}

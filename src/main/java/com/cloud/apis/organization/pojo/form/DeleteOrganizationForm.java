package com.cloud.apis.organization.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 删除组织
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/26
 */
@Data
public class DeleteOrganizationForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "组织id不能为空")
    private Long id;
}

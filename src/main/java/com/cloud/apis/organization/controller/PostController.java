package com.cloud.apis.organization.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.cloud.apis.core.model.Dict;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.web.R;
import com.cloud.apis.dict.pojo.form.DictListForm;
import com.cloud.apis.organization.pojo.form.*;
import com.cloud.apis.organization.pojo.vo.PostInfoVo;
import com.cloud.apis.organization.service.IPostService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 岗位API相关接口控制器
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/1
 */
@RestController
@RequestMapping("/post")
@Api(tags = "岗位相关API接口")
@Slf4j
public class PostController {
    @Autowired
    private IPostService postService;
    public PostController() {
        log.info("【PostController】");
    }
    @PostMapping("/organization")
    @ApiOperation(value = "根据组织id获取该组织下岗位集合")
    @ApiOperationSupport(order = 1)
    @SaCheckLogin
    public R getPostByOrganizationId(@Valid @RequestBody OrganizationIdForm form) {
        return R.ok().put("data", postService.getPostByOrganizationId(form.getId()));
    }

    @PostMapping("")
    @ApiOperation("根据直属组织分页获取岗位集合")
    @ApiOperationSupport(order = 2)
    @SaCheckLogin
    public R getApprovalList(@Valid @RequestBody PostListForm form){
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<PostInfoVo> pageData = postService.listPost(form);
        return R.ok().put("data",pageData);
    }

    @PostMapping("/addPost")
    @ApiOperation("添加岗位")
    @ApiOperationSupport(order = 3)
    @SaCheckLogin
    public R addPost(@Valid @RequestBody AddPostForm form){
        return R.ok().put("data",postService.addPost(form));
    }

    @PutMapping("/updatePost")
    @ApiOperation("修改岗位")
    @ApiOperationSupport(order = 4)
    @SaCheckLogin
    public R updatePost(@Valid @RequestBody UpdatePostForm form){
        postService.updatePost(form);
        return R.ok("修改成功");
    }

    @PostMapping("/info")
    @ApiOperation("根据岗位id获取岗位信息")
    @ApiOperationSupport(order = 5)
    @SaCheckLogin
    public R getPostInfoById(@Valid @RequestBody PostIdForm form){
        return R.ok().put("data",postService.getPostInfoById(form.getId()));
    }

    @PutMapping("/enable")
    @ApiOperation("修改岗位状态")
    @ApiOperationSupport(order = 6)
    @SaCheckLogin
    public R setPostEnable(@Valid @RequestBody EnablePostForm form){
        postService.setPostEnable(form);
        return R.ok("修改成功");
    }

    @PostMapping("/search")
    @ApiOperation("根据岗位名称搜索岗位")
    @ApiOperationSupport(order = 7)
    @SaCheckLogin
    public R searchByName(@Valid @RequestBody SearchPostForm form){
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<PostInfoVo> pageData = postService.listSearchResult(form);
        return R.ok().put("data",pageData);
    }

    @PostMapping("/getAllPost")
    @ApiOperation("根据组织id获取其下所属岗位")
    @ApiOperationSupport(order = 8)
    @SaCheckLogin
    public R getAllPost(@Valid @RequestBody OrganizationIdForm form){
        return R.ok().put("data",postService.listPostByOrganizationId(form));
    }

    @PostMapping("/getPostListExpectUSerId")
    @ApiOperation("根据组织id获取其下所属岗位")
    @ApiOperationSupport(order = 9)
    @SaCheckLogin
    public R getPostListExpectUSerId(@Valid @RequestBody OrganizationUserIdForm form){
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data",postService.listPostByOrganizationExpectUserId(form));
    }

    @PostMapping("/setTransfer")
    @ApiOperation("创建一条转岗记录")
    @ApiOperationSupport(order = 10)
    @SaCheckLogin
    public R setTransfer(@Valid @RequestBody TransferForm form){
        if(form.getTransferId()==null){
            form.setTransferId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data",postService.setTransfer(form));
    }
    @PutMapping ("/updateTransferInfo")
    @ApiOperation("修改转岗信息")
    @ApiOperationSupport(order = 11)
    @SaCheckLogin
    public R updateTransferInfo(@Valid @RequestBody UpdateTransferForm form){
        postService.updateTransferInfo(form);
        return R.ok();
    }
}

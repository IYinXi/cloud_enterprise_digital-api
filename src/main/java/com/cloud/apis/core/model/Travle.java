package com.cloud.apis.core.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 出差记录表
 * document_travle
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("document_travle")
public class Travle implements Serializable {
    /**
     * 出差记录id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableId(value = "user_id")
    private Long userId;

    /**
     * 出差地点
     */
    private String address;

    /**
     * 出差原因
     */
    private String reason;

    /**
     * 证明材料
     */
    private String resources;

    /**
     * 开始时间
     */
    @TableId(value = "start_time")
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    @TableId(value = "end_time")
    private LocalDateTime endTime;

    /**
     * 是否启用
     */
    @TableField(value = "is_enable")
    private Boolean isEnable;

    /**
     * 创建时间
     */
    @TableId(value = "create_time")
    private LocalDateTime createTime;

    private static final long serialVersionUID = 1L;
}
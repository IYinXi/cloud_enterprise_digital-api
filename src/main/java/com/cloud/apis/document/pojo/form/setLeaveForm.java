package com.cloud.apis.document.pojo.form;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/6
 */
@Data
public class setLeaveForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long userId;
    private Long typeId;
    private String reason;
    private String resources;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Boolean isEnable;
}

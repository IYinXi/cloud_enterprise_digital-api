package com.cloud.apis.user.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 根据名称搜索角色表单
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class SearchRoleForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "字典名称不能为空")
    private String name;
    @NotNull(message = "页码不能为空")
    private Integer pageNum;
}

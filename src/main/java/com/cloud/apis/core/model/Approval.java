package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 审批流程表
 * tb_approval
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_approval")
public class Approval implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 审批id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 申请者id
     */
    @TableField("apply_id")
    private Long applyId;

    /**
     * 审批者id
     */
    @TableField("approval_id")
    private Long approvalId;

    /**
     * 审批类型id
     */
    @TableField("approval_type")
    private Long approvalType;

    /**
     * 相关附件路径
     */
    private String certificate;

    /**
     * 审批说明
     */
    @TableField("approval_reason")
    private String approvalReason;
    /**
     * 申请说明
     */
    @TableField("apply_reason")
    private String applyReason;

    /**
     * 审批状态（0,已提交;1,待审批;2,通过;3,未通过;4,已完成）
     */
    @TableField("approval_status")
    private Byte approvalStatus;

    /**
     * 是否撤销（0,否;1,是)
     */
    @TableField("is_cut")
    private Boolean isCut;

    /**
     * 申请日期
     */
    @TableField("apply_time")
    private Date applyTime;

    /**
     * 审批日期
     */
    @TableField("approval_time")
    private Date approvalTime;
}
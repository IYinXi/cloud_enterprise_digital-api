package com.cloud.apis.work.db.dao;

import com.cloud.apis.work.pojo.form.UpdateWorkForm;
import com.cloud.apis.work.pojo.form.WorkDailyForm;
import com.cloud.apis.work.pojo.form.WorkDateForm;
import com.cloud.apis.work.pojo.form.WorkDateIdForm;
import com.cloud.apis.work.pojo.vo.WorkDailyVo;
import com.cloud.apis.work.pojo.vo.WorkDateVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/4
 */
@Mapper
public interface WorkMapper {
    List<HashMap> searchWorkByPage(Long userId, long start, int length);
    int setWorkDaily(@Param("form")WorkDailyForm form);
    WorkDailyVo selectWorkDailyById(Long id);

    WorkDailyVo searchWorkDailyDetialById(Long id);

    int setWorkDate(WorkDateForm form);

    WorkDateVo selectWorkDateById(Long id);

    WorkDateVo selectWorkDateByIdAndDate(WorkDateIdForm form);

    int updateWorkDate(UpdateWorkForm form);
}

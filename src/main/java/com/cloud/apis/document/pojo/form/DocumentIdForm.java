package com.cloud.apis.document.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/3
 */
@Data
public class DocumentIdForm implements Serializable {
    private static long serialVersionUID = 1L;
    @NotNull(message = "数据更新，请再次点击")
    @Range(min = 1, message = "数据更新，请再次点击")
    private Long id;
}

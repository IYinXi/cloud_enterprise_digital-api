package com.cloud.apis.dict.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 是否启用字典数据表单
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/27
 */
@Data
public class EnableDictForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "id不能为空")
    private Long id;
    @NotNull(message = "父节点id不能为空")
    private Long parentId;
    private boolean isEnable;
}

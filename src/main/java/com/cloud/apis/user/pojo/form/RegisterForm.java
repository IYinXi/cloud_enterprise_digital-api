package com.cloud.apis.user.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 注册用户
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/20
 */
@Data
public class RegisterForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "账号不能为空")
    @Pattern(regexp = "^1[3-9]\\d{9}$", message = "账号格式不正确")
    @ApiModelProperty(value = "账号", required = true)
    private String account;

    @NotNull(message = "邮箱不能为空")
    @Pattern(regexp = "^([a-zA-Z]|[0-9])(\\w|\\-)+@[a-zA-Z0-9]+\\.([a-zA-Z]{2,4})$", message = "邮箱格式不正确")
    @ApiModelProperty(value = "邮箱", required = true)
    private String eMail;

    @NotNull(message = "角色不能为空")
    @ApiModelProperty(value = "角色id", required = true)
    private Long[] roleId;

    @NotNull(message = "组织不能为空")
    @Range(min = 1, message = "请提交正确的组织id")
    @ApiModelProperty(value = "组织id", required = true)
    private Long organizationId;

    @NotNull(message = "岗位不能为空")
    @Range(min = 1, message = "请提交正确的岗位id")
    @ApiModelProperty(value = "岗位id", required = true)
    private Long jobId;
}

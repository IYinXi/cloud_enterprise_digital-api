package com.cloud.apis.core.config;

import com.baidu.aip.face.AipFace;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 百度云配置
 */
@Configuration
@Slf4j
public class BaiduConfig {
 
    @Value("${baidu.appId}")
    private String appId;
 
    @Value("${baidu.key}")
    private String key;
 
    @Value("${baidu.secret}")
    private String secret;
    public BaiduConfig(){
        log.info("【BaiduConfig】");
    }
    /**
     * 创建百度云对象
     * @return
     */
    @Bean
    public AipFace aipFace(){
        AipFace client = new AipFace(appId, key, secret);
        return client;
    }
}
package com.cloud.apis.attendance.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/9
 */
@Data
public class AddressVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String nation;
    private String province;
    private String city;
    private String district;
    private String street;
    private String streetNumber;
    private Boolean isEnable;
    private LocalDateTime createTime;
}

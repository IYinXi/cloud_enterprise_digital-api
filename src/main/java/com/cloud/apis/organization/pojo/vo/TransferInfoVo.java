package com.cloud.apis.organization.pojo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.cloud.apis.user.pojo.vo.UserInfoVo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/2
 */
@Data
public class TransferInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private Long id;

    /**
     * 转岗人id
     */
    private Long transferId;
    /**
     * 转岗人信息
     */
    private UserInfoVo transfer;
    /**
     * 原组织id
     */
    private Long oldOrganizationId;
    /**
     * 原组织信息
     */
    private OrganizationInfoVo oldOrganization;
    /**
     * 原岗位id
     */
    private Long oldPostId;
    /**
     * 原岗位信息
     */
    private PostInfoVo oldPost;
    /**
     * 现组织id
     */
    private Long nowOrganizationId;
    /**
     * 现组织信息
     */
    private OrganizationInfoVo nowOrganization;
    /**
     * 现岗位id
     */
    private String nowPostId;
    /**
     * 现岗位信息
     */
    private PostInfoVo nowPost;
    /**
     * 转岗理由
     */
    private String reason;
    /**
     * 相关材料路径
     */
    private String resources;
    /**
     * 是否可用
     */
    private Boolean isEnable;

    /**
     * 记录时间
     */
    private Date logTime;
}

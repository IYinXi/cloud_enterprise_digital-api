package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 岗位模型
 * tb_post
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_post")
public class Post implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 岗位id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 组织id
     */
    @TableField("organization_id")
    private Long organizationId;

    /**
     * 岗位名称
     */
    private String name;

    /**
     * 岗位简称
     */
    private String abbreviation;

    /**
     * 是否启用(0,否;1,是)
     */
    @TableField("is_enable")
    private Boolean isEnable;

    /**
     * 创建日期
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 修改日期
     */
    @TableField("modified_time")
    private Date modifiedTime;


}
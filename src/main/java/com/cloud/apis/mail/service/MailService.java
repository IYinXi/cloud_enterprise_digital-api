package com.cloud.apis.mail.service;
import com.cloud.apis.mail.utils.MailSendUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * 邮件发送业务
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/5
 */
@Slf4j
@Component
public class MailService{
    @Autowired
    private MailSendUtil mailSendUtil;
    public MailService() {
        log.info("【MailService】");
    }

    /**
     * 发送入职邮件
     */
    @Async("AsyncTaskExecutor")
    public void sendEntryMail(Long id, String[] receivers) {
        mailSendUtil.sendEntryMail(id,receivers);
    }
    /**
     * 发送离职邮件
     */
    @Async("AsyncTaskExecutor")
    public void sendOutMail(Long id, String[] receivers) {
        mailSendUtil.sendOutMail(id,receivers);
    }
    /**
     * 发送工资邮件
     */
    @Async("AsyncTaskExecutor")
    public void sendSalaryMail(Long id, String[] receivers) {
        mailSendUtil.sendSalaryMail(id,receivers);
    }
    @Async("AsyncTaskExecutor")
    public void sendException(Long id, String[] receivers) {
        mailSendUtil.sendException(id,receivers);
    }
}

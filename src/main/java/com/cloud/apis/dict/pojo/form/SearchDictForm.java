package com.cloud.apis.dict.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 搜索表单
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/27
 */
@Data
public class SearchDictForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "字典名称不能为空")
    private String name;
    @NotNull(message = "页码不能为空")
    private Integer pageNum;
    @NotNull(message = "父级id不能为空")
    @Range(min = 1, message = "请提交有效的id")
    private Long parentId;
}

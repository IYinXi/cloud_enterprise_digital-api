package com.cloud.apis.user.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 添加权限表单
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class AddPermissionForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    @NotNull(message = "权限名称不能为空")
    private String name;
    @NotNull(message = "权限简称不能为空")
    private String code;
    private boolean isEnable;

}

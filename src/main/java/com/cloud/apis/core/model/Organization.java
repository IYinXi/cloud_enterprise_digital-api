package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 组织架构表
 * tb_organization
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_organization")
public class Organization implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 组织架构id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 父级id
     */
    @TableField("parent_id")
    private Long parentId;

    /**
     * 组织名称
     */
    private String name;

    /**
     * 简称
     */
    private String abbreviation;

    /**
     * 是否启用(0,否;1,是)
     */
    @TableField("is_enable")
    private Boolean isEnable;
    /**
     * 组织类型id
     */
    @TableField("type_id")
    private Long typeId;

    /**
     * 创建日期
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 修改日期
     */
    @TableField("modified_time")
    private Date modifiedTime;


}
package com.cloud.apis.message.service.impl;

import com.cloud.apis.message.db.dao.MessageDao;
import com.cloud.apis.message.db.dao.MessageRefDao;
import com.cloud.apis.message.model.MessageEntity;
import com.cloud.apis.message.model.MessageRefEntity;
import com.cloud.apis.message.service.IMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 消息模块业务实现
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/5
 */
@Slf4j
@Service
public class MessageService implements IMessageService {
    @Autowired
    private MessageDao messageDao;
    @Autowired
    private MessageRefDao messageRefDao;
    private MessageService(){
        log.info("【MessageService】");
    }
    @Override
    public String insert(MessageEntity entity) {
        return messageDao.insert(entity);
    }

    @Override
    public List<HashMap> searchMessageByPage(Long userId, long start, int length) {
        return messageDao.searchMessageByPage(userId,start,length);
    }

    @Override
    public HashMap searchMessageById(String id) {
        return messageDao.searchMessageById(id);
    }

    @Override
    public String insert(MessageRefEntity entity) {
        return messageRefDao.insert(entity);
    }

    @Override
    public long searchUnreadCount(Long userId) {
        return messageRefDao.searchUnreadCount(userId);
    }

    @Override
    public long searchLastCount(Long userId) {
        return messageRefDao.searchLastCount(userId);
    }

    @Override
    public long updateUnreadMessage(String id) {
        return messageRefDao.updateUnreadMessage(id);
    }

    @Override
    public long deleteMessageRefById(String id) {
        return messageRefDao.deleteMessageRefById(id);
    }

    @Override
    public long deleteUserMessageRef(int userId) {
        return messageRefDao.deleteUserMessageRef(userId);
    }
}

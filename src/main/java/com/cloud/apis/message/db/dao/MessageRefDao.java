package com.cloud.apis.message.db.dao;


import com.cloud.apis.message.model.MessageRefEntity;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/5
 */
@Repository
public class MessageRefDao {
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 向消息引用表中插入一条数据
     */
    public String insert(MessageRefEntity entity){
        entity = mongoTemplate.save(entity);
        return entity.get_id();
    }
    /**
     * 查询某个用户的未读消息
     */
    public long searchUnreadCount(Long userId){
        Query query = new Query();
        query.addCriteria(Criteria.where("readFlag").is(false).and("receiverId").is(userId));
        long count = mongoTemplate.count(query,MessageRefEntity.class);
        return count;
    }
    /**
     * 查询用户新接收到的消息数量
     */
    public long searchLastCount(Long userId){
        Query query = new Query();
        query.addCriteria(Criteria.where("lastFlag").is(true).and("receiverId").is(userId));
        Update update = new Update();
        update.set("lastFlag",false);
        UpdateResult result = mongoTemplate.updateMulti(query,update,"message_ref");
        long rows = result.getModifiedCount();
        return rows;
    }
    /**
     * 将未读消息改为已读消息
     */
    public long updateUnreadMessage(String id){
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        Update update = new Update();
        update.set("readFlag",true);
        UpdateResult result = mongoTemplate.updateFirst(query,update,"message_ref");
        long rows = result.getModifiedCount();
        return rows;
    }
    /**
     * 根据主键值删除消息记录
     */
    public long deleteMessageRefById(String id){
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        DeleteResult result = mongoTemplate.remove(query,"message_ref");
        long rows = result.getDeletedCount();
        return rows;
    }
    /**
     * 根据userId删除用户的所有消息
     */
    public long deleteUserMessageRef(int userId){
        Query query = new Query();
        query.addCriteria(Criteria.where("receiverId").is(userId));
        DeleteResult result = mongoTemplate.remove(query,"message_ref");
        long rows = result.getDeletedCount();
        return rows;
    }
}

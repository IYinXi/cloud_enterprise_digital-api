package com.cloud.apis.document.pojo.vo;

import com.cloud.apis.core.model.Dict;
import com.cloud.apis.user.pojo.vo.UserInfoVo;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/6
 */
@Data
public class LeaveVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long userId;
    private UserInfoVo user;
    private Long typeId;
    private Dict type;
    private String reason;
    private String resources;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Boolean isEnable;
    private LocalDateTime createTime;
}

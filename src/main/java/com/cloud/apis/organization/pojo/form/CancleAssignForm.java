package com.cloud.apis.organization.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/29
 */
@Data
public class CancleAssignForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "id不能为空")
    @Range(min = 1,message = "id必须大于0")
    private Long id;
    @NotNull(message = "id不能为空")
    @Range(min = 1,message = "id必须大于0")
    private Long organizationId;
}

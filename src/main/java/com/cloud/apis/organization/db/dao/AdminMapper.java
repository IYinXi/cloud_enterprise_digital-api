package com.cloud.apis.organization.db.dao;

import com.cloud.apis.core.model.Organization;
import com.cloud.apis.organization.pojo.form.AdminIdForm;
import com.cloud.apis.organization.pojo.form.AssignForm;
import com.cloud.apis.organization.pojo.form.CancleAssignForm;
import com.cloud.apis.organization.pojo.form.OrganizationIdPageForm;
import com.cloud.apis.organization.pojo.vo.AdminInfoVo;
import com.cloud.apis.user.pojo.vo.UserInfoVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/29
 */
@Mapper
public interface AdminMapper {
    /**
     * 根据管理者id获取管理的组织列表
     */
    ArrayList<AdminInfoVo> getAdminOrganizations(Long id);
    /**
     * 根据组织id获取其下所属组织
     */
    ArrayList<Organization> selectOrganizationByTypeId(@Param("typeId") Long id,@Param("form") AdminIdForm form);
    /**
     * 添加指派
     */
    int insertAssign(AssignForm form);
    /**
     * 根据管理id获取内容
     */
    AdminInfoVo selectAdminById(Long id);
    /**
     * 删除指派
     */
    int deleteAssign(CancleAssignForm form);
    /**
     * 根据组织id获取组织下的管理员
     */
    ArrayList<UserInfoVo> selectadminByOrganizationId(Long id);
    /**
     * 根据组织id获取组织下的管理员
     */
    List<UserInfoVo> organizationsPage(OrganizationIdPageForm form);
    /**
     * 根据用户id获取其管理的组织id
     */
    ArrayList<Organization> selectAdminByUserId(Long id);
    /**
     * 根据用户id删除其管理的组织id
     */
    int deleteAssignsByAdminId(Long id);
    /**
     * 管理当前用户所在组织的用户id
     */
    ArrayList<Long> selectAdminByOrganizationId(Long organizationId);
}

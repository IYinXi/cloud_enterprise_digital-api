package com.cloud.apis.user.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 根据角色id获取分页权限的表单
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class RolePermissionPageForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "角色id不能为空")
    Long roleId;
    private Integer pageNum;
}

package com.cloud.apis.core.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户模型
 * tb_user
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_user")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 证件照路径
     */
    private String avater;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别(来自通用字典数据)
     */
    @TableField("sex_id")
    private Long sexId;

    /**
     * 生日
     */
    private LocalDateTime birthday;

    /**
     * 身份证号
     */
    @TableField("idcard")
    private String idCard;

    /**
     * 民族id(来自通用字典数据）
     */
    @TableField("ethnic_id")
    private Long ethnicId;

    /**
     * 学历id(来自通用字典数据)
     */
    @TableField("degree_id")
    private Long degreeId;

    /**
     * 毕业院校
     */
    private String graduate;

    /**
     * 身份id(来自通用字典数据）
     */
    @TableField("identity_id")
    private Long identityId;

    /**
     * 户籍地id(来自通用字典数据)
     */
    @TableField("domicile_id")
    private Long domicileId;

    /**
     * 家庭住址
     */
    private String address;
    /**
     * 账户（也就是用户的国内手机号)
     */
    private String account;
    /**
     * 密码
     */
    private String password;
    /**
     * 所在组织id（来自于组织架构表）
     */
    @TableField("organization_id")
    private Long organizationId;
    /**
     * 岗位id(来自于岗位表）
     */
    @TableField("job_id")
    private Long jobId;
    /**
     * 电子邮箱
     */
    @TableField("e_mail")
    private String eMail;
    /**
     * 微信号
     */
    private String vx;
    /**
     * 其他联系方式
     */
    private String contact;
    /**
     * 自我介绍
     */
    private String description;
    /**
     * 座右铭
     */
    private String motto;
    /**
     * 是否离职（0，离职；1，在职）
     */
    @TableField("is_incumbency")
    private Boolean isIncumbency;
    /**
     * 是否登记(0，未登记；1，已登记)
     */
    @TableField("is_registration")
    private Boolean isRegistration;
    /**
     * 入职日期
     */
    @TableField("create_time")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @TableField("modified_time")
    private LocalDateTime modifiedTime;
    /**
     * 离职时间
     */
    @TableField("out_time")
    private LocalDateTime outTime;
}
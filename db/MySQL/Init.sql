-- 创建数据库
create database cloud_enterprise_digital;
-- 使用数据库
use cloud_enterprise_digital;
-- 创建表并初始化
-- 数据字典表
CREATE TABLE `dict_common` (
                               `id` bigint NOT NULL AUTO_INCREMENT COMMENT '通用字典id',
                               `name` varchar(255) NOT NULL COMMENT '名称',
                               `abbreviation` varchar(255) NOT NULL COMMENT '简称',
                               `parent_id` bigint DEFAULT NULL COMMENT '父级id',
                               `type` varchar(255) NOT NULL COMMENT '字典数据类型',
                               `remarks` varchar(255) DEFAULT NULL COMMENT '字典数据说明',
                               `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                               `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
                               `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否开启',
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `dict_common_unique` (`abbreviation`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8mb3 COMMENT='通用数据字典';
-- 民族相关数据
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(1, '民族', 'ethnicList', 0, 'ethnic', '民族', '2024-03-21 19:12:38', '2024-04-27 16:33:24', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(2, '汉族', 'hanzu', 1, 'ethnic', '民族', '2024-03-21 19:13:43', '2024-05-02 18:13:40', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(3, '壮族', 'zhuangzu', 1, 'ethnic', '民族', '2024-03-21 19:14:26', '2024-04-27 12:19:33', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(6, '蒙古族', 'mengguzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-04-27 12:18:30', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(7, '回族', 'huizu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(8, '藏族', 'zangzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(9, '维吾尔族', 'weiwuerzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(10, '苗族', 'miaozu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(11, '彝族', 'yizu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(12, '布依族', 'buyizu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(13, '朝鲜族', 'chaoxianzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(14, '满族', 'manzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(15, '侗族', 'dongzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(16, '瑶族', 'yaozu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(17, '白族', 'baizu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(18, '土家族', 'tujiaz', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(19, '哈尼族', 'hanizu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(20, '哈萨克族', 'hasakezu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(21, '傣族', 'daizu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(22, '黎族', 'lizu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(23, '傈僳族', 'lisuzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(24, '佤族', 'wazu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(25, '畲族', 'shezu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(26, '高山族', 'gaoshanzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(27, '拉祜族', 'lahuzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(28, '水族', 'shuizu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(29, '东乡族', 'dongxiangzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(30, '纳西族', 'naxizu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(31, '景颇族', 'jingpozu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(32, '柯尔克孜族', 'keerkezizu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(33, '土族', 'tuzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(34, '达斡尔族', 'dawoerzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(35, '仫佬族', 'mulaozu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(36, '羌族', 'qiangzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(37, '布朗族', 'bulangzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(38, '撒拉族', 'salazu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(39, '毛难族', 'maonanzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(40, '仡佬族', 'gelaozu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(41, '锡伯族', 'xibozu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(42, '阿昌族', 'achangzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(43, '普米族', 'pumizu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(44, '塔吉克族', 'tajikezu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-04-27 21:18:04', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(45, '怒族', 'nuzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(46, '乌孜别克族', 'wuzibiekezu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(47, '俄罗斯族', 'eluosizu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(48, '鄂温克族', 'ewenkezu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(49, '德昂族', 'deangzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(50, '保安族', 'baoanzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(51, '裕固族', 'yuguzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(52, '京族', 'jingzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(53, '塔塔尔族', 'tataerzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(54, '独龙族', 'dulongzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(55, '鄂伦春族', 'elunchunzu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(56, '赫哲族', 'hezhezu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(57, '门巴族', 'menbazu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(58, '珞巴族', 'luobazu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(59, '基诺族', 'jinuozu', 1, 'ethnic', '民族', '2024-03-21 19:22:58', '2024-03-21 19:22:58', 1);
-- 学历相关数据
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(60, '学历', 'degreeList', 0, 'degree', '学历', '2024-03-21 19:24:28', '2024-03-21 19:24:30', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(61, '小学及以下', 'xiaoxue', 60, 'degree', '学历', '2024-03-21 19:29:18', '2024-03-21 19:29:18', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(62, '初中', 'chuchong', 60, 'degree', '学历', '2024-03-21 19:29:18', '2024-03-21 19:29:18', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(63, '中专', 'zhongzhuan', 60, 'degree', '学历', '2024-03-21 19:29:18', '2024-04-27 21:16:20', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(64, '高中', 'gaozhong', 60, 'degree', '学历', '2024-03-21 19:29:18', '2024-03-21 19:29:18', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(65, '大专', 'dazhuan', 60, 'degree', '学历', '2024-03-21 19:29:18', '2024-03-21 19:29:18', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(66, '本科', 'benke', 60, 'degree', '学历', '2024-03-21 19:29:18', '2024-03-21 19:29:18', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(67, '研究生', 'yanjiusheng', 60, 'degree', '学历', '2024-03-21 19:29:18', '2024-03-21 19:29:18', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(68, '博士及以上', 'boshi', 60, 'degree', '学历', '2024-03-21 19:29:18', '2024-03-21 19:29:18', 1);
-- 身份相关数据
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(69, '身份', 'identityList', 0, 'identity', '身份', '2024-03-21 19:30:45', '2024-03-21 19:30:45', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(70, '群众', 'qunzhong', 69, 'identity', '身份', '2024-03-21 19:36:02', '2024-03-21 19:36:02', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(71, '共青团员', 'gongqingtuanyuan', 69, 'identity', '身份', '2024-03-21 19:36:02', '2024-03-21 19:36:02', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(72, '中共党员', 'dangyuan', 69, 'identity', '身份', '2024-03-21 19:36:02', '2024-03-21 19:36:02', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(73, '九三学社', 'jiusanxueshe', 69, 'identity', '身份', '2024-03-21 19:36:02', '2024-03-21 19:36:02', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(74, '民革', 'mingge', 69, 'identity', '身份', '2024-03-21 19:36:02', '2024-03-21 19:36:02', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(75, '民建', 'mingjian', 69, 'identity', '身份', '2024-03-21 19:36:02', '2024-03-21 19:36:02', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(76, '民进', 'mingjin', 69, 'identity', '身份', '2024-03-21 19:36:02', '2024-03-21 19:36:02', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(77, '农工', 'noggong', 69, 'identity', '身份', '2024-03-21 19:36:02', '2024-03-21 19:36:02', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(78, '致工', 'zhigong', 69, 'identity', '身份', '2024-03-21 19:36:02', '2024-03-21 19:36:02', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(79, '台盟', 'taimeng', 69, 'identity', '身份', '2024-03-21 19:36:02', '2024-03-21 19:36:02', 1);
-- 户籍地相关数据
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(80, '户籍地', 'domicileList', 0, 'domicile', '户籍', '2024-03-21 19:38:04', '2024-03-21 19:38:04', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(81, '北京市', 'beijing', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(82, '天津市', 'tianjin', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(83, '河北省', 'hebei', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(84, '山西省', 'shanxi', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(85, '内蒙古自治区', 'neimenggu', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(86, '辽宁省', 'liaoning', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(87, '吉林省', 'jilin', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(88, '黑龙江省', 'heilongjiang', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(89, '上海市', 'shanghai', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(90, '江苏省', 'jiangsu', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(91, '浙江省', 'zhejiang', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(92, '安徽省', 'anhui', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(93, '福建省', 'fujian', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(94, '江西省', 'jiangxi', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(95, '山东省', 'shandong', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(96, '河南省', 'henan', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(97, '湖北省', 'hubei', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(98, '湖南省', 'hunan', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(99, '广东省', 'guangdong', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(100, '广西壮族自治区', 'guangxi', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(101, '海南省', 'hainan', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(102, '重庆市', 'chongqing', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(103, '四川省', 'sichuan', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(104, '贵州省', 'guizhou', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(105, '云南省', 'yunnan', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(106, '西藏自治区', 'xizang', 80, 'domicile', '户籍', '2024-03-21 19:43:59', '2024-03-21 19:43:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(109, '陕西省', 'shanxv', 80, 'domicile', '户籍', '2024-03-21 19:45:43', '2024-03-21 19:45:43', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(110, '甘肃省', 'gansu', 80, 'domicile', '户籍', '2024-03-21 19:45:43', '2024-03-21 19:45:43', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(111, '青海省', 'qinghai', 80, 'domicile', '户籍', '2024-03-21 19:45:43', '2024-03-21 19:45:43', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(112, '宁夏回族自治区', 'ningxia', 80, 'domicile', '户籍', '2024-03-21 19:45:43', '2024-03-21 19:45:43', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(113, '新疆维吾尔自治区', 'xinjiang', 80, 'domicile', '户籍', '2024-03-21 19:45:43', '2024-03-21 19:45:43', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(117, '香港特别行政区', 'xianggang', 80, 'domicile', '户籍', '2024-03-21 19:47:07', '2024-03-21 19:47:07', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(118, '澳门特别行政区', 'aomen', 80, 'domicile', '户籍', '2024-03-21 19:47:07', '2024-03-21 19:47:07', 1);
-- 性别相关数据
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(124, '性别', 'sexList', 0, 'sex', '性别', '2024-03-21 21:50:13', '2024-03-21 21:50:15', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(125, '男', 'nan', 124, 'sex', '性别', '2024-03-21 21:51:10', '2024-03-21 21:51:12', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(126, '女', 'nv', 124, 'sex', '性别', '2024-03-21 21:51:37', '2024-03-21 21:51:39', 1);
-- 组织类型相关数据
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(127, '组织类型', 'organizationList', 0, 'organization', '组织类型', '2024-03-21 21:51:37', '2024-03-21 21:51:39', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(128, '公司', 'company', 127, 'organization', '组织类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(129, '中心', 'centre', 127, 'organization', '组织类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(130, '部门', 'department', 127, 'organization', '组织类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
-- 审批类型相关数据
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(131, '审批类型', 'approvalTypeList', 0, 'approvalType', '审批类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(132, '入职', 'entry', 131, 'approvalType', '审批类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(133, '请假', 'leave', 131, 'approvalType', '审批类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(134, '离职', 'out', 131, 'approvalType', '审批类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(135, '转岗', 'transfer', 131, 'approvalType', '审批类型', '2024-03-21 21:51:37', '2024-05-02 10:41:13', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(136, '出差', 'travle', 131, 'approvalType', '审批类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(137, '报销', 'reimbursement', 131, 'approvalType', '审批类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(138, '补助', 'subsidy', 131, 'approvalType', '审批类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(139, '用印', 'seal', 131, 'approvalType', '审批类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(140, '加班', 'jiaban', 131, 'approvalType', '审批类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(141, '申诉', 'shensu', 131, 'approvalType', '审批类型', '2024-03-21 21:51:37', '2024-03-21 21:51:37', 1);
-- 报销类型相关数据
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(149, '报销类型', 'reimbursementTypeList', 0, 'reimbursement', '报销类型', '2024-05-03 01:32:46', '2024-05-03 01:32:46', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(150, '住宿', 'zhusu', 149, 'reimbursement', '报销类型', '2024-05-03 01:35:25', '2024-05-03 01:35:25', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(151, '聚餐', 'jucan', 149, 'reimbursement', '报销类型', '2024-05-03 01:36:06', '2024-05-03 01:36:06', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(152, '车票', 'chepiao', 149, 'reimbursement', '报销类型', '2024-05-03 01:38:11', '2024-05-03 01:38:11', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(153, '出行', 'chuxing', 149, 'reimbursement', '报销类型', '2024-05-03 01:40:08', '2024-05-03 01:40:08', 1);
-- 请假类型相关数据
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(154, '请假类型', 'leaveTypeList', 0, 'leave', '请假类型', '2024-05-03 16:32:19', '2024-05-03 16:32:19', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(155, '事假', 'shijia', 154, 'leave', '请假类型', '2024-05-03 16:33:11', '2024-05-03 16:33:11', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(156, '病假', 'bingjia', 154, 'leave', '请假类型', '2024-05-03 16:33:32', '2024-05-03 16:33:32', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(157, '年假', 'nianjia', 154, 'leave', '请假类型', '2024-05-03 16:41:06', '2024-05-03 16:41:21', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(158, '调休', 'tiaoxiu', 154, 'leave', '请假类型', '2024-05-03 16:48:32', '2024-05-03 16:48:47', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(159, '婚假', 'hunjia', 154, 'leave', '请假类型', '2024-05-03 16:49:12', '2024-05-03 16:49:12', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(160, '产假', 'chanjia', 154, 'leave', '请假类型', '2024-05-03 16:49:40', '2024-05-03 16:49:40', 1);
-- 补助类型相关数据
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(161, '补助类型', 'subsidyListType', 0, 'subsidy', '补助类型', '2024-05-03 16:51:56', '2024-05-03 16:51:56', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(162, '交通', 'jiaotong', 161, 'subsidy', '补助类型', '2024-05-03 16:52:35', '2024-05-03 16:52:35', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(163, '住房', 'zhufang', 161, 'subsidy', '补助类型', '2024-05-03 16:53:44', '2024-05-03 16:53:44', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(164, '高温', 'gaowen', 161, 'subsidy', '补助类型', '2024-05-03 16:54:37', '2024-05-03 16:54:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(165, '节日', 'jieri', 161, 'subsidy', '补助类型', '2024-05-03 16:54:49', '2024-05-03 16:54:49', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(166, '新人', 'xinren', 161, 'subsidy', '补助类型', '2024-05-03 16:55:08', '2024-05-03 16:55:08', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(167, '旅行', 'lvxing', 161, 'subsidy', '补助类型', '2024-05-03 16:55:41', '2024-05-03 16:55:41', 1);
-- 用印类型相关数据
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(168, '用印类型', 'sealListType', 0, 'seal', '用印类型', '2024-05-03 19:06:05', '2024-05-03 19:06:05', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(169, '人事专用印章', 'renshizhuanyong', 168, 'seal', '用印类型', '2024-05-03 19:06:32', '2024-05-03 19:06:59', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(170, '财务专用章', 'caiwuzhuanyong', 168, 'seal', '用印类型', '2024-05-03 19:07:37', '2024-05-03 19:07:37', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(171, '行政专用章', 'xingzhengzhuanyong', 168, 'seal', '用印类型', '2024-05-03 19:07:53', '2024-05-03 19:07:53', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(172, '党委印章', 'dangwei', 168, 'seal', '用印类型', '2024-05-03 19:08:20', '2024-05-03 19:08:20', 1);
INSERT INTO dict_common
(id, name, abbreviation, parent_id, `type`, remarks, create_time, modified_time, is_enable)
VALUES(173, '公司印章', 'gongsi', 168, 'seal', '用印类型', '2024-05-03 19:08:32', '2024-05-03 19:08:32', 1);
-- 组织架构表
CREATE TABLE `tb_organization` (
                                   `id` bigint NOT NULL AUTO_INCREMENT COMMENT '组织架构id',
                                   `parent_id` bigint DEFAULT NULL COMMENT '父级id',
                                   `name` varchar(255) NOT NULL COMMENT '组织名称',
                                   `abbreviation` varchar(255) NOT NULL COMMENT '简称',
                                   `type_id` bigint NOT NULL COMMENT '组织类型id(来自通用字典数据)',
                                   `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用(0,否;1,是)',
                                   `create_time` datetime DEFAULT NULL COMMENT '创建日期',
                                   `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `tb_organization_unique` (`abbreviation`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb3 COMMENT='组织架构表';
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(1, 0, '中大科技有限公司', '0', 128, 1, '2024-03-21 19:45:43', '2024-04-26 18:01:36');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(2, 1, '研发中心', '1', 129, 1, '2024-04-26 18:56:44', '2024-04-26 18:56:44');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(3, 1, '产品中心', '2', 129, 1, '2024-04-26 18:58:55', '2024-04-26 18:58:55');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(4, 1, '市场中心', '3', 129, 1, '2024-04-26 18:59:18', '2024-04-26 18:59:18');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(5, 1, '管理中心', '4', 129, 1, '2024-04-26 18:59:29', '2024-04-26 20:31:03');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(6, 2, '软件研发部', '11', 130, 1, '2024-04-26 19:01:20', '2024-04-26 19:01:20');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(7, 2, '网络运维部', '12', 130, 1, '2024-04-26 19:01:36', '2024-04-26 19:01:36');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(8, 2, '自动化部', '13', 130, 1, '2024-04-26 19:01:59', '2024-04-26 19:01:59');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(9, 3, '产品研发部', '21', 130, 1, '2024-04-26 19:02:55', '2024-04-26 19:02:55');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(10, 3, '产品生产部', '22', 130, 1, '2024-04-26 20:18:39', '2024-04-26 20:18:39');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(11, 4, '市场营销部', '31', 130, 1, '2024-04-26 20:19:25', '2024-04-26 20:19:25');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(12, 4, '市场分析部', '32', 130, 1, '2024-04-26 20:19:37', '2024-04-26 20:19:37');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(13, 4, '法务部', '33', 130, 1, '2024-04-26 20:19:56', '2024-04-26 20:19:56');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(14, 5, '人事部', '41', 130, 1, '2024-04-26 20:20:13', '2024-04-26 20:20:13');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(15, 5, '行政部', '42', 130, 1, '2024-04-26 20:20:31', '2024-04-26 20:20:31');
INSERT INTO tb_organization
(id, parent_id, name, abbreviation, type_id, is_enable, create_time, modified_time)
VALUES(16, 5, '财务部', '43', 130, 1, '2024-04-26 20:20:41', '2024-04-26 20:20:41');
-- 岗位表
CREATE TABLE `tb_post` (
                           `id` bigint NOT NULL AUTO_INCREMENT COMMENT '岗位id',
                           `organization_id` bigint NOT NULL COMMENT '组织id',
                           `name` varchar(255) NOT NULL COMMENT '岗位名称',
                           `abbreviation` varchar(255) NOT NULL COMMENT '岗位简称',
                           `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用(0,否;1,是)',
                           `create_time` datetime DEFAULT NULL COMMENT '创建日期',
                           `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `tb_post_unique` (`abbreviation`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb3 COMMENT='岗位表';
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(1, 1, '系统管理员', '0', 1, '2024-03-21 19:45:43', '2024-04-28 02:38:41');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(2, 6, '服务器研发工程师', '111', 1, '2024-04-28 01:39:18', '2024-04-29 15:50:23');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(3, 6, '大前端开发工程师', '112', 1, '2024-04-28 01:40:59', '2024-04-29 15:50:28');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(4, 1, '总经理', '11', 1, '2024-04-29 15:42:09', '2024-04-29 15:56:05');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(5, 2, '研发中心经理', '21', 1, '2024-04-29 15:43:51', '2024-04-29 15:43:51');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(6, 3, '产品中心经理', '22', 1, '2024-04-29 15:44:46', '2024-04-29 15:44:46');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(7, 4, '市场中心经理', '23', 1, '2024-04-29 15:45:01', '2024-04-29 15:45:01');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(8, 5, '管理中心经理', '24', 1, '2024-04-29 15:45:17', '2024-04-29 15:45:17');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(9, 6, '算法工程师', '113', 1, '2024-04-29 15:49:06', '2024-04-29 15:50:33');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(10, 6, 'AI大模型开发', '114', 1, '2024-04-29 15:49:44', '2024-04-29 15:50:38');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(11, 7, '网络安全防护', '121', 1, '2024-04-29 15:50:47', '2024-04-29 15:50:47');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(12, 7, '系统运维与部署', '122', 1, '2024-04-29 15:51:10', '2024-04-29 15:51:10');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(13, 7, '信息通讯构建', '123', 1, '2024-04-29 15:51:49', '2024-04-29 15:51:49');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(14, 8, '机械自动化工程师', '131', 1, '2024-04-29 15:52:24', '2024-04-29 15:52:24');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(15, 8, '电气自动化工程师', '132', 1, '2024-04-29 15:52:38', '2024-04-29 15:52:38');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(16, 8, '智能机械工程师', '133', 1, '2024-04-29 15:52:56', '2024-04-29 15:52:56');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(17, 6, '软件研发部门经理', '110', 1, '2024-04-29 15:53:50', '2024-04-29 15:53:50');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(18, 7, '网络运维部门经理', '120', 1, '2024-04-29 15:54:10', '2024-04-29 15:54:10');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(19, 8, '智能设备设计工程师', '134', 1, '2024-04-29 15:54:46', '2024-04-29 15:54:46');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(20, 8, '自动化部门经理', '130', 1, '2024-04-29 15:55:01', '2024-04-29 15:55:01');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(21, 9, '产品设计', '211', 1, '2024-04-29 15:57:05', '2024-04-29 15:57:05');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(22, 9, '产品分析与优化', '212', 1, '2024-04-29 15:57:35', '2024-04-29 15:57:35');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(23, 9, '产品升级与改造', '213', 1, '2024-04-29 15:58:00', '2024-04-29 15:58:00');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(24, 9, '产品研发部门经理', '210', 1, '2024-04-29 15:58:18', '2024-04-29 15:58:23');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(25, 10, '产品生产对接', '221', 1, '2024-04-29 15:58:45', '2024-04-29 15:58:45');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(26, 10, '产品生产监督', '222', 1, '2024-04-29 15:58:56', '2024-04-29 15:58:56');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(27, 10, '产品生产服务', '223', 1, '2024-04-29 15:59:25', '2024-04-29 15:59:25');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(28, 10, '产品生产部门经理', '220', 1, '2024-04-29 15:59:41', '2024-04-29 15:59:41');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(29, 11, '客户经理', '311', 1, '2024-04-29 16:00:33', '2024-04-29 16:00:33');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(30, 11, '产品销售员', '312', 1, '2024-04-29 16:00:56', '2024-04-29 16:00:56');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(31, 11, '产品扩展团队', '313', 1, '2024-04-29 16:01:24', '2024-04-29 16:01:24');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(32, 11, '产品品牌运营', '314', 1, '2024-04-29 16:01:34', '2024-04-29 16:01:34');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(33, 11, '线上销售操作员', '315', 1, '2024-04-29 16:01:50', '2024-04-29 16:01:50');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(34, 11, '推广员', '316', 1, '2024-04-29 16:02:04', '2024-04-29 16:02:04');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(35, 11, '市场营销部门经理', '310', 1, '2024-04-29 16:02:27', '2024-04-29 16:02:27');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(36, 12, '产品市场调研', '321', 1, '2024-04-29 16:03:04', '2024-04-29 16:03:04');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(37, 12, '市场驻派监督', '322', 1, '2024-04-29 16:03:34', '2024-04-29 16:03:34');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(38, 12, '市场动向与分析', '323', 1, '2024-04-29 16:03:54', '2024-04-29 16:03:54');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(39, 12, '市场分析部门经理', '320', 1, '2024-04-29 16:04:11', '2024-04-29 16:04:11');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(40, 13, '法务书记', '331', 1, '2024-04-29 16:04:29', '2024-04-29 16:04:29');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(41, 13, '法务员', '332', 1, '2024-04-29 16:04:38', '2024-04-29 16:04:38');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(42, 13, '法务派遣', '333', 1, '2024-04-29 16:05:22', '2024-04-29 16:05:22');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(43, 13, '法务部门经理', '330', 1, '2024-04-29 16:05:34', '2024-04-29 16:05:41');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(44, 14, '人事主管', '411', 1, '2024-04-29 16:06:40', '2024-04-29 16:42:00');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(45, 14, '人事经理', '412', 1, '2024-04-29 16:06:51', '2024-04-29 16:42:07');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(46, 14, '人事部门经理', '410', 0, '2024-04-29 16:07:11', '2024-04-29 16:42:23');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(47, 15, '行政主管', '421', 1, '2024-04-29 16:34:53', '2024-04-29 16:36:46');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(48, 15, '行政经理', '422', 1, '2024-04-29 16:35:08', '2024-04-29 16:41:31');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(49, 15, '行政部经理', '420', 0, '2024-04-29 16:35:25', '2024-04-29 16:42:40');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(50, 16, '财务主管', '431', 1, '2024-04-29 16:36:00', '2024-04-29 16:36:00');
INSERT INTO tb_post
(id, organization_id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(51, 16, '财务经理', '432', 1, '2024-04-29 16:36:28', '2024-04-29 16:36:28');
-- 用户表
CREATE TABLE `tb_user` (
                           `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户id',
                           `avater` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci COMMENT '证件照路径',
                           `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '姓名',
                           `sex_id` bigint DEFAULT NULL COMMENT '性别',
                           `birthday` datetime DEFAULT NULL COMMENT '生日',
                           `idcard` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '身份证号',
                           `ethnic_id` bigint DEFAULT NULL COMMENT '民族id(来自通用字典数据）',
                           `degree_id` bigint DEFAULT NULL COMMENT '学历id(来自通用字典数据)',
                           `graduate` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '毕业院校',
                           `identity_id` bigint DEFAULT NULL COMMENT '身份id(来自通用字典数据）',
                           `domicile_id` bigint DEFAULT NULL COMMENT '户籍地id(来自通用字典数据)',
                           `address` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '家庭住址',
                           `account` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '账户（也就是用户的国内手机号)',
                           `password` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '密码',
                           `organization_id` bigint DEFAULT NULL COMMENT '组织id',
                           `job_id` bigint DEFAULT NULL COMMENT '岗位id(来自于岗位表）',
                           `e_mail` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '电子邮箱',
                           `vx` varchar(255) DEFAULT NULL COMMENT '微信号',
                           `contact` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '其他联系方式',
                           `description` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '自我介绍',
                           `motto` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '座右铭',
                           `is_registration` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否登记',
                           `is_incumbency` tinyint(1) DEFAULT NULL COMMENT '是否离职（0，离职；1，在职）',
                           `create_time` datetime DEFAULT NULL COMMENT '入职日期',
                           `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
                           `out_time` datetime DEFAULT NULL COMMENT '离职时间',
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `tb_user_unique` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3 COMMENT='用户表';
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(1, 'https://ts1.cn.mm.bing.net/th/id/R-C.d0691ba5924a0d89c53d8f9a967d9915?rik=tyCZ4vU2mq9MBw&riu=http%3a%2f%2fbpic.588ku.com%2felement_pic%2f17%2f09%2f25%2f82c236d84cde152b2f2cd5f40de05d5c.jpg&ehk=CD3VEhzWlMqpGyspIshgmkyqw46IDDFmAkW0t%2bo0CD4%3d&risl=&pid=ImgRaw&r=0', '管理员', 125, '2002-02-01 15:13:50', '530328200202010011', 2, 66, '昆明理工大学', 71, 105, '曲靖市', '13114206921', 'E2AF21E240A250D3B31720F79673ED3A', 1, 1, '2148675886@qq.com', '13114206921', 'QQ:13114206921', '中大科技有限公司系统管理员', '系统', 1, 0, '2024-05-05 14:59:50', '2024-05-18 19:25:21', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(2, 'https://tupian.qqw21.com/article/UploadPic/2021-2/202121219531349614.jpg', '朱垠熹', 125, '2002-02-22 15:07:03', '530328200202220013', 2, 66, '昆明理工大学', 71, 105, '曲靖市', '18182970400', 'AC266D1A69CB678E7F0F507D88AA5C31', 1, 4, '2148675886@qq.com', '18182970400', 'QQ:2148675886', '中大科技有限公司总经理', '勤勉', 1, 0, '2024-05-05 15:03:14', '2024-05-05 15:12:48', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(3, 'https://tupian.qqw21.com/article/UploadPic/2022-12/2022121722404931382.jpg', '李梦', 126, '2003-03-03 15:21:59', '530328200303030011', 3, 66, '昆明理工大学', 71, 105, '未填写', '18182970401', '2854DCD19637B84E1EA35D1F85BCE318', 5, 8, '2148675886@qq.com', '18182970401', 'QQ:limeng', '中大科技有限公司管理中心经理', '公正', 1, 0, '2024-05-05 15:20:39', '2024-05-05 15:24:00', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(4, 'https://tupian.qqw21.com/article/UploadPic/2020-6/202061022593139233.jpg', '黄磊权', 125, '2003-05-05 15:33:17', '530328200305050011', 2, 66, '昆明理工大学', 71, 105, '昆明市', '18182970402', 'CC1EA682159F04BBA2C86F32A1160AB5', 14, 44, '2148675886@qq.com', '18182970402', 'QQ:yyds1123', '中大科技有限公司人事部人事主管', '公平', 1, 0, '2024-05-05 15:32:17', '2024-05-05 15:38:06', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(5, 'https://tse2-mm.cn.bing.net/th/id/OIP-C.Wmcr_rynRMy5919NKO92OQAAAA?rs=1&pid=ImgDetMain', '李晓霞', 126, '2004-03-05 15:40:33', '530328200403050012', 2, 66, '云南师范大学', 71, 105, '红河州', '18182970403', '9D32BA371BA17159924A8F34B6F393AF', 15, 47, '2148675886@qq.com', '18182970403', 'QQ:211886756', '中大科技有限公司人事部门经理', '枫叶', 1, 0, '2024-05-05 15:39:52', '2024-05-05 15:43:23', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(6, 'https://tse3-mm.cn.bing.net/th/id/OIP-C.MxlmuC_LicwZ4Pq1rOz5mwAAAA?rs=1&pid=ImgDetMain', '李华', 125, '2003-03-05 15:46:51', '530328200303050016', 2, 66, '昆明理工大学', 71, 105, '普洱市', '18182970404', '091F8C7BCCE2DEBC8584BE26FA64E3A8', 16, 50, '2148675886@qq.com', '18182970404', 'QQ:21188675663', '中大科技有限公司财务主管', '公开', 1, 0, '2024-05-05 15:45:05', '2024-05-05 15:48:56', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(7, 'https://tse1-mm.cn.bing.net/th/id/OIP-C.NjqntlZs7IvdxLxef3Cd1wAAAA?rs=1&pid=ImgDetMain', '李菲', 126, '2003-05-05 16:09:30', '530328200303050016', 2, 66, '昆明理工大学', 71, 105, '普洱市', '18182970405', '43BAD95D8D52189F2A041F362A774754', 14, 45, '2148675886@qq.com', '18182970405', 'QQ:21766535568', '中大科技有限公司人事经理', '公工', 1, 0, '2024-05-05 16:00:19', '2024-05-05 16:12:57', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(8, 'https://tse3-mm.cn.bing.net/th/id/OIP-C.Dk_zEzcLGUFoI_ghpcaAJAAAAA?rs=1&pid=ImgDetMain', '朱可莱', 125, '2003-04-05 16:17:20', '530328200304050012', 2, 66, '昆明理工大学', 71, 105, '普洱市', '18182970406', '735481E90005B9FC696A659798F1FEC0', 2, 5, '2148675886@qq.com', '18182970406', 'QQ:21766535534', '中大科技有限公司研发部经理', '时代在召唤', 1, 0, '2024-05-05 16:16:21', '2024-05-05 16:19:41', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(9, 'https://tse2-mm.cn.bing.net/th/id/OIP-C.GvOTQndA7WXbfLVolbO60wAAAA?w=194&h=194&c=7&r=0&o=5&dpr=1.3&pid=1.7', '李淑华', 126, '2000-05-05 16:22:11', '530328200005050016', 2, 66, '昆明理工大学', 71, 105, '曲靖市', '18182970407', '3CD05F04148E039660623D58E6213FF1', 6, 17, '2148675886@qq.com', '18182970407', 'QQ:21188675563', '中大科技有限公司软件研发部经理', '好人', 1, 0, '2024-05-05 16:21:19', '2024-05-05 16:26:02', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(10, 'https://tupian.qqw21.com/article/UploadPic/2020-5/20205311785681183.jpg', '黄得以', 125, '2003-04-05 16:28:19', '530328200304050011', 2, 66, '昆明理工大学', 72, 105, '腾冲市', '18182970408', 'E31863A7FED12C0B6112EFCA38753D4E', 6, 2, '2148675886@qq.com', '18182970408', 'QQ:12233456637', '中大科技有限公司服务器研发工程师', '好帅', 1, 0, '2024-05-05 16:27:13', '2024-05-05 16:31:39', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(11, 'https://tse2-mm.cn.bing.net/th/id/OIP-C.1Joxz7PVloIzSj_SDOnt1wAAAA?w=216&h=215&c=7&r=0&o=5&dpr=1.3&pid=1.7', '李大哥', 125, '2003-05-05 16:34:44', '530328200305050018', 2, 66, '昆明理工大学', 72, 105, '红河州', '18182970409', '73FEBEFCDED64212392517288A902335', 7, 18, '2148675886@qq.com', '18182970409', 'QQ:113688675563', '中大科技有限公司网络运维经理', '好吃好喝', 1, 0, '2024-05-05 16:33:26', '2024-05-05 16:37:06', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(12, 'https://tse3-mm.cn.bing.net/th/id/OIP-C.QxwEaz3b0M9gXB8lSTsvCwAAAA?rs=1&pid=ImgDetMain', '陈梦雪', 126, '2003-04-05 16:38:41', '530328200304050010', 2, 66, '昆明理工大学', 71, 105, '曲靖市', '18182970410', 'AE2991D9B5133559F3332377ECC2D168', 6, 2, '2148675886@qq.com', '18182970410', 'QQ:235593', '中大科技有限公司网络安全防护', '晚安', 1, 0, '2024-05-05 16:37:59', '2024-05-18 21:01:45', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(13, 'https://tse3-mm.cn.bing.net/th/id/OIP-C.SOzTT3NPKKuirvXskSchbwAAAA?pid=ImgDet&w=60&h=60&c=7&dpr=1.3&rs=1', '李学凌', 125, '2003-05-19 00:27:45', '530328200305190017', 2, 66, '云南民族大学', 71, 105, '红河州', '18182970411', 'E4C6CB7302DE0DA04D999EBCBEC7606A', 6, 2, '2148675886@qq.com', '18182970411', 'QQ：54321', '开发', '开发', 0, 1, '2024-05-19 00:25:18', '2024-05-19 00:31:00', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(14, 'https://tupian.qqw21.com/article/UploadPic/2020-6/202062016282113834.jpg', '李孟豆', 125, '2003-05-19 01:29:02', '530328200305190013', 2, 66, '昆明理工大学', 71, 105, '曲靖市', '18182970412', 'A425654B6579E03C970CE0A762CFDFB5', 7, 12, '2148675886@qq.com', '1183', 'QQ:12345678', '11111', '11111', 0, 1, '2024-05-19 00:25:51', '2024-05-19 01:31:45', NULL);
INSERT INTO tb_user
(id, avater, name, sex_id, birthday, idcard, ethnic_id, degree_id, graduate, identity_id, domicile_id, address, account, password, organization_id, job_id, e_mail, vx, contact, description, motto, is_registration, is_incumbency, create_time, modified_time, out_time)
VALUES(15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '18182970413', 'A043B09AFC02920F503411415666CC2D', 8, 14, '2148675886@qq.com', NULL, NULL, NULL, NULL, 0, 0, '2024-05-19 00:26:16', '2024-05-19 00:26:16', NULL);
-- 角色表
CREATE TABLE `tb_role` (
                           `id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色id',
                           `name` varchar(255) NOT NULL COMMENT '角色名称',
                           `abbreviation` varchar(255) NOT NULL COMMENT '简称',
                           `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用(0,否;1,是)',
                           `create_time` datetime DEFAULT NULL COMMENT '创建日期',
                           `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `tb_role_unique` (`abbreviation`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3 COMMENT='角色表(职级表)';
INSERT INTO tb_role
(id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(1, '系统管理员', 'system', 1, '2024-03-21 18:30:12', '2024-04-28 14:35:54');
INSERT INTO tb_role
(id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(2, '实习生', 'intern', 1, '2024-03-21 18:30:17', '2024-03-21 18:30:20');
INSERT INTO tb_role
(id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(3, '普通员工', 'employee', 1, '2024-03-21 18:31:07', '2024-03-21 18:31:09');
INSERT INTO tb_role
(id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(4, '部门经理', 'department', 1, '2024-03-21 18:31:43', '2024-03-21 18:31:45');
INSERT INTO tb_role
(id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(5, '中心经理', 'centre', 1, '2024-03-21 18:32:08', '2024-03-21 18:32:11');
INSERT INTO tb_role
(id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(6, '副总经理', 'fugeneral', 0, '2024-03-21 18:33:12', '2024-04-30 23:55:34');
INSERT INTO tb_role
(id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(7, '总经理', 'general', 1, '2024-03-21 18:33:31', '2024-03-21 18:33:33');
INSERT INTO tb_role
(id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(8, '人事', 'hr', 1, '2024-03-21 18:34:07', '2024-03-21 18:34:09');
INSERT INTO tb_role
(id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(9, '行政', 'administrative', 1, '2024-03-21 18:34:38', '2024-03-21 18:34:40');
INSERT INTO tb_role
(id, name, abbreviation, is_enable, create_time, modified_time)
VALUES(10, '财务', 'finance', 1, '2024-03-21 18:35:06', '2024-03-21 18:35:09');
-- 用户-角色表
CREATE TABLE `tb_user_role` (
                                `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                `user_id` bigint NOT NULL COMMENT '用户id',
                                `role_id` bigint NOT NULL COMMENT '角色id',
                                `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
                                `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `tb_user_role_unique` (`user_id`,`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3 COMMENT='用户-角色表';
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(1, 1, 1, 1, '2024-05-05 14:59:50', '2024-05-05 14:59:50');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(2, 2, 7, 1, '2024-05-05 15:03:14', '2024-05-05 15:03:14');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(3, 3, 5, 1, '2024-05-05 15:20:39', '2024-05-05 15:20:39');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(4, 3, 8, 1, '2024-05-05 15:30:55', '2024-05-05 15:30:55');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(5, 4, 4, 1, '2024-05-05 15:32:17', '2024-05-05 15:32:17');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(6, 4, 8, 1, '2024-05-05 15:32:17', '2024-05-05 15:32:17');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(7, 5, 9, 1, '2024-05-05 15:39:52', '2024-05-05 15:39:52');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(8, 5, 4, 1, '2024-05-05 15:39:52', '2024-05-05 15:39:52');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(9, 6, 10, 1, '2024-05-05 15:45:05', '2024-05-05 15:45:05');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(10, 6, 4, 1, '2024-05-05 15:45:05', '2024-05-05 15:45:05');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(11, 7, 8, 1, '2024-05-05 16:00:19', '2024-05-05 16:00:19');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(12, 7, 3, 1, '2024-05-05 16:00:19', '2024-05-05 16:00:19');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(13, 8, 5, 1, '2024-05-05 16:16:21', '2024-05-05 16:16:21');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(14, 9, 4, 1, '2024-05-05 16:21:19', '2024-05-05 16:21:19');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(15, 10, 3, 1, '2024-05-05 16:27:13', '2024-05-05 16:27:13');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(16, 11, 4, 1, '2024-05-05 16:33:26', '2024-05-05 16:33:26');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(17, 12, 3, 1, '2024-05-05 16:37:59', '2024-05-05 16:37:59');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(18, 13, 2, 1, '2024-05-19 00:25:18', '2024-05-19 00:25:18');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(19, 14, 2, 1, '2024-05-19 00:25:51', '2024-05-19 00:25:51');
INSERT INTO tb_user_role
(id, user_id, role_id, is_enable, create_time, modified_time)
VALUES(20, 15, 2, 1, '2024-05-19 00:26:16', '2024-05-19 00:26:16');
-- 权限表
CREATE TABLE `tb_permission` (
                                 `id` bigint NOT NULL AUTO_INCREMENT COMMENT '权限id',
                                 `name` varchar(255) NOT NULL COMMENT '权限名称',
                                 `code` varchar(255) NOT NULL COMMENT '权限代码',
                                 `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用(0,否;1,是）',
                                 `create_time` datetime DEFAULT NULL COMMENT '创建日期',
                                 `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `tb_permission_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COMMENT='权限表';
INSERT INTO tb_permission
(id, name, code, is_enable, create_time, modified_time)
VALUES(1, '注册用户', 'user:register', 1, '2024-04-28 14:11:17', '2024-04-28 14:39:47');
INSERT INTO tb_permission
(id, name, code, is_enable, create_time, modified_time)
VALUES(2, '登出用户', 'user:setLogout', 1, '2024-04-28 16:52:20', '2024-05-18 18:04:36');
INSERT INTO tb_permission
(id, name, code, is_enable, create_time, modified_time)
VALUES(3, '审批中心', 'approval:operation', 1, '2024-04-30 23:54:06', '2024-05-18 18:07:29');
INSERT INTO tb_permission
(id, name, code, is_enable, create_time, modified_time)
VALUES(4, '指派操作', 'admin:assign', 1, '2024-05-01 04:17:09', '2024-05-18 18:05:02');
INSERT INTO tb_permission
(id, name, code, is_enable, create_time, modified_time)
VALUES(5, '角色操作', 'role:set', 1, '2024-05-01 04:25:27', '2024-05-18 18:05:10');
INSERT INTO tb_permission
(id, name, code, is_enable, create_time, modified_time)
VALUES(6, '工资操作', 'document:setSalary', 1, '2024-05-04 10:14:32', '2024-05-18 18:05:26');
INSERT INTO tb_permission
(id, name, code, is_enable, create_time, modified_time)
VALUES(7, '工作操作', 'work:setArrange', 1, '2024-05-05 15:58:07', '2024-05-18 18:05:44');
INSERT INTO tb_permission
(id, name, code, is_enable, create_time, modified_time)
VALUES(8, '考勤统计', 'attendance:statistics', 1, '2024-05-18 17:50:23', '2024-05-18 17:50:23');
INSERT INTO tb_permission
(id, name, code, is_enable, create_time, modified_time)
VALUES(9, '重置密码', 'user:resetPassword', 1, '2024-05-18 17:54:02', '2024-05-18 17:54:14');
INSERT INTO tb_permission
(id, name, code, is_enable, create_time, modified_time)
VALUES(10, '员工管理', 'user:admin', 1, '2024-05-18 18:06:57', '2024-05-18 18:06:57');
INSERT INTO tb_permission
(id, name, code, is_enable, create_time, modified_time)
VALUES(13, '在职统计', 'user:isIn', 1, '2024-05-19 01:19:54', '2024-05-19 01:19:54');
-- 角色-权限表
CREATE TABLE `tb_role_permission` (
                                      `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                      `role_id` bigint NOT NULL COMMENT '角色id',
                                      `permission_id` bigint NOT NULL COMMENT '权限id',
                                      `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
                                      `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                      `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `tb_role_permission_unique` (`role_id`,`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb3 COMMENT='角色-权限表';
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(1, 1, 1, 1, '2024-05-18 18:28:27', '2024-05-18 18:28:27');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(2, 1, 2, 1, '2024-05-18 18:28:27', '2024-05-18 18:28:27');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(3, 1, 3, 1, '2024-05-18 18:28:27', '2024-05-18 18:28:27');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(4, 1, 4, 1, '2024-05-18 18:28:27', '2024-05-18 18:28:27');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(5, 1, 5, 1, '2024-05-18 18:28:27', '2024-05-18 18:28:27');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(6, 1, 6, 1, '2024-05-18 18:28:27', '2024-05-18 18:28:27');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(7, 1, 7, 1, '2024-05-18 18:28:27', '2024-05-18 18:28:27');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(8, 1, 8, 1, '2024-05-18 18:28:27', '2024-05-18 18:28:27');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(9, 1, 9, 1, '2024-05-18 18:28:27', '2024-05-18 18:28:27');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(10, 1, 10, 1, '2024-05-18 18:28:27', '2024-05-18 18:28:27');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(11, 7, 1, 1, '2024-05-18 18:28:33', '2024-05-18 18:28:33');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(12, 7, 2, 1, '2024-05-18 18:28:33', '2024-05-18 18:28:33');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(13, 7, 3, 1, '2024-05-18 18:28:33', '2024-05-18 18:28:33');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(14, 7, 4, 1, '2024-05-18 18:28:33', '2024-05-18 18:28:33');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(15, 7, 5, 1, '2024-05-18 18:28:33', '2024-05-18 18:28:33');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(16, 7, 6, 1, '2024-05-18 18:28:33', '2024-05-18 18:28:33');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(17, 7, 7, 1, '2024-05-18 18:28:33', '2024-05-18 18:28:33');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(18, 7, 8, 1, '2024-05-18 18:28:33', '2024-05-18 18:28:33');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(19, 7, 9, 1, '2024-05-18 18:28:33', '2024-05-18 18:28:33');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(20, 7, 10, 1, '2024-05-18 18:28:33', '2024-05-18 18:28:33');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(21, 5, 3, 1, '2024-05-18 18:32:06', '2024-05-18 18:32:06');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(22, 5, 7, 1, '2024-05-18 18:32:06', '2024-05-18 18:32:06');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(23, 5, 10, 1, '2024-05-18 18:32:06', '2024-05-18 18:32:06');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(24, 5, 8, 1, '2024-05-18 18:32:06', '2024-05-18 18:32:06');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(25, 8, 1, 1, '2024-05-18 18:32:21', '2024-05-18 18:32:21');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(26, 8, 2, 1, '2024-05-18 18:32:21', '2024-05-18 18:32:21');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(27, 8, 3, 1, '2024-05-18 18:32:21', '2024-05-18 18:32:21');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(28, 8, 4, 1, '2024-05-18 18:32:21', '2024-05-18 18:32:21');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(29, 8, 5, 1, '2024-05-18 18:32:21', '2024-05-18 18:32:21');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(30, 8, 8, 1, '2024-05-18 18:32:21', '2024-05-18 18:32:21');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(31, 8, 9, 1, '2024-05-18 18:32:21', '2024-05-18 18:32:21');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(32, 8, 10, 1, '2024-05-18 18:32:21', '2024-05-18 18:32:21');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(33, 9, 3, 1, '2024-05-18 18:34:13', '2024-05-18 18:34:13');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(34, 9, 10, 1, '2024-05-18 18:34:13', '2024-05-18 18:34:13');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(35, 10, 10, 1, '2024-05-18 18:34:57', '2024-05-18 18:34:57');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(36, 10, 6, 1, '2024-05-18 18:34:57', '2024-05-18 18:34:57');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(37, 10, 3, 1, '2024-05-18 18:34:57', '2024-05-18 18:34:57');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(38, 4, 3, 1, '2024-05-18 18:35:46', '2024-05-18 18:35:46');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(39, 4, 10, 1, '2024-05-18 18:35:46', '2024-05-18 18:35:46');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(40, 4, 8, 1, '2024-05-18 18:35:46', '2024-05-18 18:35:46');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(41, 4, 7, 1, '2024-05-18 18:35:46', '2024-05-18 18:35:46');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(42, 1, 13, 1, '2024-05-19 01:20:03', '2024-05-19 01:20:03');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(43, 7, 13, 1, '2024-05-19 01:20:08', '2024-05-19 01:20:08');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(44, 5, 13, 1, '2024-05-19 01:20:17', '2024-05-19 01:20:17');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(45, 4, 13, 1, '2024-05-19 01:20:22', '2024-05-19 01:20:22');
INSERT INTO tb_role_permission
(id, role_id, permission_id, is_enable, create_time, modified_time)
VALUES(46, 8, 13, 1, '2024-05-19 01:20:30', '2024-05-19 01:20:30');
-- 管理表
CREATE TABLE `tb_admin` (
                            `id` bigint NOT NULL AUTO_INCREMENT COMMENT '管理id',
                            `admin_id` bigint NOT NULL COMMENT '管理者id',
                            `organization_id` bigint NOT NULL COMMENT '管理组织id',
                            `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
                            `create_time` datetime DEFAULT NULL COMMENT '创建日期',
                            `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `tb_admin_unique` (`admin_id`,`organization_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb3 COMMENT='管理表';
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(1, 1, 1, 1, '2024-05-05 14:59:50', '2024-05-05 14:59:50');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(2, 2, 1, 1, '2024-05-05 15:11:29', '2024-05-05 15:11:29');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(3, 2, 5, 1, '2024-05-05 15:11:39', '2024-05-05 15:11:39');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(4, 3, 14, 1, '2024-05-05 15:29:44', '2024-05-05 15:29:44');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(5, 3, 15, 1, '2024-05-05 15:29:51', '2024-05-05 15:29:51');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(6, 3, 16, 1, '2024-05-05 15:29:56', '2024-05-05 15:29:56');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(7, 4, 14, 1, '2024-05-05 15:38:37', '2024-05-05 15:38:37');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(8, 5, 15, 1, '2024-05-05 15:43:50', '2024-05-05 15:43:50');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(9, 6, 16, 1, '2024-05-05 15:49:14', '2024-05-05 15:49:14');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(10, 7, 2, 1, '2024-05-05 16:15:26', '2024-05-05 16:15:26');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(11, 7, 6, 1, '2024-05-05 16:15:32', '2024-05-05 16:15:32');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(12, 8, 6, 1, '2024-05-05 16:20:01', '2024-05-05 16:20:01');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(13, 8, 7, 1, '2024-05-05 16:20:11', '2024-05-05 16:20:11');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(14, 8, 2, 1, '2024-05-05 16:20:25', '2024-05-05 16:20:25');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(15, 8, 8, 1, '2024-05-05 16:20:32', '2024-05-05 16:20:32');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(16, 9, 6, 1, '2024-05-05 16:26:25', '2024-05-05 16:26:25');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(17, 7, 7, 1, '2024-05-05 16:34:13', '2024-05-05 16:34:13');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(18, 7, 8, 1, '2024-05-05 16:34:18', '2024-05-05 16:34:18');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(19, 11, 7, 1, '2024-05-05 16:37:19', '2024-05-05 16:37:19');
INSERT INTO tb_admin
(id, admin_id, organization_id, is_enable, create_time, modified_time)
VALUES(20, 3, 5, 1, '2024-05-18 18:49:47', '2024-05-18 18:49:47');
-- 审批表
CREATE TABLE `tb_approval` (
                               `id` bigint NOT NULL AUTO_INCREMENT COMMENT '审批id',
                               `apply_id` bigint NOT NULL COMMENT '申请者id',
                               `approval_id` bigint NOT NULL COMMENT '审批者id',
                               `approval_type` bigint NOT NULL COMMENT '审批类型id',
                               `certificate` text COMMENT '相关附件路径',
                               `apply_reason` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '申请说明',
                               `approval_reason` varchar(255) DEFAULT NULL COMMENT '审批说明',
                               `approval_status` tinyint NOT NULL DEFAULT '0' COMMENT '审批状态（0,已提交;1,待审批;2,通过;3,未通过;4,已完成）',
                               `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可用',
                               `is_cut` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否撤销（0,否;1,是)',
                               `apply_time` datetime DEFAULT NULL COMMENT '申请日期',
                               `approval_time` datetime DEFAULT NULL COMMENT '审批日期',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3 COMMENT='审批流程表';
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(1, 2, 1, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-05 15:08:53', '2024-05-05 15:09:06');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(2, 1, 1, 132, NULL, NULL, NULL, 1, 0, 1, '2024-05-05 15:15:36', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(3, 1, 2, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-05 15:16:09', '2024-05-05 15:16:29');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(4, 3, 2, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-05 15:23:28', '2024-05-05 15:23:51');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(5, 4, 2, 132, NULL, NULL, NULL, 1, 0, 1, '2024-05-05 15:34:28', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(6, 4, 3, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-05 15:34:56', '2024-05-05 15:35:08');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(7, 5, 3, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-05 15:42:55', '2024-05-05 15:43:09');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(8, 6, 3, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-05 15:48:29', '2024-05-05 15:48:47');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(9, 7, 3, 132, NULL, NULL, NULL, 1, 0, 1, '2024-05-05 16:11:29', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(10, 7, 4, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-05 16:12:15', '2024-05-05 16:12:27');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(11, 8, 7, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-05 16:19:03', '2024-05-05 16:19:19');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(12, 9, 7, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-05 16:25:00', '2024-05-05 16:25:53');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(13, 10, 7, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-05 16:30:53', '2024-05-05 16:31:32');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(14, 11, 7, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-05 16:36:43', '2024-05-05 16:36:53');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(15, 12, 7, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-05 16:40:37', '2024-05-05 16:40:48');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(16, 12, 7, 135, '', '职位轮岗，需要转岗', NULL, 1, 0, 1, '2024-05-18 19:33:28', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(17, 12, 7, 135, 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/transfer/a9a384b578ce4aba980e168e813cd128.jpg', '职位轮岗，需要转岗', NULL, 2, 1, 0, '2024-05-18 19:33:51', '2024-05-18 19:34:05');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(18, 12, 7, 137, '', '外宿报销', NULL, 2, 0, 1, '2024-05-18 19:35:23', '2024-05-18 19:35:30');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(19, 12, 7, 138, '', '交通补助', NULL, 2, 1, 0, '2024-05-18 19:36:17', '2024-05-18 19:36:25');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(20, 12, 7, 139, NULL, '签约三方协议', NULL, 2, 1, 0, '2024-05-18 19:38:45', '2024-05-18 19:38:55');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(21, 12, 7, 133, '', '结婚', '时间太长', 3, 1, 0, '2024-05-18 19:40:14', '2024-05-18 19:40:38');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(22, 12, 7, 136, '', '外出有事', NULL, 2, 1, 0, '2024-05-18 19:41:36', '2024-05-18 19:41:52');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(23, 12, 7, 137, '', '说明', NULL, 2, 0, 1, '2024-05-18 21:22:08', '2024-05-18 21:23:07');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(24, 12, 7, 137, '', '外出报销', NULL, 2, 1, 0, '2024-05-18 21:25:20', '2024-05-18 21:25:31');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(25, 13, 7, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-19 00:29:27', '2024-05-19 00:30:13');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(26, 13, 7, 134, '', '找到工作了', NULL, 2, 1, 0, '2024-05-19 00:30:41', '2024-05-19 00:30:46');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(27, 14, 7, 132, NULL, NULL, NULL, 2, 1, 0, '2024-05-19 01:30:21', '2024-05-19 01:30:52');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(28, 14, 7, 134, '', '诗和远方', NULL, 2, 1, 0, '2024-05-19 01:31:27', '2024-05-19 01:31:37');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(29, 1, 1, 136, '', '外出有事', NULL, 1, 0, 1, '2024-05-19 01:47:07', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(30, 1, 2, 136, 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/travle/14edd604276c454fb31b03bf73fc52ff.jpg', '外出有事', NULL, 2, 0, 1, '2024-05-19 01:48:15', '2024-05-19 01:48:23');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(31, 1, 1, 136, 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/travle/1f9a2dadffd34c1986682e43c8a24460.jpg', '外出有事', NULL, 1, 0, 1, '2024-05-19 01:53:05', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(32, 1, 1, 136, 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/travle/8ac9123b3ed6494a85c45e6c7c395e6d.jpg', '外出有事', NULL, 2, 0, 1, '2024-05-19 01:53:14', '2024-05-19 01:53:46');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(33, 1, 1, 133, '', '去外滩', NULL, 1, 0, 1, '2024-05-19 01:56:54', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(34, 1, 1, 133, 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/leave/9e751a9ba6254044ade29cf4accf71c2.jpg', '去外滩', NULL, 2, 0, 1, '2024-05-19 01:57:41', '2024-05-19 01:57:55');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(35, 1, 1, 133, 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/leave/831364f538f9424993fb84d1a541d28b.jpg', '去外滩', NULL, 1, 0, 1, '2024-05-19 02:04:05', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(36, 1, 1, 133, 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/leave/dc8c628afaa4460e84433e97b6131819.jpg', '去外滩', NULL, 2, 1, 0, '2024-05-19 02:04:14', '2024-05-19 02:04:24');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(37, 1, 1, 139, NULL, '三方协议', NULL, 1, 0, 1, '2024-05-19 02:10:58', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(38, 1, 2, 139, NULL, '三方协议', NULL, 2, 0, 1, '2024-05-19 02:11:11', '2024-05-19 02:11:26');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(39, 1, 2, 139, NULL, '三方协议', NULL, 1, 0, 1, '2024-05-19 02:12:02', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(40, 1, 2, 139, NULL, '三方协议', NULL, 1, 0, 1, '2024-05-19 02:12:50', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(41, 1, 2, 139, NULL, '三方协议', NULL, 2, 0, 1, '2024-05-19 02:13:11', '2024-05-19 02:13:38');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(42, 1, 1, 138, '', '交通', NULL, 1, 0, 1, '2024-05-19 02:15:20', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(43, 1, 1, 138, 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/subsidy/6444a30d70ec4cae980595d0c24caaca.jpg', '交通', NULL, 2, 1, 0, '2024-05-19 02:15:27', '2024-05-19 02:16:16');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(44, 1, 1, 137, '', '211', NULL, 2, 1, 0, '2024-05-19 10:22:35', '2024-05-19 10:22:51');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(45, 1, 2, 139, NULL, '三方协议', NULL, 1, 0, 1, '2024-05-19 10:26:46', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(46, 1, 1, 139, NULL, '人事签到表', NULL, 2, 0, 1, '2024-05-19 10:30:12', '2024-05-19 10:32:13');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(47, 1, 1, 139, NULL, '人事签到表', NULL, 1, 0, 1, '2024-05-19 10:32:54', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(48, 1, 1, 139, NULL, '人事签到表', NULL, 1, 0, 1, '2024-05-19 10:33:47', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(49, 1, 1, 139, NULL, '人事签到表', NULL, 1, 0, 1, '2024-05-19 10:34:20', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(50, 1, 1, 139, NULL, '人事签到表', NULL, 1, 0, 1, '2024-05-19 10:34:50', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(51, 1, 1, 139, NULL, '人事签到表', NULL, 1, 0, 1, '2024-05-19 10:35:46', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(52, 1, 1, 139, NULL, '人事签到表', NULL, 1, 0, 1, '2024-05-19 10:36:25', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(53, 1, 1, 139, NULL, '人事签到表', NULL, 1, 0, 1, '2024-05-19 10:36:53', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(54, 1, 1, 139, NULL, '人事签到表', NULL, 1, 0, 1, '2024-05-19 10:37:19', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(55, 1, 1, 139, NULL, '人事签到表', NULL, 1, 0, 1, '2024-05-19 10:37:39', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(56, 1, 1, 139, NULL, '人事签到表', NULL, 2, 1, 0, '2024-05-19 10:38:50', '2024-05-19 10:38:58');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(57, 1, 1, 136, '', '外出办事', NULL, 1, 0, 1, '2024-05-19 10:55:23', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(58, 1, 1, 136, 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/travle/a060f6bb4c3945a8aad0cfd35419202f.png', '外出办事', NULL, 1, 1, 0, '2024-05-19 10:55:58', NULL);
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(59, 5, 3, 137, '', '购买后勤设备', NULL, 2, 0, 1, '2024-05-22 19:39:19', '2024-05-22 19:43:52');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(60, 5, 3, 137, '', '11111', '理由不充分，请重新提交', 3, 1, 0, '2024-05-22 19:45:00', '2024-05-22 19:45:31');
INSERT INTO tb_approval
(id, apply_id, approval_id, approval_type, certificate, apply_reason, approval_reason, approval_status, is_enable, is_cut, apply_time, approval_time)
VALUES(61, 11, 7, 137, '', '出差住宿', NULL, 1, 1, 0, '2024-05-22 22:11:05', NULL);
-- 记录表（采用分表的形式因为一些记录可能需要扩展虽然有可能是一样的）
-- 转岗记录表
CREATE TABLE `document_transfer` (
                                     `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                     `transfer_id` bigint NOT NULL COMMENT '转岗人id',
                                     `old_organization_id` bigint NOT NULL COMMENT '原组织id',
                                     `old_post_id` bigint NOT NULL COMMENT '原岗位id',
                                     `now_organization_id` bigint NOT NULL COMMENT '现组织id',
                                     `now_post_id` varchar(100) NOT NULL COMMENT '现岗位id',
                                     `reason` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '转岗理由',
                                     `resources` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci COMMENT '相关材料路径',
                                     `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可用',
                                     `log_time` datetime DEFAULT NULL COMMENT '记录时间',
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3 COMMENT='转岗记录表';
INSERT INTO document_transfer
(id, transfer_id, old_organization_id, old_post_id, now_organization_id, now_post_id, reason, resources, is_enable, log_time)
VALUES(1, 12, 7, 11, 6, '2', '职位轮岗，需要转岗', '', 1, '2024-05-18 19:33:28');
INSERT INTO document_transfer
(id, transfer_id, old_organization_id, old_post_id, now_organization_id, now_post_id, reason, resources, is_enable, log_time)
VALUES(2, 12, 7, 11, 6, '2', '职位轮岗，需要转岗', 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/transfer/a9a384b578ce4aba980e168e813cd128.jpg', 1, '2024-05-18 19:33:51');

-- 离职记录表
CREATE TABLE `document_dimission` (
                                      `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                      `user_id` bigint NOT NULL COMMENT '离职员工id',
                                      `reason` varchar(255) DEFAULT NULL COMMENT '离职原因',
                                      `resources` text COMMENT '相关材料',
                                      `out_time` datetime DEFAULT NULL COMMENT '离职时间',
                                      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COMMENT='离职记录表';
INSERT INTO document_dimission
(id, user_id, reason, resources, out_time)
VALUES(1, 13, '找到工作了', '', '2024-05-19 00:30:41');
INSERT INTO document_dimission
(id, user_id, reason, resources, out_time)
VALUES(2, 14, '诗和远方', '', '2024-05-19 01:31:26');
-- 报销记录表
CREATE TABLE `document_reimbursement` (
                                          `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                          `user_id` bigint NOT NULL COMMENT '用户id',
                                          `type_id` bigint NOT NULL COMMENT '报销类型id',
                                          `reason` varchar(255) DEFAULT NULL COMMENT '报销理由',
                                          `resources` text COMMENT '材料路径',
                                          `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
                                          `log_time` datetime DEFAULT NULL COMMENT '报销时间',
                                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb3 COMMENT='报销记录表';
INSERT INTO document_reimbursement
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(1, 12, 150, '外宿报销', '', 0, '2024-05-18 19:35:23');
INSERT INTO document_reimbursement
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(2, 12, 150, '说明', '', 0, '2024-05-18 21:22:08');
INSERT INTO document_reimbursement
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(3, 12, 150, '外出报销', '', 0, '2024-05-18 21:25:20');
INSERT INTO document_reimbursement
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(4, 1, 150, '211', '', 1, '2024-05-19 10:22:34');
INSERT INTO document_reimbursement
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(5, 5, 150, '购买后勤设备', '', 0, '2024-05-22 19:39:19');
INSERT INTO document_reimbursement
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(6, 5, 150, '11111', '', 0, '2024-05-22 19:45:00');
INSERT INTO document_reimbursement
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(7, 11, 150, '出差住宿', '', 1, '2024-05-22 22:11:05');
-- 补助记录表
CREATE TABLE `document_subsidy` (
                                    `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                    `user_id` bigint NOT NULL COMMENT '用户id',
                                    `type_id` bigint NOT NULL COMMENT '申请类型',
                                    `reason` varchar(255) DEFAULT NULL COMMENT '补助理由',
                                    `resources` text COMMENT '相关材料路径',
                                    `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
                                    `log_time` datetime DEFAULT NULL COMMENT '登记时间',
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='补助记录表';
INSERT INTO document_subsidy
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(1, 12, 162, '交通补助', '', 0, '2024-05-18 19:36:17');
INSERT INTO document_subsidy
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(2, 1, 162, '交通', '', 0, '2024-05-19 02:15:20');
INSERT INTO document_subsidy
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(3, 1, 162, '交通', 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/subsidy/6444a30d70ec4cae980595d0c24caaca.jpg', 1, '2024-05-19 02:15:27');
-- 工资记录表
CREATE TABLE `document_salary` (
                                   `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                   `user_id` bigint NOT NULL COMMENT '员工id',
                                   `basic_salary` double DEFAULT NULL COMMENT '基础工资',
                                   `performance` double DEFAULT NULL COMMENT '绩效工资',
                                   `insurances` double DEFAULT NULL COMMENT '保险缴纳',
                                   `fund` double DEFAULT NULL COMMENT '基金缴纳',
                                   `reward` double DEFAULT NULL COMMENT '奖金',
                                   `reward_reason` varchar(255) DEFAULT NULL COMMENT '奖金理由',
                                   `charge` double DEFAULT NULL COMMENT '扣款',
                                   `charge_reason` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '扣款理由',
                                   `actual` double DEFAULT NULL COMMENT '实发工资',
                                   `create_time` date NOT NULL COMMENT '发放日期',
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='工资记录表';
INSERT INTO document_salary
(id, user_id, basic_salary, performance, insurances, fund, reward, reward_reason, charge, charge_reason, actual, create_time)
VALUES(2, 12, 3000.0, 3000.0, 2000.0, 2000.0, 0.0, '', 0.0, '', 6000.0, '2024-05-07');
INSERT INTO document_salary
(id, user_id, basic_salary, performance, insurances, fund, reward, reward_reason, charge, charge_reason, actual, create_time)
VALUES(3, 1, 5000.0, 2000.0, 2000.0, 2000.0, 0.0, '', 0.0, '', 7000.0, '2024-05-19');
-- 用印记录表
CREATE TABLE `document_seal` (
                                 `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                 `user_id` bigint NOT NULL COMMENT '用户id',
                                 `type_id` bigint NOT NULL COMMENT '用印类型',
                                 `reason` varchar(255) DEFAULT NULL COMMENT '用印说明',
                                 `resources` text COMMENT '相关材料路径',
                                 `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
                                 `log_time` datetime DEFAULT NULL COMMENT '登记日期',
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COMMENT='用印记录表';
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(1, 12, 169, '签约三方协议', NULL, 0, '2024-05-18 19:38:44');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(2, 1, 169, '三方协议', NULL, 0, '2024-05-19 02:10:58');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(3, 1, 169, '三方协议', NULL, 0, '2024-05-19 02:11:11');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(4, 1, 169, '三方协议', NULL, 0, '2024-05-19 02:12:02');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(5, 1, 169, '三方协议', NULL, 0, '2024-05-19 02:12:50');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(6, 1, 169, '三方协议', NULL, 0, '2024-05-19 02:13:11');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(7, 1, 169, '三方协议', NULL, 0, '2024-05-19 10:26:46');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(8, 1, 169, '人事签到表', NULL, 0, '2024-05-19 10:30:12');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(9, 1, 169, '人事签到表', NULL, 0, '2024-05-19 10:32:54');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(10, 1, 169, '人事签到表', NULL, 0, '2024-05-19 10:33:47');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(11, 1, 169, '人事签到表', NULL, 0, '2024-05-19 10:34:20');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(12, 1, 169, '人事签到表', NULL, 0, '2024-05-19 10:34:50');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(13, 1, 169, '人事签到表', NULL, 0, '2024-05-19 10:35:46');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(14, 1, 169, '人事签到表', NULL, 0, '2024-05-19 10:36:25');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(15, 1, 169, '人事签到表', NULL, 0, '2024-05-19 10:36:53');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(16, 1, 169, '人事签到表', NULL, 0, '2024-05-19 10:37:19');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(17, 1, 169, '人事签到表', NULL, 0, '2024-05-19 10:37:39');
INSERT INTO document_seal
(id, user_id, type_id, reason, resources, is_enable, log_time)
VALUES(18, 1, 169, '人事签到表', NULL, 1, '2024-05-19 10:38:50');
-- 请假记录表
CREATE TABLE `document_leave` (
                                  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '请假记录id',
                                  `user_id` bigint NOT NULL COMMENT '用户id',
                                  `type_id` bigint NOT NULL COMMENT '请假类型',
                                  `reason` varchar(255) DEFAULT NULL COMMENT '请假说明',
                                  `resources` text COMMENT '材料证明',
                                  `start_time` datetime NOT NULL COMMENT '请假开始时间',
                                  `end_time` datetime NOT NULL COMMENT '请假结束时间',
                                  `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
                                  `create_time` date DEFAULT NULL COMMENT '创建时间',
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3 COMMENT='请假记录表';
INSERT INTO document_leave
(id, user_id, type_id, reason, resources, start_time, end_time, is_enable, create_time)
VALUES(1, 12, 159, '结婚', '', '2024-06-18 19:39:56', '2024-07-18 20:39:56', 0, '2024-05-18');
INSERT INTO document_leave
(id, user_id, type_id, reason, resources, start_time, end_time, is_enable, create_time)
VALUES(2, 1, 155, '去外滩', '', '2024-07-19 01:56:12', '2024-08-19 12:56:12', 0, '2024-05-19');
INSERT INTO document_leave
(id, user_id, type_id, reason, resources, start_time, end_time, is_enable, create_time)
VALUES(3, 1, 157, '去外滩', 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/leave/9e751a9ba6254044ade29cf4accf71c2.jpg', '2024-08-19 00:00:51', '2024-09-19 00:00:51', 0, '2024-05-19');
INSERT INTO document_leave
(id, user_id, type_id, reason, resources, start_time, end_time, is_enable, create_time)
VALUES(4, 1, 155, '去外滩', 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/leave/831364f538f9424993fb84d1a541d28b.jpg', '2024-06-01 00:00:29', '2024-07-20 03:00:29', 0, '2024-05-19');
INSERT INTO document_leave
(id, user_id, type_id, reason, resources, start_time, end_time, is_enable, create_time)
VALUES(5, 1, 155, '去外滩', 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/leave/dc8c628afaa4460e84433e97b6131819.jpg', '2024-06-01 00:00:29', '2024-07-20 03:00:29', 1, '2024-05-19');
-- 出差记录表
CREATE TABLE `document_travle` (
                                   `id` bigint NOT NULL AUTO_INCREMENT COMMENT '出差记录id',
                                   `user_id` bigint NOT NULL COMMENT '用户id',
                                   `address` varchar(255) DEFAULT NULL COMMENT '出差地点',
                                   `reason` varchar(255) DEFAULT NULL COMMENT '出差原因',
                                   `resources` text COMMENT '证明材料',
                                   `start_time` datetime NOT NULL COMMENT '开始时间',
                                   `end_time` datetime NOT NULL COMMENT '结束时间',
                                   `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
                                   `create_time` date DEFAULT NULL COMMENT '创建时间',
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='出差记录表';
INSERT INTO document_travle
(id, user_id, address, reason, resources, start_time, end_time, is_enable, create_time)
VALUES(1, 12, NULL, '外出有事', '', '2024-06-18 19:41:06', '2024-07-18 20:41:06', 0, '2024-05-18');
INSERT INTO document_travle
(id, user_id, address, reason, resources, start_time, end_time, is_enable, create_time)
VALUES(2, 1, NULL, '外出有事', '', '2024-06-19 00:00:30', '2024-07-19 00:00:30', 0, '2024-05-19');
INSERT INTO document_travle
(id, user_id, address, reason, resources, start_time, end_time, is_enable, create_time)
VALUES(3, 1, NULL, '外出有事', 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/travle/14edd604276c454fb31b03bf73fc52ff.jpg', '2024-06-19 00:00:30', '2024-07-19 00:00:30', 0, '2024-05-19');
INSERT INTO document_travle
(id, user_id, address, reason, resources, start_time, end_time, is_enable, create_time)
VALUES(4, 1, NULL, '外出有事', 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/travle/1f9a2dadffd34c1986682e43c8a24460.jpg', '2024-06-19 01:52:45', '2024-07-19 12:52:45', 0, '2024-05-19');
INSERT INTO document_travle
(id, user_id, address, reason, resources, start_time, end_time, is_enable, create_time)
VALUES(5, 1, NULL, '外出有事', 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/travle/8ac9123b3ed6494a85c45e6c7c395e6d.jpg', '2024-06-19 01:52:45', '2024-07-19 12:52:45', 0, '2024-05-19');
INSERT INTO document_travle
(id, user_id, address, reason, resources, start_time, end_time, is_enable, create_time)
VALUES(6, 1, NULL, '外出办事', '', '2024-05-26 10:55:45', '2024-05-27 10:54:45', 0, '2024-05-19');
INSERT INTO document_travle
(id, user_id, address, reason, resources, start_time, end_time, is_enable, create_time)
VALUES(7, 1, NULL, '外出办事', 'https://cloudapp-1309705113.cos.ap-beijing.myqcloud.com/travle/a060f6bb4c3945a8aad0cfd35419202f.png', '2024-05-26 10:55:45', '2024-05-27 10:54:45', 1, '2024-05-19');
-- 工作表（同样采用分表的形式进行维护，方便后期扩展）
-- 工作日报表
CREATE TABLE `work_daily` (
                              `id` bigint NOT NULL AUTO_INCREMENT COMMENT '日报主键id',
                              `user_id` bigint NOT NULL COMMENT '用户id',
                              `score` tinyint DEFAULT NULL COMMENT '评价',
                              `content` text COMMENT '日报内容',
                              `create_time` datetime DEFAULT NULL COMMENT '创建日期',
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='工作日报表';
INSERT INTO work_daily
(id, user_id, score, content, create_time)
VALUES(1, 1, NULL, '今日完成系统搭建任务，并且取得了卓越的成效', '2024-05-18 19:03:42');
INSERT INTO work_daily
(id, user_id, score, content, create_time)
VALUES(2, 12, NULL, '今日学习软件开发技能
1.开发后台系统
2.学习前台系统', '2024-05-18 19:43:35');
INSERT INTO work_daily
(id, user_id, score, content, create_time)
VALUES(3, 1, NULL, '今日完成份打卡', '2024-05-19 02:06:10');
INSERT INTO work_daily
(id, user_id, score, content, create_time)
VALUES(4, 1, NULL, '随着时代的不断发展，越来越多的人开始学习软件工程，随着AI的不断积累越来越厉害', '2024-05-19 02:07:05');
INSERT INTO work_daily
(id, user_id, score, content, create_time)
VALUES(5, 1, NULL, '时间就是金钱就是生命', '2024-05-19 02:07:24');
INSERT INTO work_daily
(id, user_id, score, content, create_time)
VALUES(6, 1, NULL, '剪辑视频了吗', '2024-05-19 02:07:34');
-- 安排工作表
CREATE TABLE `work_date` (
                             `id` bigint NOT NULL AUTO_INCREMENT COMMENT '日程安排id',
                             `user_id` bigint NOT NULL COMMENT '用户id',
                             `content` text COMMENT '安排内容',
                             `create_time` date DEFAULT NULL COMMENT '创建时间',
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='工作日程表';
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(1, 12, '1，对接客户工作
2，开发应用后台
3，继续开发平台', '2024-05-18');
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(2, 12, '去开发后台系统', '2024-05-19');
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(3, 12, '去北京出差', '2024-05-20');
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(4, 12, '去北京开会', '2024-05-21');
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(5, 12, '到北京开会', '2024-05-22');
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(6, 12, '去大理开会', '2024-05-23');
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(7, 12, '去上海开会', '2024-05-24');
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(8, 12, '打扫卫生', '2024-05-07');
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(9, 12, '去大理开会', '2024-05-25');
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(10, 1, '开展节日专项活动', '2024-06-01');
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(11, 1, '继续开展，赚钱', '2024-06-02');
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(12, 1, '小学生而已，多卖点', '2024-06-03');
INSERT INTO work_date
(id, user_id, content, create_time)
VALUES(13, 1, '1+1=3', '2024-06-04');
-- 考勤表（同样采用分表的形式进行维护，方便后期扩展）
-- 考勤地点表
CREATE TABLE `attendance_address` (
                                      `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                      `nation` varchar(255) DEFAULT NULL COMMENT '所在国家',
                                      `province` varchar(255) DEFAULT NULL COMMENT '所在省分',
                                      `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
                                      `district` varchar(255) DEFAULT NULL COMMENT '所在区县',
                                      `street` varchar(255) DEFAULT NULL COMMENT '所在街道',
                                      `street_number` varchar(255) DEFAULT NULL COMMENT '所在门牌号',
                                      `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
                                      `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
                                      PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='考勤地点表';
INSERT INTO attendance_address
(id, nation, province, city, district, street, street_number, is_enable, create_time)
VALUES(1, '中国', '云南省', '昆明市', '呈贡区', '致远路', '暂无信息', 1, '2024-05-09 20:13:52');
-- 考勤签到表
CREATE TABLE `attendance_checkin` (
                                      `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                      `user_id` bigint NOT NULL COMMENT '用户id',
                                      `nation` varchar(255) DEFAULT NULL COMMENT '所在国家',
                                      `province` varchar(255) DEFAULT NULL COMMENT '所在省份',
                                      `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
                                      `district` varchar(255) DEFAULT NULL COMMENT '所在区，县',
                                      `street` varchar(255) DEFAULT NULL COMMENT '所在街道',
                                      `street_number` varchar(255) DEFAULT NULL COMMENT '街道牌号',
                                      `status` tinyint unsigned NOT NULL COMMENT '考勤结果',
                                      `data` date NOT NULL COMMENT '签到日期',
                                      `create_time` datetime NOT NULL COMMENT '签到时间',
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `attendance_checkin_unique` (`user_id`,`data`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='考勤签到表';
INSERT INTO attendance_checkin
(id, user_id, nation, province, city, district, street, street_number, status, `data`, create_time)
VALUES(1, 2, '中国', '云南省', '昆明市', '呈贡区', '致远路', '', 1, '2024-05-20', '2024-05-20 18:39:59');
INSERT INTO attendance_checkin
(id, user_id, nation, province, city, district, street, street_number, status, `data`, create_time)
VALUES(2, 2, '中国', '云南省', '昆明市', '呈贡区', '致远路', NULL, 1, '2024-05-13', '2024-05-13 08:20:30');
INSERT INTO attendance_checkin
(id, user_id, nation, province, city, district, street, street_number, status, `data`, create_time)
VALUES(3, 2, '中国', '云南省', '昆明市', '呈贡区', '致远路', NULL, 1, '2024-05-13', '2024-05-13 18:28:59');
INSERT INTO attendance_checkin
(id, user_id, nation, province, city, district, street, street_number, status, `data`, create_time)
VALUES(4, 2, '中国', '云南省', '昆明市', '呈贡区', '致远路', NULL, 2, '2024-05-14', '2024-05-14 08:35:59');
INSERT INTO attendance_checkin
(id, user_id, nation, province, city, district, street, street_number, status, `data`, create_time)
VALUES(5, 2, '中国', '云南省', '昆明市', '呈贡区', '致远路', NULL, 1, '2024-05-14', '2024-05-14 18:35:30');
INSERT INTO attendance_checkin
(id, user_id, nation, province, city, district, street, street_number, status, `data`, create_time)
VALUES(6, 2, '中国', '云南省', '昆明市', '呈贡区', '致远路', NULL, 2, '2024-05-15', '2024-05-15 08:35:30');
INSERT INTO attendance_checkin
(id, user_id, nation, province, city, district, street, street_number, status, `data`, create_time)
VALUES(7, 2, '中国', '云南省', '昆明市', '呈贡区', '致远路', '', 3, '2024-05-22', '2024-05-22 15:51:11');
INSERT INTO attendance_checkin
(id, user_id, nation, province, city, district, street, street_number, status, `data`, create_time)
VALUES(8, 4, '中国', '云南省', '昆明市', '呈贡区', '致远路', '', 1, '2024-05-22', '2024-05-22 19:02:19');
INSERT INTO attendance_checkin
(id, user_id, nation, province, city, district, street, street_number, status, `data`, create_time)
VALUES(12, 3, '中国', '云南省', '昆明市', '呈贡区', '致远路', '', 1, '2024-05-22', '2024-05-22 19:07:29');
-- 特殊考勤表
CREATE TABLE `attendance_special` (
                                      `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                      `data` date NOT NULL COMMENT '考勤特殊日名称',
                                      `mark` varchar(255) DEFAULT NULL COMMENT '特殊日说明',
                                      `is_work` tinyint(1) NOT NULL DEFAULT '0' COMMENT '（true为特殊工作日，false为节假日）',
                                      `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
                                      `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
                                      PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='特殊考勤表';
INSERT INTO attendance_special
(id, `data`, mark, is_work, is_enable, create_time)
VALUES(3, '2024-06-10', '端午节放假', 0, 1, '2024-05-19 11:06:53');
INSERT INTO attendance_special
(id, `data`, mark, is_work, is_enable, create_time)
VALUES(4, '2024-06-14', '公司周庆', 0, 1, '2024-05-19 11:07:46');
-- 系统参数表（同样采用分表的形式进行维护，方便后期扩展）
-- 系统参数表
CREATE TABLE `model_sys` (
                             `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                             `param_key` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '参数名',
                             `param_value` varchar(255) DEFAULT NULL COMMENT '参数值',
                             `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
                             `remark` varchar(255) DEFAULT NULL COMMENT '备注',
                             `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `model_sys_unique` (`param_key`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COMMENT='系统参数模型';
INSERT INTO model_sys
(id, param_key, param_value, is_enable, remark, create_time)
VALUES(1, 'attendance_start_time', '07:30', 1, '上班考勤打卡开始时间', '2024-05-18 17:15:41');
INSERT INTO model_sys
(id, param_key, param_value, is_enable, remark, create_time)
VALUES(2, 'attendance_time', '08:30', 1, '上班时间', '2024-05-18 17:15:41');
INSERT INTO model_sys
(id, param_key, param_value, is_enable, remark, create_time)
VALUES(3, 'attendance_end_time', '09:00', 1, '上班打卡截至时间', '2024-05-18 17:16:35');
INSERT INTO model_sys
(id, param_key, param_value, is_enable, remark, create_time)
VALUES(4, 'closing_start_time', '16:30', 1, '下班打卡开始时间', '2024-05-18 17:17:27');
INSERT INTO model_sys
(id, param_key, param_value, is_enable, remark, create_time)
VALUES(5, 'closing_time', '17:30', 1, '下班时间', '2024-05-18 17:17:48');
INSERT INTO model_sys
(id, param_key, param_value, is_enable, remark, create_time)
VALUES(6, 'closing_end_time', '23:30', 1, '下班打卡截至时间', '2024-05-18 17:18:27');
-- 人脸模型表
CREATE TABLE `model_face` (
                              `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                              `user_id` bigint NOT NULL COMMENT '用户id',
                              `model` text NOT NULL COMMENT '人脸模型数据',
                              `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                              PRIMARY KEY (`id`),
                              UNIQUE KEY `model_face_unique` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='用户人脸信息';
INSERT INTO model_face
(id, user_id, model, create_time)
VALUES(1, 2, 'D:/WorkFile/emos/image/tmp_dc0de4a27644f134d11aae57399b8ce97c7bb030e695c60d.jpg', '2024-05-20 18:39:52');
INSERT INTO model_face
(id, user_id, model, create_time)
VALUES(2, 4, 'D:/WorkFile/emos/image/tmp_5bd5ed6d3eda3af32dc137b9d7bbb37a8030455ccf7d0cbb.jpg', '2024-05-22 15:24:05');
INSERT INTO model_face
(id, user_id, model, create_time)
VALUES(3, 3, 'D:/WorkFile/emos/image/tmp_a709a3d114ca3f8a24eddb634d5047811a8ad96d0bf0105f.jpg', '2024-05-22 19:04:03');
INSERT INTO model_face
(id, user_id, model, create_time)
VALUES(4, 12, 'D:/WorkFile/emos/image/tmp_f14e47e630ab6ec71d88f04fcdd844ae56f2cd24add0c729.jpg', '2024-05-22 19:10:04');
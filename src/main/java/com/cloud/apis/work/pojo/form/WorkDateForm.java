package com.cloud.apis.work.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/5
 */
@Data
public class WorkDateForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    @NotNull(message = "用户不存在")
    @Range(min = 1,message = "请提交有效的用户")
    private Long userId;
    private String content;
    @NotNull(message = "请提交创建日期")
    private String createTime;
}

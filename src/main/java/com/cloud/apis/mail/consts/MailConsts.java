package com.cloud.apis.mail.consts;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/5
 */
public class MailConsts {

    public interface MailTitle {
        String INSERT_USER = "入职通知";
        String OUTSET_USER = "离职告知";
        String SALARY_USER = "工资发放通知";
        String EXCEPTION_USER = "考勤异常通知";
    }

    public interface MailContent {
        String ENTRY_USER = "您好！鉴于您出色的能力，您最终进入到%s,所执行岗位为%s" +
                ",很高兴您将成为本公司的一员，我们对您的加入表示热烈欢迎！" +
                "<br/>" +
                "此外，您在本公司系统中账号信息如下：" +
                "<br/>" +
                "<b>账 号：</b>%s\n" +
                "<br/>" +
                "<b>密 码：</b>%s\n"+
                "<br/>"+
                "请记住上述账号和密码，在入职后您将使用它登录公司的相关系统进行相关的日常工作!!!";
        String OUT_USER = "<h1>离职告知</h1>"+
                "尊敬的先生/女士:"+
                "<br/>"+
                "根据公司相关政策,您于%s正式离职,您的离职手续已经全部办理完毕"+
                "<ol>"+
                "<li>1.请将相关离职资料交公司人事部</li>"+
                "<li>2.若您有相关问题,请与原人事联系</li>"+
                "</ol>"+
                "<br/>"+
                "<p>"+
                "很高兴能够与您共同发展公司业务,但鉴于您自己的未来发展和是事业,您打算离开本公司发展"+
                "非常感谢您的参与,截至到今日，您的离职事宜已经全部办理，需要注意的是您将不能够再次登录系统," +
                "若有相关的未尽事宜请与原人事联系,感谢您的参与期待与您的再次见面"+
                "</p>";
        String SALARY_USER = "您好,您%s的工资已经发放完毕,实发%s,请及时查收,如有疑问请联系财务部门";
        String EXCEPTION_USER = "您好,您所管理的用户%s,岗位为%s,电话为%s,%s的考勤迟到或早退,请联系该用户";
    }

    public enum MailType {
        USER("user");

        MailType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        private String type;
    }
}
package com.cloud.apis.user.pojo.form;

import cn.dev33.satoken.annotation.SaCheckLogin;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 自我介绍描述表单
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/22
 */
@Data
public class DescriptionForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "描述语句不能为空")
    @Range(min = 0, max =30, message = "描述语句太长")
    @ApiModelProperty(value = "描述语句", required = true)
    private String description;
}

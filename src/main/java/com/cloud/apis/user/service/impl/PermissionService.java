package com.cloud.apis.user.service.impl;

import com.cloud.apis.core.exception.CustomException;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.model.Permission;
import com.cloud.apis.core.model.Role;
import com.cloud.apis.core.utils.PageInfoToPageDataConverterUtil;
import com.cloud.apis.core.web.ServiceCode;
import com.cloud.apis.user.db.dao.PermissionMapper;
import com.cloud.apis.user.db.dao.RoleMapper;
import com.cloud.apis.user.pojo.form.*;
import com.cloud.apis.user.service.IPermissionService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限相关业务层实现
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Slf4j
@Service
public class PermissionService implements IPermissionService{
    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Value("${custom-config.default-query-page-size}")
    private Integer defaultQueryPageSize;

    public PermissionService(){
        log.info("【PermissionService】");
    }

    @Override
    public PageData<Permission> listPermission(PermissionListForm form) {
        return listPage(form.getPageNum(),defaultQueryPageSize);
    }

    @Override
    public PageData<Permission> listPage(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Permission> list = permissionMapper.getPermissionlist();
        PageInfo<Permission> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }

    @Override
    public Permission addPermission(AddPermissionForm form) {
        if(permissionMapper.selectPermissionByCode(form.getCode())!=null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "权限编码已存在");
        }
        int num = permissionMapper.insertPermission(form);
        if(num==0){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "添加权限失败");
        }
        return permissionMapper.selectPermissionById(form.getId());
    }

    @Override
    public Permission searchPermissionById(Long id) {
        return permissionMapper.selectPermissionById(id);
    }

    @Override
    public void updatePermission(UpdatePermissionForm form) {
        if (permissionMapper.selectPermissionById(form.getId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "权限不存在");
        }
        Permission permission = permissionMapper.selectPermissionByCode(form.getCode());
        if(permission!=null&&!permission.getId().equals(form.getId())){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "权限编码已存在");
        }
        int num = permissionMapper.updatePermission(form);
        if(num==0){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "修改权限失败");
        }
    }

    @Override
    public void setPermissionEnable(EnablePermissionForm form) {
        if(permissionMapper.selectPermissionById(form.getId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "权限不存在");
        }
        int num = permissionMapper.setEnablePermission(form);
        if(num==0){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "修改权限状态失败");
        }
    }

    @Override
    public PageData<Permission> listSearchResult(SearchPermissionForm form) {
        return searchByName(form.getPageNum(),defaultQueryPageSize,form);
    }

    @Override
    public PageData<Permission> searchByName(Integer pageNum, Integer pageSize, SearchPermissionForm form) {
        PageHelper.startPage(pageNum, pageSize);
        List<Permission> list = permissionMapper.searchByName(form);
        PageInfo<Permission> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }

    @Override
    public List<Permission> listAllPermission(RoleIdForm form) {
        if(roleMapper.selectRoleById(form.getId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "角色不存在");
        }
        return permissionMapper.selectPermissionsExceptByRoleId(form);
    }
}

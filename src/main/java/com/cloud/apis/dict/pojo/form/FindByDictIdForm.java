package com.cloud.apis.dict.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 根据字典id查询字典数据
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/27
 */
@Data
public class FindByDictIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @Range(min = 1,message = "id必须大于0")
    @NotNull(message = "id不能为空")
    private Long id;
}

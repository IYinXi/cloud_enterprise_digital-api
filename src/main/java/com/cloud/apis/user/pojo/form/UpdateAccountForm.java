package com.cloud.apis.user.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 修改账户表单
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/23
 */
@Data
public class UpdateAccountForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "账户不能为空")
    @Pattern(regexp = "^1[3-9]\\d{9}$", message = "账户格式不正确")
    @ApiModelProperty(value = "账户", required = true)
    private String account;
}

package com.cloud.apis.organization.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 修改岗位数据表单
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class UpdatePostForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "岗位id不能为空")
    private Long id;
    @NotNull(message = "岗位名称不能为空")
    private String name;
    @NotNull(message = "岗位简称不能为空")
    private String abbreviation;
    @NotNull(message = "岗位类型不能为空")
    private Long organizationId;
    private boolean isEnable;
}

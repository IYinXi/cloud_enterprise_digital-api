package com.cloud.apis.document.db.dao;

import com.cloud.apis.attendance.pojo.vo.SpecialVo;
import com.cloud.apis.document.pojo.form.*;
import com.cloud.apis.document.pojo.vo.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/3
 */
@Mapper
public interface DocumentMapper {
    /**
     * 插入报销记录
     */
    int insertReimbursement(setReimbursementForm form);
    /**
     * 根据id查询报销记录
     */
    ReimbursementVo selectReimbursementById(Long id);
    /**
     * 插入补助记录
     */
    int insertSubsidy(setSubsidyForm form);
    /**
     * 根据id查询补助记录
     */
    SubsidyVo selectSubsidyById(Long id);
    /**
     * 插入印章记录
     */
    int insertSeal(setSealForm form);
    /**
     * 根据id查询印章记录
     */
    SealVo selectSealById(Long id);
    /**
     * 插入工资记录
     */
    int insertSalary(setSalaryForm form);
    /**
     * 根据id查询工资记录
     */
    SalaryVo selectSalaryById(Long id);
    /**
     * 分页查询工资记录
     */
    List<HashMap> searchSalaryByPage(Long userId, long start, int length);
    /**
     * 删除工资记录
     */
    int deleteSalaryById(Long id);
    /**
     * 修改报销记录状态
     */
    int setReimburseMentIsEnable(DocumentIsEnableForm form);
    /**
     * 根据报销id获取报销记录
     */
    ReimbursementVo getReimburseMentByIsEnable(DocumentIsEnableForm form);
    /**
     * 修改补助记录状态
     */
    int setSubsidyIsEnable(DocumentIsEnableForm form);
    /**
     * 根据补助id获取补助记录
     */
    SubsidyVo getSubsidyByIsEnable(DocumentIsEnableForm form);
    /**
     * 修改印章记录状态
     */
    int setSealIsEnable(DocumentIsEnableForm form);
    /**
     * 根据印章id获取印章记录
     */
    SealVo getSealByIsEnable(DocumentIsEnableForm form);
    /**
     * 插入请假记录
     */
    int insertLeave(setLeaveForm form);
    /**
     * 根据id查询请假记录
     */
    LeaveVo selectLeaveById(Long id);
    /**
     * 根据用户id查询当前用户的请假记录
     */
    LeaveVo selectLeaveByUserId(Long userId);
    /**
     * 修改请假记录状态
     */
    int setLeaveIsEnable(DocumentIsEnableForm form);
    /**
     * 根据请假id获取请假记录
     */
    LeaveVo getLeaveByIsEnable(DocumentIsEnableForm form);
    /**
     * 插入请假记录
     */
    int insertTravle(setTravleForm form);
    /**
     * 根据id查询请假记录
     */
    TravleVo selectTravleById(Long id);
    /**
     * 修改请假记录状态
     */
    int setTravleIsEnable(DocumentIsEnableForm form);
    /**
     * 根据出差id获取出差记录
     */
    TravleVo getTravleByIsEnable(DocumentIsEnableForm form);
    /**
     * 根据用户id查询当前用户的出差记录
     */
    TravleVo selectTravleByUserId(Long userId);
    /**
     * 根据用户id查询当前用户的请假记录
     */
    List<LeaveVo> getLeavelist(Long userId);
    /**
     * 根据用户id查询当前用户的出差记录
     */
    List<TravleVo> getTravlelist(Long userId);
    /**
     * 根据请假创建时间搜索请假列表
     */
    List<LeaveVo> searchByLeaveName(LeaveSearchForm form);
    /**
     * 根据出差创建时间搜索出差列表
     */
    List<TravleVo> searchByTransferName(TransferSearchForm form);
    /**
     * 分页查询报销记录
     */
    List<ReimbursementVo> reimbursementListPage(ReimbursementListForm form);
    /**
     * 分页查询补助记录
     */
    List<SubsidyVo> subsidyListPage(subsidyListForm form);
    /**
     * 分页查询报销记录
     */
    List<ReimbursementVo> reimbursementSearchPage(ReimbursementSearchForm form);
    /**
     * 分页查询补助记录
     */
    List<SubsidyVo> subsidySearchPage(SubsidySearchForm form);
}

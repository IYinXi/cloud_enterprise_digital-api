package com.cloud.apis.organization.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/2
 */
@Data
public class OrganizationUserIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "组织id不能为空")
    private Long organizationId;
    private Long userId;
}

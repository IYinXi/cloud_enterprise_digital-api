package com.cloud.apis.document.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.model.Reimbursement;
import com.cloud.apis.core.model.Role;
import com.cloud.apis.core.web.R;
import com.cloud.apis.document.pojo.form.*;
import com.cloud.apis.document.pojo.vo.LeaveVo;
import com.cloud.apis.document.pojo.vo.ReimbursementVo;
import com.cloud.apis.document.pojo.vo.SubsidyVo;
import com.cloud.apis.document.pojo.vo.TravleVo;
import com.cloud.apis.document.service.IDocumentService;
import com.cloud.apis.message.pojo.form.SearchMessageByPageForm;
import com.cloud.apis.message.pojo.form.SearchSalaryByPageForm;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/3
 */
@RestController
@RequestMapping("/document")
@Api(tags = "记录相关API接口")
@Slf4j
public class DocumentController {
    @Autowired
    private IDocumentService documentService;
    public DocumentController() {
        log.info("【DocumentController】");
    }

    @PostMapping("/setReimbursement")
    @ApiOperation("创建一条报销记录")
    @ApiOperationSupport(order = 1)
    @SaCheckLogin
    public R setReimbursement(@Valid @RequestBody setReimbursementForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data", documentService.setReimbursement(form));
    }

    @PostMapping("/getReimburseMent")
    @ApiOperation("根据报销id获取报销记录")
    @ApiOperationSupport(order = 2)
    @SaCheckLogin
    public R getReimburseMent(@Valid @RequestBody DocumentIdForm form) {
        return R.ok().put("data", documentService.getReimburseMent(form.getId()));
    }

    @PostMapping("/setSubsidy")
    @ApiOperation("创建一条补助记录")
    @ApiOperationSupport(order = 3)
    @SaCheckLogin
    public R setReimbursement(@Valid @RequestBody setSubsidyForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data", documentService.setSubsidyForm(form));
    }

    @PostMapping("/getSubsidy")
    @ApiOperation("根据补助id获取补助记录")
    @ApiOperationSupport(order = 4)
    @SaCheckLogin
    public R getSubsidy(@Valid @RequestBody DocumentIdForm form) {
        return R.ok().put("data", documentService.getSubsidy(form.getId()));
    }

    @PostMapping("/setSeal")
    @ApiOperation("创建一条印章记录")
    @ApiOperationSupport(order = 5)
    @SaCheckLogin
    public R setSeal(@Valid @RequestBody setSealForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data", documentService.setSealForm(form));
    }

    @PostMapping("/getSeal")
    @ApiOperation("根据印章id获取印章记录")
    @ApiOperationSupport(order = 6)
    @SaCheckLogin
    public R getSeal(@Valid @RequestBody DocumentIdForm form) {
        return R.ok().put("data", documentService.getSeal(form.getId()));
    }

    @PostMapping("/setSalary")
    @ApiOperation("创建一条工资记录")
    @ApiOperationSupport(order = 7)
    @SaCheckLogin
    public R setSalary(@Valid @RequestBody setSalaryForm form) {
        return R.ok().put("data", documentService.setSalary(form));
    }

    @PostMapping("/searchSalaryByPage")
    @ApiOperation("获取分页工资列表")
    @ApiOperationSupport(order = 8)
    @SaCheckLogin
    public R searchMessageByPage(@Valid @RequestBody SearchSalaryByPageForm form){
        Long userId = Long.parseLong((String) StpUtil.getLoginId());
        int page = form.getPage();
        int length = form.getLength();
        long start = (page-1)*length;
        List<HashMap> list = documentService.searchSalaryByPage(userId, start, length);
        return R.ok().put("result",list);
    }

    @PostMapping("/searchSalaryById")
    @ApiOperation("根据Id查询工资")
    @ApiOperationSupport(order = 9)
    @SaCheckLogin
    public R searchSalaryById(@Valid @RequestBody DocumentIdForm form){
        return R.ok().put("result",documentService.searchSalaryById(form.getId()));
    }

    @DeleteMapping("/deleteSalaryById")
    @ApiOperation("根据Id删除工资")
    @ApiOperationSupport(order = 10)
    @SaCheckLogin
    public R deleteSalaryById(@Valid @RequestBody DocumentIdForm form){
        documentService.deleteSalaryById(form.getId());
        return R.ok("删除工资信息成功");
    }

    @PutMapping("/setReimburseMentIsEnable")
    @ApiOperation("修改报销记录状态")
    @ApiOperationSupport(order = 11)
    @SaCheckLogin
    public R setReimburseMentIsEnable(@Valid @RequestBody DocumentIsEnableForm form) {
        documentService.setReimburseMentIsEnable(form);
        return R.ok("修改成功");
    }

    @PostMapping("/getReimburseMentByIsEnable")
    @ApiOperation("根据报销状态获取报销记录")
    @ApiOperationSupport(order = 12)
    @SaCheckLogin
    public R getReimburseMentByIsEnable(@Valid @RequestBody DocumentIsEnableForm form) {
        form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        return R.ok().put("data", documentService.getReimburseMentByIsEnable(form));
    }

    @PutMapping("/setSubsidyIsEnable")
    @ApiOperation("修改补助记录状态")
    @ApiOperationSupport(order = 13)
    @SaCheckLogin
    public R setSubsidyIsEnable(@Valid @RequestBody DocumentIsEnableForm form) {
        documentService.setSubsidyIsEnable(form);
        return R.ok("修改成功");
    }

    @PostMapping("/getSubsidyByIsEnable")
    @ApiOperation("根据补助状态获取补助记录")
    @ApiOperationSupport(order = 14)
    @SaCheckLogin
    public R getSubsidyByIsEnable(@Valid @RequestBody DocumentIsEnableForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data", documentService.getSubsidyByIsEnable(form));
    }

    @PutMapping("/setSealIsEnable")
    @ApiOperation("修改印章记录状态")
    @ApiOperationSupport(order = 15)
    @SaCheckLogin
    public R setSealIsEnable(@Valid @RequestBody DocumentIsEnableForm form) {
        documentService.setSealIsEnable(form);
        return R.ok("修改成功");
    }

    @PostMapping("/getSealByIsEnable")
    @ApiOperation("根据印章状态获取印章记录")
    @ApiOperationSupport(order = 16)
    @SaCheckLogin
    public R getSealByIsEnable(@Valid @RequestBody DocumentIsEnableForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data", documentService.getSealByIsEnable(form));
    }

    @PostMapping("/setTravle")
    @ApiOperation("创建一条出差记录")
    @ApiOperationSupport(order = 17)
    @SaCheckLogin
    public R setTravle(@Valid @RequestBody setTravleForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data", documentService.setTravleForm(form));
    }

    @PostMapping("/getTravle")
    @ApiOperation("根据出差id获取出差记录")
    @ApiOperationSupport(order = 6)
    @SaCheckLogin
    public R getTravle(@Valid @RequestBody DocumentIdForm form) {
        return R.ok().put("data", documentService.getTravle(form.getId()));
    }

    @PutMapping("/setTravleIsEnable")
    @ApiOperation("修改出差记录状态")
    @ApiOperationSupport(order = 15)
    @SaCheckLogin
    public R setTravleIsEnable(@Valid @RequestBody DocumentIsEnableForm form) {
        documentService.setTravleIsEnable(form);
        return R.ok("修改成功");
    }

    @PostMapping("/getTravleByIsEnable")
    @ApiOperation("根据出差状态获取出差记录")
    @ApiOperationSupport(order = 16)
    @SaCheckLogin
    public R getTravleByIsEnable(@Valid @RequestBody DocumentIsEnableForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data", documentService.getTravleByIsEnable(form));
    }

    @PostMapping("/setLeave")
    @ApiOperation("创建一条请假记录")
    @ApiOperationSupport(order = 17)
    @SaCheckLogin
    public R setLeave(@Valid @RequestBody setLeaveForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data", documentService.setLeaveForm(form));
    }

    @PostMapping("/getLeave")
    @ApiOperation("根据请假id获取请假记录")
    @ApiOperationSupport(order = 6)
    @SaCheckLogin
    public R getLeave(@Valid @RequestBody DocumentIdForm form) {
        return R.ok().put("data", documentService.getLeave(form.getId()));
    }

    @PutMapping("/setLeaveIsEnable")
    @ApiOperation("修改请假记录状态")
    @ApiOperationSupport(order = 15)
    @SaCheckLogin
    public R setLeaveIsEnable(@Valid @RequestBody DocumentIsEnableForm form) {
        documentService.setLeaveIsEnable(form);
        return R.ok("修改成功");
    }

    @PostMapping("/getLeaveByIsEnable")
    @ApiOperation("根据请假状态获取请假记录")
    @ApiOperationSupport(order = 16)
    @SaCheckLogin
    public R getLeaveByIsEnable(@Valid @RequestBody DocumentIsEnableForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data", documentService.getLeaveByIsEnable(form));
    }

    @PostMapping("/leaveList")
    @ApiOperation("请假列表")
    @ApiOperationSupport(order = 18)
    @SaCheckLogin
    public R leaveList(@Valid @RequestBody LeaveListForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<LeaveVo> pageData = documentService.leaveList(form);
        return R.ok().put("data", pageData);
    }
    @PostMapping("/travleList")
    @ApiOperation("出差列表")
    @ApiOperationSupport(order = 19)
    @SaCheckLogin
    public R travleList(@Valid @RequestBody TravleListForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<TravleVo> pageData = documentService.travleList(form);
        return R.ok().put("data", pageData);
    }

    @PostMapping("/leaveSearch")
    @ApiOperation("请假分页搜索")
    @ApiOperationSupport(order = 20)
    @SaCheckLogin
    public R leaveSearch(@Valid @RequestBody LeaveSearchForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<LeaveVo> pageData = documentService.leaveSearch(form);
        return R.ok().put("data", pageData);
    }

    @PostMapping("/travleSearch")
    @ApiOperation("出差分页搜索")
    @ApiOperationSupport(order = 21)
    @SaCheckLogin
    public R transferSearch(@Valid @RequestBody TransferSearchForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<TravleVo> pageData = documentService.transferSearch(form);
        return R.ok().put("data", pageData);
    }

    @PostMapping("/reimbursementList")
    @ApiOperation("报销分页列表")
    @ApiOperationSupport(order = 22)
    @SaCheckLogin
    public R reimbursementList(@Valid @RequestBody ReimbursementListForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<ReimbursementVo> pageData = documentService.reimbursementList(form);
        return R.ok().put("data", pageData);
    }

    @PostMapping("/subsidyList")
    @ApiOperation("补助分页列表")
    @ApiOperationSupport(order = 22)
    @SaCheckLogin
    public R subsidyList(@Valid @RequestBody subsidyListForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<SubsidyVo> pageData = documentService.subsidyList(form);
        return R.ok().put("data", pageData);
    }

    @PostMapping("/reimbursementSearch")
    @ApiOperation("报销分页搜索")
    @ApiOperationSupport(order = 23)
    @SaCheckLogin
    public R reimbursementSearch(@Valid @RequestBody ReimbursementSearchForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<ReimbursementVo> pageData = documentService.reimbursementSearch(form);
        return R.ok().put("data", pageData);
    }
    @PostMapping("/subsidySearch")
    @ApiOperation("补助分页搜索")
    @ApiOperationSupport(order = 24)
    @SaCheckLogin
    public R subsidySearch(@Valid @RequestBody SubsidySearchForm form) {
        if(form.getUserId()==null){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<SubsidyVo> pageData = documentService.subsidySearch(form);
        return R.ok().put("data", pageData);
    }
}

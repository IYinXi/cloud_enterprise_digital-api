package com.cloud.apis.attendance.pojo.form;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/13
 */
@Data
public class ResetPositionForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private String nation;
    private String province;
    private String city;
    private String district;
    private String street;
    private String streetNumber;
}

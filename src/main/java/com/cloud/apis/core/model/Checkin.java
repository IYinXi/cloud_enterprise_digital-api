package com.cloud.apis.core.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 考勤签到表
 * attendance_checkin
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("attendance_checkin")
public class Checkin implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 所在国家
     */
    private String nation;

    /**
     * 所在省份
     */
    private String province;

    /**
     * 所在城市
     */
    private String city;

    /**
     * 所在区，县
     */
    private String district;

    /**
     * 所在街道
     */
    private String street;

    /**
     * 街道牌号
     */
    @TableField("street_number")
    private String streetNumber;

    /**
     * 考勤结果
     */
    private Byte status;

    /**
     * 签到日期
     */
    private Date data;

    /**
     * 签到时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    private static final long serialVersionUID = 1L;
}
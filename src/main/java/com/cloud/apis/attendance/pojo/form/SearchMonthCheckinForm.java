package com.cloud.apis.attendance.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/13
 */
@Data
public class SearchMonthCheckinForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long userId;
    @NotNull
    @Range(min=2000,max = 3000)
    private Integer year;
    @NotNull
    @Range(min=1,max = 12)
    private Integer month;
}

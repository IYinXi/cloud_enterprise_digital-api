package com.cloud.apis.organization.pojo.form;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/2
 */
@Data
public class UpdateTransferForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long transferId;
    private Long oldOrganizationId;
    private Long oldPostId;
    private Long newOrganizationId;
    private Long newPostId;
    private String reason;
    private String resources;
    private Boolean isEnable;
}

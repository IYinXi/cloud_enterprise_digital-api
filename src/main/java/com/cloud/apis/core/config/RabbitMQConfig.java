package com.cloud.apis.core.config;

import com.rabbitmq.client.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * RabbitMQ配置
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/5
 */
@Configuration
@Slf4j
public class RabbitMQConfig {
    @Value("${custom-config.rabbit-url}")
    private String rabbitUrl;
    public RabbitMQConfig(){
        log.info("【RabbitMQConfig】");
    }
    /**
     * 配置RabbitMQ
     */
    @Bean
    public ConnectionFactory getFactory(){
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rabbitUrl);//主机的IP地址
        factory.setPort(5672);//RabbitMQ的使用端口号
        return factory;
    }
}

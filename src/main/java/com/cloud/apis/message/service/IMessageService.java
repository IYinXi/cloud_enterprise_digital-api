package com.cloud.apis.message.service;

import com.cloud.apis.message.model.MessageEntity;
import com.cloud.apis.message.model.MessageRefEntity;

import java.util.HashMap;
import java.util.List;

/**
 * 消息模块业务接口
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/5
 */
public interface IMessageService {
    /**
     * 向message集合中插入消息
     */
    String insert(MessageEntity entity);
    /**
     * 分页查询message中的消息
     */
    List<HashMap> searchMessageByPage(Long userId, long start, int length);
    /**
     * 根据id查找message中的数据
     */
    HashMap searchMessageById(String id);
    /**
     * 向message_ref中插入一条数据
     */
    String insert(MessageRefEntity entity);
    /**
     * 从message_ref中查询某个用户的未读消息
     */
    long searchUnreadCount(Long userId);
    /**
     * 从message_ref中查询某个用户新接收到的消息数量
     */
    long searchLastCount(Long userId);
    /**
     * 将message_ref中对应id的未读消息改为已读消息
     */
    long updateUnreadMessage(String id);
    /**
     * 根据id值从message_ref中删除消息记录
     */
    long deleteMessageRefById(String id);
    /**
     * 根据userId从message_ref中删除用户的所有消息
     */
    long deleteUserMessageRef(int userId);
}

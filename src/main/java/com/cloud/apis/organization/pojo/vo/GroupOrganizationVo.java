package com.cloud.apis.organization.pojo.vo;

import com.cloud.apis.core.model.Organization;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 分组所有组织
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/27
 */
@Data
public class GroupOrganizationVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String label;
    private ArrayList<Organization> options;
}

package com.cloud.apis.approval.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/22
 */
@Data
public class ApprovalResultForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @Range(min = 1, message = "id不能为空")
    private Long id;
    @Range(min = 0,max = 4, message = "审批状态不能为空")
    private Byte approvalStatus;
    @NotNull(message = "审批类型不能为空")
    private String approvalType;
    private String approvalReason;
}

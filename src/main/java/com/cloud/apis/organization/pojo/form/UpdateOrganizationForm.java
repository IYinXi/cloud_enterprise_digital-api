package com.cloud.apis.organization.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/26
 */
@Data
public class UpdateOrganizationForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "组织id不能为空")
    @Range(min = 1, message = "组织id不能小于1")
    @ApiModelProperty(value = "组织id", required = true)
    private Long id;
    @ApiModelProperty(value = "组织名称", required = true)
    private String name;
    @ApiModelProperty(value = "组织简称", required = true)
    private String abbreviation;
    @ApiModelProperty(value = "组织类型", required = true)
    private Long typeId;
    @ApiModelProperty(value = "组织父级id", required = true)
    private Long parentId;
    @ApiModelProperty(value = "组织是否启用", required = true)
    private Boolean isEnable;
}

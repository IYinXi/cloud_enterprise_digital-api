package com.cloud.apis.attendance.pojo.form;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/13
 */
@Data
public class SysForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long[] ids;
    private String attendanceStartTime;
    private String attendanceTime;
    private String attendanceEndTime;
    private String closingStartTime;
    private String closingTime;
    private String closingEndTime;
}

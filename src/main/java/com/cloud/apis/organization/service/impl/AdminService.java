package com.cloud.apis.organization.service.impl;

import com.cloud.apis.core.exception.CustomException;
import com.cloud.apis.core.model.Dict;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.utils.PageInfoToPageDataConverterUtil;
import com.cloud.apis.core.web.ServiceCode;
import com.cloud.apis.dict.db.dao.DictMapper;
import com.cloud.apis.mail.service.MailService;
import com.cloud.apis.message.task.MessageTask;
import com.cloud.apis.message.utils.MessageSendUtil;
import com.cloud.apis.organization.db.dao.AdminMapper;
import com.cloud.apis.organization.db.dao.OrganizationMapper;
import com.cloud.apis.organization.pojo.form.AdminIdForm;
import com.cloud.apis.organization.pojo.form.AssignForm;
import com.cloud.apis.organization.pojo.form.CancleAssignForm;
import com.cloud.apis.organization.pojo.form.OrganizationIdPageForm;
import com.cloud.apis.organization.pojo.vo.AdminInfoVo;
import com.cloud.apis.organization.pojo.vo.GroupOrganizationVo;
import com.cloud.apis.organization.service.IAdminService;
import com.cloud.apis.user.db.dao.UserMapper;
import com.cloud.apis.user.pojo.vo.UserInfoVo;
import com.cloud.apis.websocket.WebSocketServe;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/29
 */
@Service
@Slf4j
public class AdminService implements IAdminService {
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DictMapper dictMapper;
    @Autowired
    private OrganizationMapper organizationMapper;
    @Value("${custom-config.default-query-page-size}")
    private Integer defaultQueryPageSize;
    @Autowired
    private MessageTask messageTask;
    @Autowired
    private WebSocketServe webSocketServe;

    public AdminService(){
        log.info("【AdminService】");
    }

    @Override
    public ArrayList<AdminInfoVo> getAdminOrganizations(Long id) {
        if(userMapper.selectUserById(id)==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "用户不存在");
        }
        return adminMapper.getAdminOrganizations(id);
    }

    @Override
    public ArrayList<GroupOrganizationVo> listGroupOrganization(AdminIdForm form) {
        ArrayList<GroupOrganizationVo> groupOrganizationVos = new ArrayList<>();
        ArrayList<Dict> dicts = dictMapper.selectDictByType("organization");
        for (int i = 0; i < dicts.size(); i++) {
            GroupOrganizationVo groupOrganizationVo = new GroupOrganizationVo();
            groupOrganizationVo.setLabel(dicts.get(i).getName());
            groupOrganizationVo.setOptions(adminMapper.selectOrganizationByTypeId(dicts.get(i).getId(),form));
            groupOrganizationVos.add(groupOrganizationVo);
        }
        return groupOrganizationVos;
    }

    @Override
    public AdminInfoVo assign(AssignForm form) {
        if(userMapper.selectUserById(form.getId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "用户不存在");
        }
        if(organizationMapper.selectOrganizationById(form.getOrganizationId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "组织不存在");
        }
        int num = adminMapper.insertAssign(form);
        if(num==0){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "添加失败");
        }
        String message = "您已经被指派管理"+organizationMapper.selectOrganizationById(form.getOrganizationId()).getName()+"请您登录服务平台注意流程审批事宜！！！";
        MessageSendUtil.sendSimpleMessage(messageTask,form.getId(),"系统消息",message);
        webSocketServe.sendMessage(form.getId());
        return adminMapper.selectAdminById(form.getAssignId());
    }

    @Override
    public void cancleAssign(CancleAssignForm form) {
        if(userMapper.selectUserById(form.getId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "用户不存在");
        }
        if(organizationMapper.selectOrganizationById(form.getOrganizationId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "组织不存在");
        }
        int num = adminMapper.deleteAssign(form);
        if(num==0){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "取消失败");
        }
        String message = "您已经被取消指派管理"+organizationMapper.selectOrganizationById(form.getOrganizationId()).getName()+"请您登录服务平台注意查收！！！";
        MessageSendUtil.sendSimpleMessage(messageTask,form.getId(),"系统消息",message);
        webSocketServe.sendMessage(form.getId());
    }

    @Override
    public ArrayList<UserInfoVo> getAdmins(Long id) {
        if(organizationMapper.selectOrganizationById(id)==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "组织不存在");
        }
        ArrayList<UserInfoVo> userInfoVos = adminMapper.selectadminByOrganizationId(id);
        return userInfoVos;
    }

    @Override
    public PageData<UserInfoVo> organizations(OrganizationIdPageForm form) {
        return organizationsPage(form.getPageNum(),defaultQueryPageSize, form);
    }

    @Override
    public PageData<UserInfoVo> organizationsPage(Integer pageNum, Integer pageSize, OrganizationIdPageForm form) {
        PageHelper.startPage(pageNum, pageSize);
        List<UserInfoVo> list = adminMapper.organizationsPage(form);
        PageInfo<UserInfoVo> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }
}

package com.cloud.apis.organization.pojo.vo;

import com.cloud.apis.user.pojo.vo.UserInfoVo;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/29
 */
@Data
public class AdminInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long adminId;
    private Long organizationId;
    private Boolean isEnable;
    private LocalDateTime createTime;
    private LocalDateTime modifiedTime;
    private UserInfoVo admin;
    private OrganizationInfoVo organization;
}

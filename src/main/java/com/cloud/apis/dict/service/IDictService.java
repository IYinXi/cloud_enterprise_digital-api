package com.cloud.apis.dict.service;

import com.cloud.apis.approval.pojo.form.ApprovalListForm;
import com.cloud.apis.approval.pojo.vo.ApprovalVo;
import com.cloud.apis.core.model.Dict;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.dict.pojo.form.*;

import java.util.ArrayList;

/**
 * 字典相关API接口业务层接口
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/25
 */
public interface IDictService {
    /**
     * 根据类型获取字典数据
     */
    ArrayList<Dict> getDictByType(String type);
    /**
     * 根据简称获取字典数据
     */
    Dict getDictByAbbreviation(String abbreviation);

    /**
     * 获取顶层字典数据
     */
    ArrayList<Dict> getDictRootList();
    /**
     * 根据顶层字典数据id分页查询其子数据
     */
    PageData<Dict> listDict(DictListForm form);
    /**
     * 分页查询字典列表
     */
    PageData<Dict> listPage(Integer pageNum, Integer pageSize, DictListForm form);
    /**
     * 新增字典数据
     */
    Dict addDict(AddDictDataForm form);
    /**
     * 根据id获取字典数据
     */
    Dict getDictById(Long id);
    /**
     * 更新字典数据
     */
    void updateDict(UpdateDictForm form);
    /**
     * 启用或禁用字典数据
     */
    void setDictEnable(EnableDictForm form);
    /**
     * 根据名称查询字典数据
     */
    PageData<Dict> searchByName(Integer pageNum, Integer pageSize,SearchDictForm form);
    /**
     * 查询结果列表
     */
    PageData<Dict> listSearchResult(SearchDictForm form);
}

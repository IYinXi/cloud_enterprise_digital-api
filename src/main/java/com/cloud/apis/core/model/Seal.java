package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用印记录
 * document_seal
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("document_seal")
public class Seal implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 用印类型
     */
    @TableField("type_id")
    private Long typeId;

    /**
     * 用印说明
     */
    private String reason;

    /**
     * 相关材料路径
     */
    private String resources;

    /**
     * 登记日期
     */
    @TableField("log_time")
    private Date logTime;
    /**
     * 是否启用
     */
    @TableField("is_enable")
    private Boolean isEnable;

    private static final long serialVersionUID = 1L;
}
package com.cloud.apis.attendance.db.dao;

import com.cloud.apis.attendance.pojo.form.*;
import com.cloud.apis.attendance.pojo.vo.*;
import com.cloud.apis.core.model.Special;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/7
 */
@Mapper
public interface AttendanceMapper {
    Special selectSpecialByDate(String date);

    String selectByParamKey(String attendanceStartTime);

    FaceVo selectFaceByUserId(Long userId);

    int insertCheckin(CheckinForm checkinForm);

    CheckinVo selectCheckinById(Long id);

    int insertFace(FaceForm form);

    ArrayList<CheckinVo> selectCheckinByData(String cur,Long userId);

    AddressVo selectAddress();

    List<SpecialVo> specialListPage();

    int insertSpecial(SpecialAddForm form);

    SpecialVo selectSpecialById(Long id);

    int deleteSpecialAttendance(Long id);

    ArrayList<SysVo> sys(Long[] ids);

    int reset(Long[] ids);

    int setSys(Long id, String s);

    AddressVo getPosition();

    int resetPosition(ResetPositionForm form);

    ArrayList<HashMap> searchWeekCheckin(HashMap param);

    ArrayList<String> searchHolidaysInRange(HashMap param);

    ArrayList<String> searchWorkdayInRange(HashMap param);

    HashMap searchTodayCheckin(Long userId);

    long searchCheckinDays(Long userId);
}

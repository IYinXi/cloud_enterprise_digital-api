package com.cloud.apis.user.pojo.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 更新用户信息表单
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/20
 */
@Data
public class UpdateUserInfoForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "姓名不能为空")
    @Pattern(regexp = "^[\\u4e00-\\u9fa5]{2,10}$", message = "姓名格式错误")
    private String name;
    @NotNull(message = "出生日期不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime birthday;
    @NotNull(message = "性别不能为空")
    private Long sexId;
    @NotNull(message = "身份证不能为空")
    @Pattern(regexp = "^([1-9]\\d{5})(18|19|20)\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}([0-9]|X)$", message = "身份证格式错误")
    private String idCard;
    @NotNull(message = "民族不能为空")
    private Long ethnicId;
    @NotNull(message = "学历不能为空")
    private Long degreeId;
    @NotNull(message = "毕业院校不能为空")
    private String graduate;
    @NotNull(message = "身份背景不能为空")
    private Long identityId;
    @NotNull(message = "户籍地不能为空")
    private Long domicileId;
    @NotNull(message = "家庭住址不能为空")
    private String address;
    @NotNull(message = "微信不能为空")
    private String vx;
    @NotNull(message = "其他联系方式不能为空")
    private String contact;
    private String description;
    private String motto;
}

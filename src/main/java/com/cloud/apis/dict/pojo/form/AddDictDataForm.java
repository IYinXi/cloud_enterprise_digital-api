package com.cloud.apis.dict.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 新增字典数据表单
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/27
 */
@Data
public class AddDictDataForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    @NotNull(message = "字典名称不能为空")
    @NotBlank(message = "字典名称不能为空")
    private String name;
    @NotNull(message = "字典简称不能为空")
    @NotBlank(message = "字典简称不能为空")
    private String abbreviation;
    @NotNull(message = "字典类型不能为空")
    @NotBlank(message = "字典类型不能为空")
    private String type;
    private boolean isEnable;
    private String remarks;
    @NotNull(message = "字典父节点id不能为空")
    private Long parentId;

}

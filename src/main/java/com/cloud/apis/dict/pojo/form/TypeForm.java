package com.cloud.apis.dict.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 类型数据
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/25
 */
@Data
public class TypeForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "类型不能为空")
    @ApiModelProperty(value = "类型", required = true)
    private String type;
}

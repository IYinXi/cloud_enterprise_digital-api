package com.cloud.apis.organization.service;

import com.cloud.apis.core.model.Dict;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.model.Post;
import com.cloud.apis.dict.pojo.form.DictListForm;
import com.cloud.apis.dict.pojo.form.SearchDictForm;
import com.cloud.apis.organization.pojo.form.*;
import com.cloud.apis.organization.pojo.vo.PostInfoVo;
import com.cloud.apis.organization.pojo.vo.TransferInfoVo;

import java.util.ArrayList;

/**
 岗位API相关接口业务层接口

 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/1
 */
public interface IPostService {
    /**
     * 根据组织id获取岗位信息
     */
    ArrayList<Post> getPostByOrganizationId(Long id);
    /**
     * 根据组织id分页获取岗位列表
     */
    PageData<PostInfoVo> listPost(PostListForm form);
    /**
     * 分页查询字典列表
     */
    PageData<PostInfoVo> listPage(Integer pageNum, Integer pageSize, PostListForm form);
    /**
     * 添加岗位
     */
    PostInfoVo addPost(AddPostForm form);
    /**
     * 更新岗位
     */
    void updatePost(UpdatePostForm form);
    /**
     * 根据id获取岗位信息
     */
    PostInfoVo getPostInfoById(Long id);
    /**
     * 设置岗位状态
     */
    void setPostEnable(EnablePostForm form);
    /**
     * 根据名称查询字典数据
     */
    PageData<PostInfoVo> searchByName(Integer pageNum, Integer pageSize, SearchPostForm form);
    /**
     * 根据名称查询岗位列表
     */
    PageData<PostInfoVo> listSearchResult(SearchPostForm form);

    /**
     *查找在指定组织id下的所有岗位
     */
    ArrayList<Post> listPostByOrganizationId(OrganizationIdForm form);
    /**
     *查找在指定组织id下的所有岗位
     */
    ArrayList<Post> listPostByOrganizationExpectUserId(OrganizationUserIdForm form);

    /**
     *创建新的转岗记录
     */
    TransferInfoVo setTransfer(TransferForm form);
    /**
     *修改转岗信息
     */
    void updateTransferInfo(UpdateTransferForm form);
}

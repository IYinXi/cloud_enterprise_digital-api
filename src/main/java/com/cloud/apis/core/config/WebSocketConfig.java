package com.cloud.apis.core.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import javax.websocket.server.ServerEndpoint;

/**
 * webSocket配置
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/25
 */
@Component
@Slf4j
public class WebSocketConfig {
    public WebSocketConfig(){
      log.info("【WebSocketConfig】");
    }
    /**
     * 配置WebSocket服务监听
     */
    @Bean
    ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}

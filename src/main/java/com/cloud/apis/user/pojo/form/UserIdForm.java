package com.cloud.apis.user.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/29
 */
@Data
public class UserIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "用户id不能为空")
    @Range(min = 1, message = "请提交正确的用户id")
    Long id;
}

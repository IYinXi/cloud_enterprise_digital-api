package com.cloud.apis.document.service;

import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.model.Reimbursement;
import com.cloud.apis.core.model.Role;
import com.cloud.apis.core.web.R;
import com.cloud.apis.document.pojo.form.*;
import com.cloud.apis.document.pojo.vo.*;

import java.util.HashMap;
import java.util.List;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/3
 */
public interface IDocumentService {
    ReimbursementVo setReimbursement(setReimbursementForm form);
    ReimbursementVo getReimburseMent(Long id);
    SubsidyVo setSubsidyForm(setSubsidyForm form);
    SubsidyVo getSubsidy(Long id);
    SealVo setSealForm(setSealForm form);
    SealVo getSeal(Long id);
    SalaryVo setSalary(setSalaryForm form);
    List<HashMap> searchSalaryByPage(Long userId, long start, int length);
    SalaryVo searchSalaryById(Long id);
    void deleteSalaryById(Long id);
    void setReimburseMentIsEnable(DocumentIsEnableForm form);
    ReimbursementVo getReimburseMentByIsEnable(DocumentIsEnableForm form);
    void setSubsidyIsEnable(DocumentIsEnableForm form);
    SubsidyVo getSubsidyByIsEnable(DocumentIsEnableForm form);
    void setSealIsEnable(DocumentIsEnableForm form);
    SealVo getSealByIsEnable(DocumentIsEnableForm form);
    LeaveVo setLeaveForm(setLeaveForm form);
    LeaveVo getLeave(Long id);
    void setLeaveIsEnable(DocumentIsEnableForm form);
    LeaveVo getLeaveByIsEnable(DocumentIsEnableForm form);
    TravleVo setTravleForm(setTravleForm form);
    TravleVo getTravle(Long id);
    void setTravleIsEnable(DocumentIsEnableForm form);
    TravleVo getTravleByIsEnable(DocumentIsEnableForm form);
    PageData<LeaveVo> leaveList(LeaveListForm form);
    PageData<LeaveVo> listPage(Integer pageNum, Integer pageSize, LeaveListForm form);
    PageData<TravleVo> travleList(TravleListForm form);
    PageData<TravleVo> listTravlePage(Integer pageNum, Integer pageSize, TravleListForm form);
    PageData<LeaveVo> leaveSearch(LeaveSearchForm form);
    PageData<LeaveVo> searchByLeaveName(Integer pageNum, Integer pageSize,LeaveSearchForm form);
    PageData<TravleVo> transferSearch(TransferSearchForm form);
    PageData<TravleVo> searchByTransferName(Integer pageNum, Integer pageSize,TransferSearchForm form);
    PageData<ReimbursementVo> reimbursementList(ReimbursementListForm form);
    PageData<ReimbursementVo> reimbursementListPage(Integer pageNum, Integer pageSize,ReimbursementListForm form);
    PageData<SubsidyVo> subsidyList(subsidyListForm form);
    PageData<SubsidyVo> subsidyListPage(Integer pageNum, Integer pageSize,subsidyListForm form);
    PageData<ReimbursementVo> reimbursementSearch(ReimbursementSearchForm form);
    PageData<ReimbursementVo> reimbursementSearchPage(Integer pageNum, Integer pageSize,ReimbursementSearchForm form);
    PageData<SubsidyVo> subsidySearch(SubsidySearchForm form);
    PageData<SubsidyVo> subsidySearchPage(Integer pageNum, Integer pageSize,SubsidySearchForm form);
}

package com.cloud.apis.core.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户人脸信息
 * model_face
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("model_face")
public class Face implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 人脸模型数据
     */
    private String model;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    private static final long serialVersionUID = 1L;
}
package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 考勤地点表
 * attendance_address
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("attendance_address")
public class Address implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 所在国家
     */
    private String nation;

    /**
     * 所在省分
     */
    private String province;

    /**
     * 所在城市
     */
    private String city;

    /**
     * 所在区县
     */
    private String district;

    /**
     * 所在街道
     */
    private String street;

    /**
     * 所在门牌号
     */
    @TableField("street_number")
    private String streetNumber;

    /**
     * 是否启用
     */
    @TableField("is_enable")
    private Boolean isEnable;

    /**
     * 创建日期
     */
    @TableField("create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}
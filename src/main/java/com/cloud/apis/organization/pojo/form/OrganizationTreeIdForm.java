package com.cloud.apis.organization.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 根据传递的组织架构Id获取当前组织架构树
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/19
 */
@Data
public class OrganizationTreeIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "组织id不能为空")
    @Range(min = 1, message = "组织id不能小于1")
    @ApiModelProperty(value = "组织id", required = true)
    private Long id;
}

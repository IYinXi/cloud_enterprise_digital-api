package com.cloud.apis.data;

import cn.hutool.core.date.DateUtil;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/4
 */
public class DataTests {
    @Test
    public void test(){
        System.out.println(DateUtil.format(new Date(),"yyyy-MM-dd"));
        System.out.println(DateUtil.date().isWeekend());
        System.out.println(DateUtil.now());
        System.out.println(DateUtil.date());
        System.out.println(DateUtil.now().compareTo("2024-05-08 17:52:09") >= 0);
        System.out.println(DateUtil.date().isPM());
        System.out.println(LocalDateTime.now().getYear());
        System.out.println(LocalDateTime.now().getMonthValue());
        System.out.println(LocalDateTime.now().getDayOfMonth());
        int hireYear = LocalDateTime.now().getYear();
        int hireMonth = LocalDateTime.now().getMonthValue();
        int hireDay = LocalDateTime.now().getDayOfMonth();
        System.out.println(hireYear + "-" + (hireMonth<10?"0"+hireMonth:hireMonth) + "-" + hireDay);
    }
}

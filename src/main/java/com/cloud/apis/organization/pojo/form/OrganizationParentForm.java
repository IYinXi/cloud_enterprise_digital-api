package com.cloud.apis.organization.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 父级组织表单
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/25
 */
@Data
public class OrganizationParentForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "父级组织id不能为空")
    @Range(min = 1, message = "父级组织id不能小于1")
    @ApiModelProperty(value = "父级组织id", required = true)
    Long parentId;
}

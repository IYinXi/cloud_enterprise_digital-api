package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 报销记录表
 * document_reimbursement
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("document_subsidy")
public class Subsidy implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 报销类型id
     */
    @TableId("type_id")
    private Long typeId;

    /**
     * 报销理由
     */
    private String reason;

    /**
     * 材料路径
     */
    private String resources;

    /**
     * 报销时间
     */
    @TableField("log_time")
    private Date logTime;
    /**
     * 是否启用
     */
    @TableField("is_enable")
    private Boolean isEnable;

    private static final long serialVersionUID = 1L;
}
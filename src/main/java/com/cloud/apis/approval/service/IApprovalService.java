package com.cloud.apis.approval.service;

import com.cloud.apis.approval.pojo.form.*;
import com.cloud.apis.approval.pojo.vo.ApprovalInfoVo;
import com.cloud.apis.approval.pojo.vo.ApprovalVo;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.user.pojo.vo.UserInfoVo;

import java.util.ArrayList;

/**
 * 审批模块业务接口
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/6
 */
public interface IApprovalService {
    /**
     * 根据申请者id和审批类型查询审批记录
     */
    ArrayList<ApprovalInfoVo> searchApprovalByApplyIdAndApprovalType(Long applyId, String approvalType);

    /**
     *申请审批
     */
    ApprovalInfoVo applyApprovalService(ApplyApprovalForm form);

    /**
     * 分页查询审批列表
     */
    PageData<ApprovalVo> listApproval(ApprovalListForm form);

    /**
     * 分页查询审批列表
     */
    PageData<ApprovalVo> listPage(Integer pageNum, Integer pageSize,ApprovalListForm form);

    /**
     * 更新审批结果
     */
    void updateResult(ApprovalResultForm form) throws IllegalAccessException;

    /**
     * 设置撤销状态
     */
    void updateCut(SetCutForm form);
    /**
     * 根据审批者id分页查询审核列表
     */
    PageData<ApprovalVo> getListByApprovalId(ApprovalIdForm form);
    /**
     * 分页查询审批列表
     */
    PageData<ApprovalVo> listPageByApprovalId(Integer pageNum, Integer pageSize,ApprovalIdForm form);
}

package com.cloud.apis.attendance.pojo.form;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/7
 */
@Data
public class CheckinForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String nation;
    private String province;
    private String city;
    private String district;
    private String street;
    private String streetNumber;
    private Integer status;
    private Long userId;
    private String data;
    private String createTime;
}

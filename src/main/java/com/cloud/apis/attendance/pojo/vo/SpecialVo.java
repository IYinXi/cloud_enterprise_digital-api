package com.cloud.apis.attendance.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/7
 */
@Data
public class SpecialVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Date data;
    private String mark;
    private Boolean isWork;
    private Boolean isEnable;
    private LocalDateTime createTime;
}

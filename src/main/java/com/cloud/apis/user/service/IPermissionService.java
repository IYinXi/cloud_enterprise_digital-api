package com.cloud.apis.user.service;

import com.cloud.apis.core.model.PageData;

import com.cloud.apis.core.model.Permission;
import com.cloud.apis.core.model.Role;
import com.cloud.apis.user.pojo.form.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限相关API业务层接口
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
public interface IPermissionService {
    /**
     * 分页查询权限列表
     */
    PageData<Permission> listPermission(PermissionListForm form);
    /**
     * 权限列表分页
     */
    PageData<Permission> listPage(Integer pageNum, Integer pageSize);
    /**
     * 添加权限
     */
    Permission addPermission(AddPermissionForm form);
    /**
     * 根据id查询权限
     */
    Permission searchPermissionById(Long id);
    /**
     * 修改权限
     */
    void updatePermission(UpdatePermissionForm form);
    /**
     * 启用或禁用权限
     */
    void setPermissionEnable(EnablePermissionForm form);
    /**
     * 分页获取搜索列表
     */
    PageData<Permission> listSearchResult(SearchPermissionForm form);
    /**
     * 根据权限名称查询角色信息列表
     */
    PageData<Permission> searchByName(Integer pageNum, Integer pageSize, SearchPermissionForm form);
    /**
     * 查询所有权限
     */
    List<Permission> listAllPermission(RoleIdForm form);
}

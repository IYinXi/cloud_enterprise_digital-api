package com.cloud.apis.work.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.cloud.apis.core.web.R;
import com.cloud.apis.work.pojo.form.*;
import com.cloud.apis.work.service.IWorkService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/4
 */
@RestController
@RequestMapping("/work")
@Api(tags = "工作相关API接口")
@Slf4j
public class WorkController {
    @Autowired
    private IWorkService workService;
    public WorkController() {
        log.info("【WorkController】");
    }
    @PostMapping("/searchWorkDailyByPage")
    @ApiOperation("获取分页日报列表")
    @ApiOperationSupport(order = 1)
    @SaCheckLogin
    public R searchWorkDailyByPage(@Valid @RequestBody SearchWorkDailyByPageForm form){
        Long userId = Long.parseLong((String) StpUtil.getLoginId());
        int page = form.getPage();
        int length = form.getLength();
        long start = (page-1)*length;
        List<HashMap> list = workService.searchWorkByPage(userId, start, length);
        return R.ok().put("result",list);
    }
    @PostMapping("/setWorkDaily")
    @ApiOperation("提交日报记录")
    @ApiOperationSupport(order = 2)
    @SaCheckLogin
    public R setWorkDaily(@Valid @RequestBody WorkDailyForm form){
        Long userId = Long.parseLong((String) StpUtil.getLoginId());
        if(form.getUserId() == null || form.getUserId() == 0){
            form.setUserId(userId);
        }
        return R.ok("提交成功").put("data",workService.setWorkDaily(form));
    }

    @PostMapping("/searchWorkDailyDetialById")
    @ApiOperation("获取日报详情")
    @ApiOperationSupport(order = 3)
    @SaCheckLogin
    public R searchWorkDailyDetialById(@Valid @RequestBody WorkIdForm form){
        Long userId = Long.parseLong((String) StpUtil.getLoginId());
        if(form.getId()==null || form.getId()==0){
            form.setId(userId);
        }
        return R.ok().put("data",workService.searchWorkDailyDetialById(form.getId()));
    }

    @PostMapping("/addArrange")
    @ApiOperation("新增工作日程安排")
    @ApiOperationSupport(order = 4)
    @SaCheckLogin
    public R addArrange(@Valid @RequestBody WorkDateForm form){
        System.out.println(form);
        return R.ok("提交成功").put("data", workService.setWorkDate(form));
    }
    @PostMapping("/getArrange")
    @ApiOperation("获取工作日程安排")
    @ApiOperationSupport(order = 5)
    @SaCheckLogin
    public R getArrange(@Valid @RequestBody WorkDateIdForm form){
        if(form.getUserId() == null || form.getUserId() == 0){
            form.setUserId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data",workService.selectWorkDateByIdAndDate(form));
    }

    @PutMapping("/updateArrange")
    @ApiOperation("更新工作日程安排")
    @ApiOperationSupport(order = 6)
    @SaCheckLogin
    public R updateArrange(@Valid @RequestBody UpdateWorkForm form){
        return R.ok("更新成功").put("data", workService.updateWorkDate(form));
    }
}

package com.cloud.apis.organization.pojo.vo;

import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/27
 */
@Data
public class PostInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 岗位id
     */
    private Long id;

    /**
     * 组织id
     */
    private Long organizationId;

    /**
     * 岗位名称
     */
    private String name;

    /**
     * 岗位简称
     */
    private String abbreviation;

    /**
     * 是否启用(0,否;1,是)
     */
    private Boolean isEnable;

    /**
     * 创建日期
     */
    private LocalDateTime createTime;

    /**
     * 修改日期
     */
    private LocalDateTime modifiedTime;

    /**
     * 直属组织
     */
    private OrganizationInfoVo organization;

}

package com.cloud.apis.oss.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
public class DeleteCosFileForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotEmpty(message = "pathes不能为空")
    private String[] pathes;
}
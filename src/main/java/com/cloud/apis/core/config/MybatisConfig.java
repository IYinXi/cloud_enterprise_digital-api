package com.cloud.apis.core.config;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * MyBatis配置
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/20
 */
@Slf4j
@Configuration
@MapperScan("com.cloud.apis.**.dao")
public class MybatisConfig {
    public MybatisConfig() {
        log.info("【MybatisConfig】");
    }
}
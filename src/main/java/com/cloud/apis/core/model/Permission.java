package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 权限表
 * tb_permission
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_permission")
public class Permission implements Serializable {
    /**
     * 权限id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 权限代码
     */
    private String code;

    /**
     * 是否启用(0,否;1,是）
     */
    @TableField("is_enable")
    private Boolean isEnable;

    /**
     * 创建日期
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 修改日期
     */
    @TableField("modified_time")
    private Date modifiedTime;

    private static final long serialVersionUID = 1L;
}
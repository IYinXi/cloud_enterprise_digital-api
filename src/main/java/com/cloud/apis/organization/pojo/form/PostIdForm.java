package com.cloud.apis.organization.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 岗位id表单
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class PostIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "岗位id不能为空")
    @Range(min = 1, message = "请提交正确的岗位id")
    private Long id;
}

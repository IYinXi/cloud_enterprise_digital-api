package com.cloud.apis.document.service.impl;

import cn.hutool.core.date.DateUtil;
import com.cloud.apis.core.exception.CustomException;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.utils.PageInfoToPageDataConverterUtil;
import com.cloud.apis.core.web.ServiceCode;
import com.cloud.apis.dict.db.dao.DictMapper;
import com.cloud.apis.document.db.dao.DocumentMapper;
import com.cloud.apis.document.pojo.form.*;
import com.cloud.apis.document.pojo.vo.*;
import com.cloud.apis.document.service.IDocumentService;
import com.cloud.apis.mail.service.MailService;
import com.cloud.apis.message.task.MessageTask;
import com.cloud.apis.message.utils.MessageSendUtil;
import com.cloud.apis.user.db.dao.UserMapper;
import com.cloud.apis.websocket.WebSocketServe;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/3
 */
@Service
@Slf4j
public class DocumentService implements IDocumentService {
    @Autowired
    private DocumentMapper documentMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DictMapper dictMapper;
    @Autowired
    private MessageTask messageTask;
    @Autowired
    private WebSocketServe webSocketServe;
    @Autowired
    private MailService mailService;
    @Value("${custom-config.default-query-page-size}")
    private Integer defaultQueryPageSize;

    public DocumentService(){
        log.info("【DocumentService】");
    }

    @Override
    public ReimbursementVo setReimbursement(setReimbursementForm form) {
        if(userMapper.selectUserById(form.getUserId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前用户不存在");
        }
        if(dictMapper.selectDictById(form.getTypeId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前报销类型不存在");
        }
        int num = documentMapper.insertReimbursement(form);
        if(num<=0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "插入数据失败");
        }
        return documentMapper.selectReimbursementById(form.getId());
    }

    @Override
    public ReimbursementVo getReimburseMent(Long id) {
        return documentMapper.selectReimbursementById(id);
    }

    @Override
    public SubsidyVo setSubsidyForm(setSubsidyForm form) {
        if(userMapper.selectUserById(form.getUserId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前用户不存在");
        }
        if(dictMapper.selectDictById(form.getTypeId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前报销类型不存在");
        }
        int num = documentMapper.insertSubsidy(form);
        if(num<=0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "插入数据失败");
        }
        return documentMapper.selectSubsidyById(form.getId());
    }

    @Override
    public SubsidyVo getSubsidy(Long id) {
        return documentMapper.selectSubsidyById(id);
    }

    @Override
    public SealVo setSealForm(setSealForm form) {
        if(userMapper.selectUserById(form.getUserId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前用户不存在");
        }
        if(dictMapper.selectDictById(form.getTypeId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前报销类型不存在");
        }
        int num = documentMapper.insertSeal(form);
        if(num<=0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "插入数据失败");
        }
        return documentMapper.selectSealById(form.getId());
    }

    @Override
    public SealVo getSeal(Long id) {
        return documentMapper.selectSealById(id);
    }

    @Override
    public SalaryVo setSalary(setSalaryForm form) {
        if(userMapper.selectUserById(form.getUserId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前用户不存在");
        }
        form.setCreateTime(new Date());
        int num = documentMapper.insertSalary(form);
        if(num<=0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "插入数据失败");
        }
        String message = "您"+DateUtil.format(form.getCreateTime(),"yyyy-MM")+"工资已发放，请及时查收领取，若有问题，请您及时联系主管的财务！！";
        MessageSendUtil.sendSimpleMessage(messageTask, form.getUserId(),"工资发放通知",message);
        webSocketServe.sendMessage(form.getUserId());
        SalaryVo salaryVo = documentMapper.selectSalaryById(form.getId());
        mailService.sendSalaryMail(form.getId(),new String[]{salaryVo.getUser().getEMail()});
        return salaryVo;
    }

    @Override
    public List<HashMap> searchSalaryByPage(Long userId, long start, int length) {
        if(userMapper.selectUserById(userId)==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前用户不存在");
        }
        return documentMapper.searchSalaryByPage(userId,start,length);
    }

    @Override
    public SalaryVo searchSalaryById(Long id) {
        return documentMapper.selectSalaryById(id);
    }

    @Override
    public void deleteSalaryById(Long id) {
        if(documentMapper.selectSalaryById(id)==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前工资不存在");
        }
        int num = documentMapper.deleteSalaryById(id);
        if(num<=0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "删除数据失败");
        }
    }

    @Override
    public void setReimburseMentIsEnable(DocumentIsEnableForm form) {
        int num = documentMapper.setReimburseMentIsEnable(form);
        if(num<=0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "修改数据失败");
        }
    }

    @Override
    public ReimbursementVo getReimburseMentByIsEnable(DocumentIsEnableForm form) {
        return documentMapper.getReimburseMentByIsEnable(form);
    }

    @Override
    public void setSubsidyIsEnable(DocumentIsEnableForm form) {
        int num = documentMapper.setSubsidyIsEnable(form);
        if(num<=0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "修改数据失败");
        }
    }

    @Override
    public SubsidyVo getSubsidyByIsEnable(DocumentIsEnableForm form) {
        return documentMapper.getSubsidyByIsEnable(form);
    }

    @Override
    public void setSealIsEnable(DocumentIsEnableForm form) {
        int num = documentMapper.setSealIsEnable(form);
        if(num<=0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "修改数据失败");
        }
    }

    @Override
    public SealVo getSealByIsEnable(DocumentIsEnableForm form) {
        return documentMapper.getSealByIsEnable(form);
    }

    @Override
    public LeaveVo setLeaveForm(setLeaveForm form) {
        int num = documentMapper.insertLeave(form);
        if(num<=0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "插入数据失败");
        }
        return documentMapper.selectLeaveById(form.getId());
    }

    @Override
    public LeaveVo getLeave(Long id) {
        return documentMapper.selectLeaveById(id);
    }

    @Override
    public void setLeaveIsEnable(DocumentIsEnableForm form) {
        int num = documentMapper.setLeaveIsEnable(form);
        if(num<=0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "修改数据失败");
        }
    }

    @Override
    public LeaveVo getLeaveByIsEnable(DocumentIsEnableForm form) {
        return documentMapper.getLeaveByIsEnable(form);
    }

    @Override
    public TravleVo setTravleForm(setTravleForm form) {
        int num = documentMapper.insertTravle(form);
        if(num<=0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "插入数据失败");
        }
        return documentMapper.selectTravleById(form.getId());
    }

    @Override
    public TravleVo getTravle(Long id) {
        return documentMapper.selectTravleById(id);
    }

    @Override
    public void setTravleIsEnable(DocumentIsEnableForm form) {
        int num = documentMapper.setTravleIsEnable(form);
        if(num<=0) {
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(), "修改数据失败");
        }
    }

    @Override
    public TravleVo getTravleByIsEnable(DocumentIsEnableForm form) {
        return documentMapper.getTravleByIsEnable(form);
    }

    @Override
    public PageData<LeaveVo> leaveList(LeaveListForm form) {
        return listPage(form.getPageNum(),defaultQueryPageSize,form);
    }

    @Override
    public PageData<LeaveVo> listPage(Integer pageNum, Integer pageSize, LeaveListForm form) {
        PageHelper.startPage(pageNum, pageSize);
        List<LeaveVo> list = documentMapper.getLeavelist(form.getUserId());
        PageInfo<LeaveVo> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }

    @Override
    public PageData<TravleVo> travleList(TravleListForm form) {
        return listTravlePage(form.getPageNum(),defaultQueryPageSize,form);
    }

    @Override
    public PageData<TravleVo> listTravlePage(Integer pageNum, Integer pageSize, TravleListForm form) {
        PageHelper.startPage(pageNum, pageSize);
        List<TravleVo> list = documentMapper.getTravlelist(form.getUserId());
        PageInfo<TravleVo> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }

    @Override
    public PageData<LeaveVo> leaveSearch(LeaveSearchForm form) {
        return searchByLeaveName(form.getPageNum(),defaultQueryPageSize,form);
    }

    @Override
    public PageData<LeaveVo> searchByLeaveName(Integer pageNum, Integer pageSize,LeaveSearchForm form) {
        PageHelper.startPage(pageNum, pageSize);
        List<LeaveVo> list = documentMapper.searchByLeaveName(form);
        PageInfo<LeaveVo> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }

    @Override
    public PageData<TravleVo> transferSearch(TransferSearchForm form) {
        return searchByTransferName(form.getPageNum(),defaultQueryPageSize,form);
    }

    @Override
    public PageData<TravleVo> searchByTransferName(Integer pageNum, Integer pageSize,TransferSearchForm form) {
        PageHelper.startPage(pageNum, pageSize);
        List<TravleVo> list = documentMapper.searchByTransferName(form);
        PageInfo<TravleVo> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }

    @Override
    public PageData<ReimbursementVo> reimbursementList(ReimbursementListForm form) {
        return reimbursementListPage(form.getPageNum(),defaultQueryPageSize,form);
    }

    @Override
    public PageData<ReimbursementVo> reimbursementListPage(Integer pageNum, Integer pageSize, ReimbursementListForm form) {
        PageHelper.startPage(pageNum, pageSize);
        List<ReimbursementVo> list = documentMapper.reimbursementListPage(form);
        PageInfo<ReimbursementVo> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }

    @Override
    public PageData<SubsidyVo> subsidyList(subsidyListForm form) {
        return subsidyListPage(form.getPageNum(),defaultQueryPageSize,form);
    }

    @Override
    public PageData<SubsidyVo> subsidyListPage(Integer pageNum, Integer pageSize, subsidyListForm form) {
        PageHelper.startPage(pageNum, pageSize);
        List<SubsidyVo> list = documentMapper.subsidyListPage(form);
        PageInfo<SubsidyVo> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }

    @Override
    public PageData<ReimbursementVo> reimbursementSearch(ReimbursementSearchForm form) {
        return reimbursementSearchPage(form.getPageNum(),defaultQueryPageSize,form);
    }

    @Override
    public PageData<ReimbursementVo> reimbursementSearchPage(Integer pageNum, Integer pageSize, ReimbursementSearchForm form) {
        PageHelper.startPage(pageNum, pageSize);
        List<ReimbursementVo> list = documentMapper.reimbursementSearchPage(form);
        PageInfo<ReimbursementVo> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }

    @Override
    public PageData<SubsidyVo> subsidySearch(SubsidySearchForm form) {
        return subsidySearchPage(form.getPageNum(),defaultQueryPageSize,form);
    }

    @Override
    public PageData<SubsidyVo> subsidySearchPage(Integer pageNum, Integer pageSize, SubsidySearchForm form) {
        PageHelper.startPage(pageNum, pageSize);
        List<SubsidyVo> list = documentMapper.subsidySearchPage(form);
        PageInfo<SubsidyVo> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverterUtil.convert(pageInfo);
    }
}

package com.cloud.apis.user.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 新增权限表单
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class UpdatePermissionForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "权限id不能为空")
    private Long id;
    @NotNull(message = "权限名称不能为空")
    private String name;
    @NotNull(message = "权限代码不能为空")
    private String code;
    private boolean isEnable;
}

package com.cloud.apis.approval.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/30
 */
@Data
public class ApprovalIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "审批者id不能为空")
    private Long approvalId;
    private Integer pageNum;
    private Integer approvalStatus;
}

package com.cloud.apis.user.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 用户头像路径表单
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/4
 */
@Data
public class AvaterUrlForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "url不能为空")
    private String url;
}

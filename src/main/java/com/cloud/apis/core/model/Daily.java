package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 工作日报表
 * work_daily
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("work_daily")
public class Daily implements Serializable {
    /**
     * 日报主键id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 评价
     */
    private Byte score;

    /**
     * 日报内容
     */
    private String content;

    /**
     * 创建日期
     */
    @TableField("create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}
package com.cloud.apis.core.exception;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 自定义异常
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/20
 */
@Slf4j
public class CustomException extends RuntimeException{
    /**
     * 状态码
     */
    @Getter
    private int code;
    public CustomException(int code , String msg){
        super(msg);
        this.code = code;
        log.info("【CustomException】");
    }
}
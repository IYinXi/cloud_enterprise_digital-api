package com.cloud.apis.user.db.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.apis.core.model.Dict;
import com.cloud.apis.core.model.Permission;
import com.cloud.apis.core.model.Role;
import com.cloud.apis.user.pojo.form.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色相关DAO层
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/21
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {
    /**
     * 根据角色id查询角色
     */
    Role selectRoleById(Long roleId);
    /**
     * 根据多个角色id查询一组角色信息
     */
    ArrayList<Role> selectRoleInfosByRoleIds(ArrayList<Long> roleIds);
    /**
     * 查询所有角色
     */
    ArrayList<Role> getRolelist();
    /**
     * 根据角色名称查询角色
     */
    Role selectRoleByAbbreviation(String abbreviation);
    /**
     * 根据角色id查询权限id
     */
    ArrayList<Long> searchRolePermission(ArrayList<Long> roleList);

    /**
     *插入角色
     */
    int insertRole(AddRoleForm form);
    /**
     * 修改角色
     */
    int updateRole(UpdateRoleForm form);
    /**
     * 修改角色是否启用
     */
    int updateRoleEnable(EnableRoleForm form);
    /**
     * 根据角色名称查询角色
     */
    List<Role> searchByName(SearchRoleForm form);

    /**
     * 根据角色id分页查询权限
     */
    List<Permission> selectPermissionsByRoleId(RolePermissionPageForm form);

    /**
     *根据角色id批量插入权限
     */
    int insertRolePermission(ArrayList<RolePermission> list);
    /**
     * 根据角色-权限id查询是否存在
     */
    boolean selectRolePermissionById(RolePermissionIdForm form);
    /**
     * 根据角色-权限id删除
     */
    int deleteRolePermission(RolePermissionIdForm form);
    /**
     * 根据权限名称查询权限
     */
    List<Permission> searchByRolePermissionName(RolePermissionSearchForm form);
    /**
     * 查询除了当前角色外的其他角色
     */
    ArrayList<Role> selectOtherRoles(Long id);

    /**
     *根据用户id查询角色
     */
    ArrayList<Role> selectRoleByUserId(Long id);
    /**
     *根据用户id删除用户角色
     */
    int deleteUserRole(UserRoleForm form);
}

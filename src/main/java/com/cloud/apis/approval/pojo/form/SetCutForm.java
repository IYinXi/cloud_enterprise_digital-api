package com.cloud.apis.approval.pojo.form;

import lombok.Data;

import java.io.Serializable;

/**
 * 设置撤销状态表单
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/24
 */
@Data
public class SetCutForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Byte cutStatus;
}

package com.cloud.apis.user.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/2
 */
@Data
public class DimissiomInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long userId;
    private UserInfoVo user;
    private String reason;
    private String resources;
    private LocalDateTime outTime;
}

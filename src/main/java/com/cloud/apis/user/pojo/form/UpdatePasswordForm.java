package com.cloud.apis.user.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 更新密码表单
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/22
 */
@Data
public class UpdatePasswordForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "密码不能为空")
    @Pattern(regexp = "^[a-zA-Z0-9]{4,8}$", message = "密码格式不正确,位数包含4-8位，只能是数字和字母组成")
    private String password;
}

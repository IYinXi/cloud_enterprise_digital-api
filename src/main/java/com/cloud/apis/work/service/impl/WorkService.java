package com.cloud.apis.work.service.impl;

import com.cloud.apis.core.exception.CustomException;
import com.cloud.apis.core.web.ServiceCode;
import com.cloud.apis.mail.service.MailService;
import com.cloud.apis.message.task.MessageTask;
import com.cloud.apis.message.utils.MessageSendUtil;
import com.cloud.apis.user.db.dao.UserMapper;
import com.cloud.apis.websocket.WebSocketServe;
import com.cloud.apis.work.db.dao.WorkMapper;
import com.cloud.apis.work.pojo.form.UpdateWorkForm;
import com.cloud.apis.work.pojo.form.WorkDailyForm;
import com.cloud.apis.work.pojo.form.WorkDateForm;
import com.cloud.apis.work.pojo.form.WorkDateIdForm;
import com.cloud.apis.work.pojo.vo.WorkDailyVo;
import com.cloud.apis.work.pojo.vo.WorkDateVo;
import com.cloud.apis.work.service.IWorkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/4
 */
@Service
@Slf4j
public class WorkService implements IWorkService {
    @Autowired
    private WorkMapper workMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private WebSocketServe webSocketServe;
    @Autowired
    private MessageTask messageTask;

    public WorkService(){
        log.info("【WorkService】");
    }
    @Override
    public List<HashMap> searchWorkByPage(Long userId, long start, int length) {
        if(userMapper.selectUserById(userId)==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前用户不存在");
        }
        return workMapper.searchWorkByPage(userId,start,length);
    }

    @Override
    public WorkDailyVo setWorkDaily(WorkDailyForm form) {
        if(userMapper.selectUserById(form.getUserId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前用户不存在");
        }
        int num = workMapper.setWorkDaily(form);
        if(num==0){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"添加日报失败");
        }
        return workMapper.selectWorkDailyById(form.getId());
    }

    @Override
    public WorkDailyVo searchWorkDailyDetialById(Long id) {
        return workMapper.searchWorkDailyDetialById(id);
    }

    @Override
    public WorkDateVo setWorkDate(WorkDateForm form) {
        if(userMapper.selectUserById(form.getUserId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前用户不存在");
        }
        int num = workMapper.setWorkDate(form);
        if(num==0){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"添加日程安排失败");
        }
        String message = "您有新的工作安排，请您及时查看相关内容安排!!!";
        MessageSendUtil.sendSimpleMessage(messageTask,form.getUserId(),"工作安排",message);
        webSocketServe.sendApprovalMessage(form.getUserId());
        return workMapper.selectWorkDateById(form.getId());
    }

    @Override
    public WorkDateVo selectWorkDateByIdAndDate(WorkDateIdForm form) {
        return workMapper.selectWorkDateByIdAndDate(form);
    }

    @Override
    public WorkDateVo updateWorkDate(UpdateWorkForm form) {
        if(workMapper.selectWorkDateById(form.getId())==null){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"当前安排不存在");
        }
        int num = workMapper.updateWorkDate(form);
        if(num==0){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"更新安排失败");
        }
        WorkDateVo workDateVo = workMapper.selectWorkDateById(form.getId());
        String message = "您的"+workDateVo.getCreateTime()+"的工作内容有所变东请请及时查看";
        MessageSendUtil.sendSimpleMessage(messageTask,workDateVo.getUserId(),"工作安排",message);
        webSocketServe.sendApprovalMessage(workDateVo.getUserId());
        return workDateVo;
    }

}

package com.cloud.apis.message.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * 消息实体
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/5
 */
@Data
@Document(collection = "message")
public class MessageEntity implements Serializable {
    @Id
    private String _id;

    @Indexed(unique = true)
    private String uuid;

    private Long senderId;

    private String senderPhoto = "../../static/system.png";

    private String senderName;

    private String msg;

    @Indexed
    private Date sendTime;
}

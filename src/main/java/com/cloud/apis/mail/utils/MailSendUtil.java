package com.cloud.apis.mail.utils;

import cn.hutool.core.date.DateUtil;
import com.cloud.apis.attendance.db.dao.AttendanceMapper;
import com.cloud.apis.attendance.pojo.vo.CheckinVo;
import com.cloud.apis.core.exception.CustomException;
import com.cloud.apis.core.web.ServiceCode;
import com.cloud.apis.document.db.dao.DocumentMapper;
import com.cloud.apis.document.pojo.vo.SalaryVo;
import com.cloud.apis.mail.consts.MailConsts;
import com.cloud.apis.mail.model.MailInfo;
import com.cloud.apis.user.db.dao.UserMapper;
import com.cloud.apis.user.pojo.vo.UserInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;

/**
 * 邮件发送工具类
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/5
 */
@Slf4j
@Component
public class MailSendUtil {
    @Value("${spring.mail.username}")
    private String systemMail;
    @Value("${custom-config.password}")
    private String defaultPassword;
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DocumentMapper documentMapper;
    @Autowired
    private AttendanceMapper attendanceMapper;
    public MailSendUtil(){
        log.info("【MailSendUtil】");
    }
    public void sendEntryMail(Long id,String[] receivers){
        MailInfo mailInfo = new MailInfo();
        mailInfo.setMailTitle("用户入职书");
        mailInfo.setMailTitle(MailConsts.MailTitle.INSERT_USER);
        UserInfoVo userInfoVo = userMapper.selectUserById(id);
        mailInfo.setMailContent(String.format(MailConsts.MailContent.ENTRY_USER
                ,userInfoVo.getOrganization().getName()
                ,userInfoVo.getJob().getName(),
                userInfoVo.getAccount()
                ,defaultPassword));
        mailInfo.setReceiveMailAccount(receivers);
        try{
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,true);
            //设置发送人
            helper.setFrom(systemMail);
            //设置收件人
            helper.setTo(mailInfo.getReceiveMailAccount());
            //设置邮件主题
            helper.setSubject(mailInfo.getMailTitle());
            //设置邮件内容
            helper.setText(mailInfo.getMailContent(),true);
            mailSender.send(message);
        }catch (MessagingException ignored){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"邮件发送失败");
        }

    }

    public void sendOutMail(Long id, String[] receivers) {
        MailInfo mailInfo = new MailInfo();
        mailInfo.setMailTitle("用户离职书");
        mailInfo.setMailTitle(MailConsts.MailTitle.OUTSET_USER);
        UserInfoVo userInfoVo = userMapper.selectUserById(id);
        mailInfo.setMailContent(String.format(MailConsts.MailContent.OUT_USER, userInfoVo.getOutTime()));
        mailInfo.setReceiveMailAccount(receivers);
        try{
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,true);
            //设置发送人
            helper.setFrom(systemMail);
            //设置收件人
            helper.setTo(mailInfo.getReceiveMailAccount());
            //设置邮件主题
            helper.setSubject(mailInfo.getMailTitle());
            //设置邮件内容
            helper.setText(mailInfo.getMailContent(),true);
            mailSender.send(message);
        }catch (MessagingException ignored){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"邮件发送失败");
        }
    }

    public void sendSalaryMail(Long id, String[] receivers) {
        MailInfo mailInfo = new MailInfo();
        mailInfo.setMailTitle("发放工资通知");
        mailInfo.setMailTitle(MailConsts.MailTitle.SALARY_USER);
        SalaryVo salaryVo = documentMapper.selectSalaryById(id);
        mailInfo.setMailContent(String.format(MailConsts.MailContent.SALARY_USER, DateUtil.format(salaryVo.getCreateTime(),"yyyy-MM"),salaryVo.getActual()));
        mailInfo.setReceiveMailAccount(receivers);
        try{
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,true);
            //设置发送人
            helper.setFrom(systemMail);
            //设置收件人
            helper.setTo(mailInfo.getReceiveMailAccount());
            //设置邮件主题
            helper.setSubject(mailInfo.getMailTitle());
            //设置邮件内容
            helper.setText(mailInfo.getMailContent(),true);
            mailSender.send(message);
        }catch (MessagingException ignored){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"邮件发送失败");
        }
    }

    public void sendException(Long id, String[] receivers) {
        MailInfo mailInfo = new MailInfo();
        mailInfo.setMailTitle("员工异常通知");
        mailInfo.setMailTitle(MailConsts.MailTitle.EXCEPTION_USER);
        CheckinVo checkinVo = attendanceMapper.selectCheckinById(id);
        mailInfo.setMailContent(
                String.format(MailConsts.MailContent.EXCEPTION_USER,
                checkinVo.getUser().getName(),
                checkinVo.getUser().getJob().getName(),
                checkinVo.getUser().getAccount(),
                checkinVo.getCreateTime()));
        mailInfo.setReceiveMailAccount(receivers);
        try{
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,true);
            //设置发送人
            helper.setFrom(systemMail);
            //设置收件人
            helper.setTo(mailInfo.getReceiveMailAccount());
            //设置邮件主题
            helper.setSubject(mailInfo.getMailTitle());
            //设置邮件内容
            helper.setText(mailInfo.getMailContent(),true);
            mailSender.send(message);
        }catch (MessagingException ignored){
            throw new CustomException(ServiceCode.ERROR_BAD_REQUEST.getValue(),"邮件发送失败");
        }
    }
}

package com.cloud.apis.mail.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/5
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MailInfo implements Serializable {
    // 序列化版本号
    private static final long serialVersionUID = 1L;

    // 收件人账户
    private String[] receiveMailAccount;

    // 邮件标题
    private String mailTitle;

    // 邮件正文
    private String mailContent;
}
package com.cloud.apis.work.pojo.form;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/18
 */
@Data
public class UpdateWorkForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String content;
}

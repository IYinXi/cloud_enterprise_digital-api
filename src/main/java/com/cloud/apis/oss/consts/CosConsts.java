package com.cloud.apis.oss.consts;

/**
 * COS类型
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/20
 */
public enum CosConsts {

    AVATER("avater"),
    TRANSFER("transfer"),
    OUT("out"),
    REIMURSEMENT("reimbursement"),
    SUBSIDY("subsidy"),
    LEAVE("leave"),
    TRAVLE("travle"),
    ;

    /**
     * 存储类型
     */
    private String key;

    CosConsts(String key) {
        this.key = key;
    }

    /**
     * 获取key
     * @return String
     */
    private String getKey() {
        return key;
    }

    /**
     * 根据key获取枚举
     * @param key
     * @return CosConsts
     */
    public static CosConsts findByKey(String key) {
        if (key != null) {
            for (CosConsts type : CosConsts.values()) {
                if (key.equals(type.getKey())) {
                    return type;
                }
            }
        }
        return null;
    }
}
package com.cloud.apis.work.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/4
 */
@Data
public class WorkIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "请提交有效id")
    @Range(min =1,message = "请提交有效id")
    private Long id;
}

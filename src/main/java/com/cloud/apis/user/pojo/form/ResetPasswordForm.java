package com.cloud.apis.user.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 重置密码表单
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/29
 */
@Data
public class ResetPasswordForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "提交的id不能为空")
    @Range(min = 1, message = "提交的id不能小于1")
    private Long id;
}

package com.cloud.apis.work.pojo.vo;

import com.cloud.apis.user.pojo.vo.UserInfoVo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/5
 */
@Data
public class WorkDateVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long userId;
    private UserInfoVo user;
    private String content;
    private Date createTime;
}

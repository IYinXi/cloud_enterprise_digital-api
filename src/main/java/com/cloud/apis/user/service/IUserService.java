package com.cloud.apis.user.service;

import cn.hutool.json.JSONObject;
import com.cloud.apis.core.model.Dict;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.model.User;
import com.cloud.apis.dict.pojo.form.SearchDictForm;
import com.cloud.apis.user.pojo.form.*;
import com.cloud.apis.user.pojo.vo.DimissiomInfoVo;
import com.cloud.apis.user.pojo.vo.UserInfoVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 用户相关API业务层接口
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/20
 */
public interface IUserService {
    /**
     * 登录
     */
    HashMap login(HashMap param);
    /**
     * 根据账户名称查询账户信息
     */
    User selectUserByAccount(String account);
    /**
     * 注册用户
     */
    void register(User user,Long[] roleIds);

    /**
     *根据用户id查询用户对应权限
     */
    ArrayList<String> searchUserPermission(Long userId);

    /**
     * 获取用户信息
     */
    HashMap getUserInfo(Long id);

    /**
     * 校验用户密码
     */
    Boolean validPassword(Long id, String password);

    /**
     *修改密码
     */
    Boolean updatePassword(Long id, String password);

    /**
     * 修改自我介绍
     */
    String updateDescription(Long id, String description);

    /**
     * 修改座右铭
     */
    String updateMotto(Long id, String motto);

    /**
     * 根据用户id查询当前用户账号
     */
    String getAccount(Long id);
    /**
     * 修改当前用户账号
     */
    String updateAccount(Long id, String account);

    /**
     * 修改用户证件照
     */
    Integer updateAvater(Long id, String url);

    /**
     * 根据用户id查询当前用户邮箱
     */
    String getEmail(Long id);
    /**
     * 分页获取用户列表
     */
    PageData<UserInfoVo> listUser(Integer pageNum,Long userId);
    /**
     * 分页查询用户列表
     */
    PageData<UserInfoVo> listPage(Integer pageNum, Integer pageSize, Long userId);

    /**
     *根据用户id更新用户信息
     */
     void updateUserInfo(Long id, UpdateUserInfoForm form);
     /**
     * 重置密码
     */
    Boolean resetPassword(Long id);
    /**
     * 根据用户姓名搜索用户数据
     */
    PageData<UserInfoVo> listSearchResult(SearchUserForm form);
    /**
     * 根据名称查询用户数据
     */
    PageData<UserInfoVo> searchByName(Integer pageNum, Integer pageSize, SearchUserForm form);
    /**
     * 修改用户注册信息
     */
    void updateRegistration(UpdateRegistrationForm form);
    /**
     * 根据组织ids获取组织下的用户列表
     */
    PageData<UserInfoVo> listgroupOrgUserList(GroupOrgUserListForm form);
    /**
     * 根据组织ids获取组织下的用户列表
     */
    PageData<UserInfoVo> listgroupOrgUserListPage(Integer pageNum, Integer pageSize, GroupOrgUserListForm form);
    /**
     * 在组织id的情况下根据姓名搜索用户
     */
    PageData<UserInfoVo> searchInOrganizationids(GroupOrgUserSearchForm form);
    /**
     * 在组织id的情况下根据姓名搜索用户
     */
    PageData<UserInfoVo> searchInOrganizationidsPage(Integer pageNum, Integer pageSize, GroupOrgUserSearchForm form);
    /**
     * 管理员获取所有用户列表
     */
    PageData<UserInfoVo> getAllUserList(GetAllUserListForm form);
    /**
     * 管理员获取所有用户列表
     */
    PageData<UserInfoVo> getAllUserListPage(Integer pageNum, Integer pageSize, GetAllUserListForm form);
    /**
     * 修改用户组织岗位
     */
    void updateUserOrgAndPost(UpdateUserOrgAndPostForm form);
    /**
     * 创建一条离职记录
     */
    DimissiomInfoVo setDimission(DimissiomForm form);
    /**
     * 用户id更新为离职
     */
    void updateUserDimission(Long id);
    /**
     * 获取当前用户的通讯录列表
     */
    JSONObject searchUserContactList();
    /**
     * 管理员获取所有离职用户列表
     */
    PageData<UserInfoVo> getAllOutUserList(GetAllUserListForm form);
    /**
     * 管理员获取所有离职用户列表
     */
    PageData<UserInfoVo> getAllOutUserListPage(Integer pageNum, Integer pageSize, GetAllUserListForm form);
    /**
     * 获取当前系统的所有用户数量
     */
    Integer getAllPeopleCount();
    /**
     * 根据组织ids获取组织下的离职用户列表
     */
    PageData<UserInfoVo> listgroupOrgOutUserList(GroupOrgUserListForm form);
    /**
     * 根据组织ids获取组织下的用户列表
     */
    PageData<UserInfoVo> listgroupOrgOutUserListPage(Integer pageNum, Integer pageSize, GroupOrgUserListForm form);
}

package com.cloud.apis.organization.pojo.form;

import io.swagger.models.auth.In;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/1
 */
@Data
public class OrganizationIdPageForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer pageNum;
    @NotNull(message = "组织id不能为空")
    @Range(min = 1, message = "组织id不能小于1")
    private Long organizationId;
    private Long currentId;
}

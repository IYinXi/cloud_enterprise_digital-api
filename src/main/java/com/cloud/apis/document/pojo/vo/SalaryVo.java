package com.cloud.apis.document.pojo.vo;

import com.cloud.apis.user.pojo.vo.UserInfoVo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/4
 */
@Data
public class SalaryVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private Long id;

    /**
     * 员工id
     */
    private Long userId;
    /**
     * 员工
     */
    private UserInfoVo user;
    /**
     * 基础工资
     */
    private Double basicSalary;
    /**
     * 绩效工资
     */
    private Double performance;
    /**
     * 保险缴纳
     */
    private Double insurances;
    /**
     * 基金缴纳
     */
    private Double fund;
    /**
     * 奖金
     */
    private Double reward;
    /**
     * 奖金理由
     */
    private String rewardReason;
    /**
     * 扣款
     */
    private Double charge;
    /**
     * 扣款理由
     */
    private String chargeReason;
    /**
     * 实发工资
     */
    private Double actual;
    /**
     * 发放日期
     */
    private Date createTime;
}

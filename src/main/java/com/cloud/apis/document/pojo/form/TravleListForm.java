package com.cloud.apis.document.pojo.form;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/11
 */
@Data
public class TravleListForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long userId;
    private Integer pageNum;
}

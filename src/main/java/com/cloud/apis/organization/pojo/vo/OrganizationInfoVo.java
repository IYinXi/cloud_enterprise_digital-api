package com.cloud.apis.organization.pojo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.cloud.apis.core.model.Dict;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 组织信息
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/26
 */
@Data
public class OrganizationInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 组织架构id
     */
    private Long id;

    /**
     * 父级id
     */
    private Long parentId;

    /**
     * 组织名称
     */
    private String name;

    /**
     * 简称
     */
    private String abbreviation;

    /**
     * 是否启用(0,否;1,是)
     */
    private Boolean isEnable;
    /**
     * 组织类型id
     */
    private Long typeId;

    /**
     * 创建日期
     */
    private Date createTime;

    /**
     * 修改日期
     */
    private Date modifiedTime;
    /**
     * 父组织
     */
    private OrganizationInfoVo parent;
    /**
     * 组织类型
     */
    private Dict type;
}

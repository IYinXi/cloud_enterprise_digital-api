package com.cloud.apis.user.db.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.apis.core.model.User;
import com.cloud.apis.user.pojo.form.*;
import com.cloud.apis.user.pojo.vo.DimissiomInfoVo;
import com.cloud.apis.user.pojo.vo.UserInfoVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 用户相关DAO层
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/20
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    /**
     * 登录
     */
    HashMap login(HashMap param);
    /**
     * 根据用户id查询用户信息
     */
    UserInfoVo selectUserById(Long userId);
    /**
     * 插入用户角色信息
     */
    int insertUserRole(Long userId, Long[] roleIds);
    /**
     * 根据账户信息查询用户信息
     */
    User selectUserByAccount(String account);
    /**
     * 根据用户id查询用户账户
     */
    String selectAccountByUserId(Long userId);
    /**
     * 插入用户信息
     */
    int insertUser(@Param("user") User user,@Param("secret") String secret);
    /**
     * 根据用户id查询角色id
     */
    ArrayList<Long> searchUserRole(Long userId);
    /**
     * 根据用户id和角色Id查询是否存在
     */
    boolean isExistRole(Long userId, Long roleId);
    /**
     * 根据角色id和组织id查询用户
     */
    UserInfoVo searchUserByRoleIdAndAdminId(Long roleId, Long adminId);
    /**
     * 根据用户id删除用户信息
     */
    int deleteUserById(Long userId);
    /**
     * 根据用户id查询用户密码
     */
    Boolean isUserIdAndPassWord(Long id, String password,String secret);

    /**
     *修改密码
     */
    Boolean updateUserPassword(Long id, String password, String secret);
    /**
     * 修改用户为登记状态
     */
    Integer updateRegistration(Long id, Boolean registration);
    /**
     * 根据用户id修改用户介绍
     */
    Boolean updateUserDescription(Long id, String description);

    /**
     * 根据用户id修改用户座右铭
     */
    Boolean updateUserMotto(Long id, String motto);

    /**
     * 修改用户账户信息
     */
    Boolean updateUserAccount(Long id, String row,String account,String password,String cur);

    /**
     * 根据用户id修改用户证件照
     */
    Integer updateUserAvater(Long id, String url);

    /**
     * 根据用户id查询用户邮箱
     */
    String selectEmailByUserId(Long id);

    /**
     * 查询用户列表
     */
    List<UserInfoVo> list(Long userId);

    /**
     * 修改用户信息
     */
    int updateUserInfo(Long id, UpdateUserInfoForm form);
    /**
     * 根据用户名查询用户
     */
    List<UserInfoVo> searchByName(SearchUserForm form);
    /**
     * 根据组织ids获取组织下的用户列表
     */
    List<UserInfoVo> listgroupOrgUserListPage(Long[] ids,Long userId);
    /**
     * 根据组织ids获取组织下的用户列表
     */
    List<UserInfoVo> searchInOrganizationidsPage(Long[] ids, String searchName, Long userId);
    /**
     * 根据岗位id查询用户
     */
    ArrayList<UserInfoVo> selectUSerByPostId(Long postId);
    /**
     * 查询所有用户列表
     */
    List<UserInfoVo> getAllUserListPage(Long userId);
    /**
     * 修改用户组织岗位信息
     */
    int updateUserOrgAndPost(UpdateUserOrgAndPostForm form);

    /**
     * 根据组织id获取其下的所有用户
     */
    ArrayList<UserInfoVo> getUserByOrganizationId(Long organizationId);
    /**
     * 创建一条离职记录
     */
    int setDimission(DimissiomForm form);
    /**
     * 根据id查询离职记录
     */
    DimissiomInfoVo selectDimissionInfoById(Long id);
    /**
     * 根据id删除离职记录
     */
    int updateUserDimission(Long id);
    /**
     * 根据申请id查询离职用户
     */
    UserInfoVo selectUserIsDimission(Long applyId);
    /**
     * 查询通讯录列表
     */
    ArrayList<HashMap> searchUserContactList(ArrayList<Long> userIds);
    /**
     * 根据组织id查询用户
     */
    ArrayList<Long> selectUserByOrganizationId(Long organizationId);
    /**
     * 查询所有离职用户
     */
    List<UserInfoVo> getAllOutUserListPage(Long id);
    /**
     * 查询所有用户数量
     */
    Integer getAllPeopleCount();
    /**
     * 根据组织id查询离职用户
     */
    List<UserInfoVo> listgroupOrgOutUserListPage(Long[] ids,Long userId);
}

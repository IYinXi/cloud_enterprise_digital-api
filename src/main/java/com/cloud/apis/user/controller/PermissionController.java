package com.cloud.apis.user.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.model.Permission;
import com.cloud.apis.core.model.Role;
import com.cloud.apis.core.web.R;
import com.cloud.apis.user.pojo.form.*;
import com.cloud.apis.user.service.IPermissionService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 权限相关API
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@RestController
@RequestMapping("/permission")
@Api(tags = "权限相关API接口")
@Slf4j
public class PermissionController {
    @Autowired
    private IPermissionService permissionService;
    public PermissionController() {
        log.info("【RoleController】");
    }
    @PostMapping("")
    @ApiOperation(value = "分页查询权限列表")
    @ApiOperationSupport(order = 1)
    @SaCheckLogin
    public R getPermissionList(@Valid @RequestBody PermissionListForm form) {
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<Permission> pageData = permissionService.listPermission(form);
        return R.ok().put("data", pageData);
    }
    @PostMapping("/add")
    @ApiOperation(value = "添加权限")
    @ApiOperationSupport(order = 2)
    @SaCheckLogin
    public R addPermission(@Valid @RequestBody AddPermissionForm form) {
        form.setEnable(true);
        return R.ok("创建成功").put("data", permissionService.addPermission(form));
    }

    @PostMapping("/info")
    @ApiOperation(value = "根据权限id查询权限信息")
    @ApiOperationSupport(order = 3)
    @SaCheckLogin
    public R searchPermissionById(@Valid @RequestBody PermissionIdForm form) {
        return R.ok().put("permission", permissionService.searchPermissionById(form.getId()));
    }

    @PutMapping("/update")
    @ApiOperation(value = "修改权限")
    @ApiOperationSupport(order = 4)
    @SaCheckLogin
    public R updatePermission(@Valid @RequestBody UpdatePermissionForm form) {
        form.setEnable(true);
        permissionService.updatePermission(form);
        return R.ok("修改成功");
    }

    @PutMapping("/enable")
    @ApiOperation(value = "启用或禁用权限")
    @ApiOperationSupport(order = 5)
    @SaCheckLogin
    public R setEnable(@Valid @RequestBody EnablePermissionForm form) {
        permissionService.setPermissionEnable(form);
        return R.ok("修改成功");
    }

    @PostMapping("/search")
    @ApiOperation("根据权限名称查询权限数据")
    @ApiOperationSupport(order = 6)
    @SaCheckLogin
    public R searchByName(@Valid @RequestBody SearchPermissionForm form) {
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<Permission> pageData = permissionService.listSearchResult(form);
        return R.ok().put("data",pageData);
    }

    @PostMapping("/allList")
    @ApiOperation("查询当前角色未拥有的权限数据")
    @ApiOperationSupport(order = 7)
    @SaCheckLogin
    public R listAllPermission(@Valid @RequestBody RoleIdForm form) {
        return R.ok().put("data", permissionService.listAllPermission(form));
    }

}

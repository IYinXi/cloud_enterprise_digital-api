package com.cloud.apis.organization.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.cloud.apis.core.web.R;
import com.cloud.apis.organization.pojo.form.*;
import com.cloud.apis.organization.pojo.vo.OrganizationTreeVo;
import com.cloud.apis.organization.service.IOrganizationService;
import com.cloud.apis.user.pojo.form.UserIdForm;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 组织架构API相关接口控制器
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/25
 */
@RestController
@RequestMapping("/organization")
@Api(tags = "组织架构相关API接口")
@Slf4j
public class OrganizationController {
    @Autowired
    private IOrganizationService organizationService;

    public OrganizationController() {
        log.info("【OrganizationController】");
    }

    @PostMapping("/info")
    @ApiOperation(value = "根据组织Id获取组织信息")
    @ApiOperationSupport(order = 1)
    @SaCheckLogin
    public R getOrganizationById(@RequestBody OrganizationTreeIdForm form) {
        return R.ok().put("data",organizationService.getOrganizationById(form.getId()));
    }
    @PostMapping("/type")
    @ApiOperation(value = "根据组织类型获取组织信息")
    @ApiOperationSupport(order = 2)
    @SaCheckLogin
    public R getOrganizationByType(@Valid @RequestBody OrganizationTypeForm form) {
        return R.ok().put("data",organizationService.getOrganizationByType(form.getType()));
    }

    @PostMapping("/parent")
    @ApiOperation(value = "根据父级组织Id获取其子级组织信息")
    @ApiOperationSupport(order = 3)
    @SaCheckLogin
    public R getOrganizationByParentId(@RequestBody OrganizationParentForm form) {
        return R.ok().put("data",organizationService.getOrganizationByParentId(form.getParentId()));
    }

    @PostMapping("/struct")
    @ApiOperation(value = "获取当前完整组织架构树")
    @ApiOperationSupport(order = 4)
    @SaCheckLogin
    public R getOrganizationStruct() {
        return R.ok().put("data",organizationService.getAllOrganizationTree());
    }

    @PostMapping("/tree")
    @ApiOperation(value = "根据组织id获取其下的组织树")
    @ApiOperationSupport(order = 5)
    @SaCheckLogin
    public R getOrganizationTreeById(@Valid @RequestBody OrganizationTreeIdForm form) {
        return R.ok().put("data",organizationService.getOrganizationTreeById(form.getId()));
    }
    @PutMapping("/update")
    @ApiOperation(value = "根据组织id更新组织的信息")
    @ApiOperationSupport(order = 6)
    @SaCheckLogin
    public R updateOrganization(@Valid @RequestBody UpdateOrganizationForm form) {
        organizationService.updateOrganization(form);
        return R.ok("更新成功");
    }
    @PostMapping("/add")
    @ApiOperation(value = "添加组织")
    @ApiOperationSupport(order = 7)
    @SaCheckLogin
    public R addOrganization(@Valid @RequestBody AddOrganizationForm form) {
        return R.ok("添加成功").put("data",organizationService.addOrganization(form));
    }

    @DeleteMapping("/delete")
    @ApiOperation(value = "根据组织id删除组织")
    @ApiOperationSupport(order = 8)
    @SaCheckLogin
    public R deleteOrganization(@Valid @RequestBody DeleteOrganizationForm form) {
        organizationService.deleteOrganization(form.getId());
        return R.ok("删除成功");
    }

    @PostMapping("/groupOrganization")
    @ApiOperation(value = "获取所有分组的组织")
    @ApiOperationSupport(order = 9)
    @SaCheckLogin
    public R listGroupOrganization() {
        return R.ok().put("data",organizationService.listGroupOrganization());
    }

    @PostMapping("/listExpectCurIds")
    @ApiOperation(value = "获取除了当前用户id的组织列表")
    @ApiOperationSupport(order = 10)
    @SaCheckLogin
    public R listExpectCurIds(@Valid @RequestBody NoUserIdForm form) {
        if(form.getId()==null){
            form.setId(Long.parseLong((String) StpUtil.getLoginId()));
        }
        return R.ok().put("data",organizationService.listExpectCurIds(form.getId()));
    }

    @PostMapping("/allList")
    @ApiOperation(value = "获取当前所有组织列表")
    @ApiOperationSupport(order = 11)
    @SaCheckLogin
    public R getAllOrganizationList() {
        return R.ok().put("data",organizationService.getOrganizationList());
    }

}

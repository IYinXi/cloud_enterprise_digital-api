package com.cloud.apis.user.db.dao;


import com.cloud.apis.core.model.Permission;
import com.cloud.apis.user.pojo.form.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限相关DAO层
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/26
 */
@Mapper
public interface PermissionMapper {
    /**
     * 根据权限id查询权限代码
     */
    ArrayList<String> searchUserPermission(ArrayList<Long> permissionList);
    /**
     * 查询所有权限
     */
    List<Permission> getPermissionlist();
    /**
     * 根据权限id查询权限信息
     */
    Permission selectPermissionByCode(String code);
    /**
     * 添加权限
     */
    int insertPermission(AddPermissionForm form);
    /**
     * 根据权限id查询权限信息
     */
    Permission selectPermissionById(Long id);
    /**
     * 修改权限
     */
    int updatePermission(UpdatePermissionForm form);
    /**
     * 启用或禁用权限
     */
    int setEnablePermission(EnablePermissionForm form);
    /**
     * 根据权限名称查询权限信息
     */
    List<Permission> searchByName(SearchPermissionForm form);
    /**
     * 根据角色id查询未拥有的权限数据
     */
    List<Permission> selectPermissionsExceptByRoleId(RoleIdForm form);
}

package com.cloud.apis.work.pojo.form;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/4
 */
@Data
public class WorkDailyForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long userId;
    private String content;
}

package com.cloud.apis.organization.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class SearchPostForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "岗位名称不能为空")
    private String name;
    @NotNull(message = "页码不能为空")
    private Integer pageNum;
    @NotNull(message = "组织id不能为空")
    @Range(min = 1, message = "请提交有效的id")
    private Long organizationId;
}

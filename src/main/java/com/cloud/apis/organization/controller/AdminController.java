package com.cloud.apis.organization.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.cloud.apis.core.model.Dict;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.web.R;
import com.cloud.apis.organization.pojo.form.AdminIdForm;
import com.cloud.apis.organization.pojo.form.AssignForm;
import com.cloud.apis.organization.pojo.form.CancleAssignForm;
import com.cloud.apis.organization.pojo.form.OrganizationIdPageForm;
import com.cloud.apis.organization.service.IAdminService;
import com.cloud.apis.user.pojo.vo.UserInfoVo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Locale;

/**
 * 指派组织相关API
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/29
 */
@RestController
@RequestMapping("/admin")
@Api(tags = "指派相关API接口")
@Slf4j
public class AdminController {
    @Autowired
    private IAdminService adminService;
    public AdminController() {
        log.info("【AdminController】");
    }
    @PostMapping("/getAdminOrganizations")
    @ApiOperation(value = "根据管理者id获取管理的组织列表")
    @ApiOperationSupport(order = 1)
    @SaCheckLogin
    public R getAdminOrganizations(@Valid @RequestBody AdminIdForm form) {
        return R.ok().put("organizations",adminService.getAdminOrganizations(form.getId()));
    }

    @PostMapping("/groupOrganization")
    @ApiOperation(value = "获取可选分组的组织")
    @ApiOperationSupport(order = 2)
    @SaCheckLogin
    public R listGroupOrganization(@Valid @RequestBody AdminIdForm form) {
        return R.ok().put("data",adminService.listGroupOrganization(form));
    }
    @PostMapping("/assign")
    @ApiOperation(value = "指派组织")
    @ApiOperationSupport(order = 3)
    @SaCheckLogin
    public R assign(@Valid @RequestBody AssignForm form) {
        return R.ok("指派成功").put("data",adminService.assign(form));
    }
    @PostMapping("/cancleAssign")
    @ApiOperation(value = "取消指派组织")
    @ApiOperationSupport(order = 4)
    @SaCheckLogin
    public R cancleAssign(@Valid @RequestBody CancleAssignForm form) {
        adminService.cancleAssign(form);
        return R.ok("取消指派成功");
    }

    @PostMapping("/getAdmins")
    @ApiOperation(value = "根据组织id获取其管理者")
    @ApiOperationSupport(order = 5)
    @SaCheckLogin
    public R getAdmins(@Valid @RequestBody AdminIdForm form) {
        return R.ok().put("data",adminService.getAdmins(form.getId()));
    }

    @PostMapping("/organizations")
    @ApiOperation(value = "根据组织id分页获取其管理者")
    @ApiOperationSupport(order = 6)
    @SaCheckLogin
    public R organizations(@Valid @RequestBody OrganizationIdPageForm form) {
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        form.setCurrentId(Long.parseLong((String) StpUtil.getLoginId()));
        PageData<UserInfoVo> pageData = adminService.organizations(form);
        return R.ok().put("data",pageData);
    }
}

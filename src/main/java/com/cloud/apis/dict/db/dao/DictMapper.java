package com.cloud.apis.dict.db.dao;

import com.cloud.apis.approval.pojo.vo.ApprovalVo;
import com.cloud.apis.core.model.Dict;
import com.cloud.apis.dict.pojo.form.AddDictDataForm;
import com.cloud.apis.dict.pojo.form.EnableDictForm;
import com.cloud.apis.dict.pojo.form.SearchDictForm;
import com.cloud.apis.dict.pojo.form.UpdateDictForm;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * 字典相关API接口DAO层接口
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/21
 */
@Mapper
public interface DictMapper {
    /**
     * 根据字典id查询字典数据
     */
    Dict selectDictById(Long id);
    /**
     * 根据类型查询数据
     */
    ArrayList<Dict> selectDictByType(String type);
    /**
     * 根据简称获取字典数据
     */
    Dict selectDictByAbbreviation(String abbreviation);
    /**
     * 根据顶层字典数据id查询其子数据
     */
    ArrayList<Dict> selectDictByRootId(Long rootId);
    /**
     * 获取顶层字典数据
     */
    ArrayList<Dict> selectDictRootList();
    /**
     * 获取审批列表
     */
    List<Dict> list(Long rootId);
    /**
     * 添加字典数据
     */
    int insertDictData(AddDictDataForm form);
    /**
     * 修改字典数据
     */
    int updateDict(UpdateDictForm form);
    /**
     * 修改字典数据状态
     */
    int updateDictEnable(EnableDictForm form);
    /**
     * 根据名称查询字典数据
     */
    List<Dict> searchByName(SearchDictForm form);
}

package com.cloud.apis.attendance.service;

import com.cloud.apis.attendance.pojo.form.ResetPositionForm;
import com.cloud.apis.attendance.pojo.form.SpecialAddForm;
import com.cloud.apis.attendance.pojo.form.SpecialListForm;
import com.cloud.apis.attendance.pojo.form.SysForm;
import com.cloud.apis.attendance.pojo.vo.AddressVo;
import com.cloud.apis.attendance.pojo.vo.SpecialVo;
import com.cloud.apis.attendance.pojo.vo.SysVo;
import com.cloud.apis.core.model.PageData;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/7
 */
public interface IAttendanceService {
    Boolean isCheckin(String curDate,Long userId);

    void checkin(HashMap map);

    void createFaceModel(Long userId, String path);

    PageData<SpecialVo> specialList(SpecialListForm form);
    PageData<SpecialVo> specialListPage(Integer pageNum, Integer pageSize,SpecialListForm form);

    SpecialVo specialAdd(SpecialAddForm form);

    void deleteSpecialAttendance(Long id);

    ArrayList<SysVo> sys(Long[] ids);

    void reset(Long[] ids);

    void setSys(SysForm form);

    AddressVo getPosition();

    void resetPosition(ResetPositionForm form);

    ArrayList<HashMap> searchMonthCheckin(Long userId, Integer year, Integer month);

    ArrayList<HashMap> searchWeekCheckin(HashMap param);

    HashMap searchTodayCheckin(Long userId);

    long searchCheckinDays(Long userId);

    ArrayList<HashMap> weekCheckin(Long userId);
}

package com.cloud.apis.approval.db.dao;

import com.cloud.apis.approval.pojo.form.ApplyApprovalForm;
import com.cloud.apis.approval.pojo.form.ApprovalIdForm;
import com.cloud.apis.approval.pojo.form.ApprovalResultForm;
import com.cloud.apis.approval.pojo.form.SetCutForm;
import com.cloud.apis.approval.pojo.vo.ApprovalInfoVo;
import com.cloud.apis.approval.pojo.vo.ApprovalVo;
import com.cloud.apis.core.model.Approval;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * 审批模块DAO层接口
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/6
 */
@Mapper
public interface ApprovalMapper {
    /**
     *根据申请者id和审批类型查询审批记录
     */
    ArrayList<ApprovalInfoVo> searchApprovalByApplyIdAndApprovalType(Long applyId, Long approvalType);

    /**
     * 提交审批给对应的审批人员
     */
    int applyApproval(@Param("form") ApplyApprovalForm form, @Param("approvalId") Long approvalId);

    /**
     * 根据审批id查询审批信息
     */
    ApprovalInfoVo selectApprovalById(Long approvalId);

    /**
     * 根据审批类型和审批状态分页查询审批列表
     */
    List<ApprovalVo> list(String approvalType, Integer approvalStatus);

    /**
     * 审批结果
     */
    int updateByIdAndApprovalStatus(ApprovalResultForm form);

    /**
     * 设置撤销状态
     */
    int updateCutById(SetCutForm form);
    /**
     * 根据审批id分页查询审批列表
     */
    List<ApprovalVo> listPageByApprovalId(ApprovalIdForm form);
    /**
     * 批量删除审批记录
     */
    int deleteApprovalByApprovalId(ArrayList<Long> ids);
}

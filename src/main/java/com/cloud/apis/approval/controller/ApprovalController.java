package com.cloud.apis.approval.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.cloud.apis.approval.pojo.form.*;
import com.cloud.apis.approval.pojo.vo.ApprovalInfoVo;
import com.cloud.apis.approval.pojo.vo.ApprovalVo;
import com.cloud.apis.approval.service.IApprovalService;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.web.R;
import com.cloud.apis.user.pojo.vo.UserInfoVo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 审批模块控制器
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/6
 */
@RestController
@Slf4j
@RequestMapping("/approval")
@Api(tags = "审批相关API接口")
public class ApprovalController {
    @Autowired
    private IApprovalService approvalService;
    public ApprovalController(){
        log.info("【ApprovalController】");
    }
    @PostMapping("/apply_approval")
    @ApiOperation("提交审批")
    @ApiOperationSupport(order = 1)
    @SaCheckLogin
    public R applyApproval(@Valid @RequestBody ApplyApprovalForm form) {
        //获取申请者id
        Long applyId = Long.parseLong((String) StpUtil.getLoginId());
        form.setApplyId(applyId);
        ApprovalInfoVo approvalInfoVo = approvalService.applyApprovalService(form);
        return R.ok().put("approvalInfo", approvalInfoVo);
    }
    @PostMapping("/type")
    @ApiOperation("根据审批类型分页查询审核列表")
    @ApiOperationSupport(order = 2)
    @SaCheckLogin
    public R getApprovalList(@Valid @RequestBody ApprovalListForm form){
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<ApprovalVo> pageData = approvalService.listApproval(form);
        return R.ok().put("data",pageData);
    }
    @PostMapping("")
    @ApiOperation("根据审批者id分页查询审核列表")
    @ApiOperationSupport(order = 3)
    @SaCheckLogin
    public R getApprovalListByApprovalId(@Valid @RequestBody ApprovalIdForm form){
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<ApprovalVo> pageData = approvalService.getListByApprovalId(form);
        return R.ok().put("data",pageData);
    }
    @PostMapping("/cur_approval")
    @ApiOperation("当前审批状态")
    @ApiOperationSupport(order = 4)
    @SaCheckLogin
    public R curApproval(@Valid @RequestBody IsApprovalForm form) {
        Long applyId = Long.parseLong((String) StpUtil.getLoginId());
        return R.ok().put("curApproval", approvalService.searchApprovalByApplyIdAndApprovalType(applyId, form.getApprovalType()));
    }

    @PostMapping("/set_cut")
    @ApiOperation("设置撤销状态")
    @ApiOperationSupport(order = 5)
    @SaCheckLogin
    public R setCut(@Valid @RequestBody SetCutForm form){
        approvalService.updateCut(form);
        return R.ok("撤销成功");
    }

    @PutMapping("/operation")
    @ApiOperation("审批操作")
    @ApiOperationSupport(order = 6)
    @SaCheckLogin
    public R result(@Valid @RequestBody ApprovalResultForm form){
        try {
            approvalService.updateResult(form);
        }catch (IllegalAccessException e){
            return R.error(e.getMessage());
        }
        return R.ok();
    }

}

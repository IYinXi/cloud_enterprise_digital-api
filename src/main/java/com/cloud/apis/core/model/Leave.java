package com.cloud.apis.core.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 请假记录表
 * document_leave
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("document_leave")
public class Leave implements Serializable {
    /**
     * 请假记录id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 请假类型
     */
    @TableField("type_id")
    private Long typeId;

    /**
     * 请假说明
     */
    private String reason;

    /**
     * 材料证明
     */
    private String resources;

    /**
     * 请假开始时间
     */
    @TableField("start_time")
    private LocalDateTime startTime;

    /**
     * 请假结束时间
     */
    @TableField("end_time")
    private LocalDateTime endTime;

    /**
     * 是否启用
     */
    @TableField("is_enable")
    private Boolean isEnable;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}
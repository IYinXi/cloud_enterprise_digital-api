package com.cloud.apis.approval.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 审批表单
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/6
 */
@Data
public class IsApprovalForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "审批类型不能为空")
    private String approvalType;
}

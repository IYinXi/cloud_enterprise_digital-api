package com.cloud.apis.attendance.pojo.vo;

import com.cloud.apis.user.pojo.vo.UserInfoVo;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/8
 */
@Data
public class CheckinVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long userId;
    private UserInfoVo user;
    private String nation;
    private String province;
    private String city;
    private String district;
    private String street;
    private String streetNumber;
    private Byte status;
    private String data;
    private String createTime;
}

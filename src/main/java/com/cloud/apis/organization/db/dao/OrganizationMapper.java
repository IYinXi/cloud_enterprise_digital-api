package com.cloud.apis.organization.db.dao;

import com.cloud.apis.core.model.Organization;
import com.cloud.apis.organization.pojo.form.AddOrganizationForm;
import com.cloud.apis.organization.pojo.form.UpdateOrganizationForm;
import com.cloud.apis.organization.pojo.vo.OrganizationInfoVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;

/**
 * 组织相关DAO层
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/21
 */
@Mapper
public interface OrganizationMapper {
    /**
     * 根据组织id查询组织Organization
     */
    OrganizationInfoVo selectOrganizationById(Long id);
    /**
     * 根据组织id获取组织详细信息OrganizationInfoVo
     */
    OrganizationInfoVo getOrganizationById(Long id);
    /**
     * 根据组织类型id获取相关组织列表
     */
    ArrayList<Organization> selectOrganizationByTypeId(Long id);
    /**
     * 根据父级组织id获取子级组织列表
     */
    ArrayList<Organization> selectOrganizationByParentId(Long id);

    /**
     * 获取所有组织列表
     */
    ArrayList<Organization> getOrganizationList();

    /**
     * 根据组织简称获取组织信息
     */
    OrganizationInfoVo getOrganizationByAbbreviation(String abbreviation);

    /**
     * 更新组织信息
     */
    int updateOrganization(UpdateOrganizationForm form);

    /**
     * 添加组织
     */
    int insertOrganization(AddOrganizationForm form);
    /**
     * 删除组织
     */
    int deleteOrganization(Long id);
    /**
     * 获取除了用户id所在组织的其他所有组织信息
     */
    ArrayList<OrganizationInfoVo> listExpectCurIds(Long id);
    /**
     * 获取所有组织信息
     */
    ArrayList<OrganizationInfoVo> getOrganizationInfoList();

}

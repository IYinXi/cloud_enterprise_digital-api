package com.cloud.apis.attendance.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/13
 */
@Data
public class SysVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String paramKey;
    private String paramValue;
    private Boolean isEnable;
    private String remark;
    private String createTime;
}

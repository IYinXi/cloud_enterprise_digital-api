package com.cloud.apis.core.config;

import com.github.xiaoymin.knife4j.spring.extension.OpenApiExtensionResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * knife4j配置类
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/20
 */
@Slf4j
@Configuration
@EnableSwagger2WebMvc
public class Knife4jConfig {

    /**
     * 指定Controller包路径
     */
    private String basePackage = "com.cloud.apis";

    /**
     * 主机名
     */
    private String host = "https://localhost:8080";

    /**
     * 标题
     */
    private String title = "云端智企后端接口文档";

    /**
     * 简介
     */
    private String description = "关于云端智企项目的后端API接口文档";

    /**
     * 服务条款URL
     */
    private String termsOfServiceUrl = "http://www.apache.org/licenses/LICENSE-2.0";

    /**
     * 联系人
     */
    private String contactName = "朱垠熹";

    /**
     * 联系网址
     */
    private String contactUrl = "无";

    /**
     * 联系邮箱
     */
    private String contactEmail = "2148675886@qq.com";

    /**
     * 版本号
     */
    private String version = "1.0";

    /**
     * 通过解析器来获取OpenAPI扩展信息
     */
    @Autowired
    private OpenApiExtensionResolver openApiExtensionResolver;

    public Knife4jConfig() {
        log.info("【Knife4jConfig】");
    }

    /**
     * 配置并创建一个Docket Bean，用于生成Swagger文档
     * @return Docket
     */
    @Bean
    public Docket docket() {
        String groupName = "1.0.0";
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .host(host)
                .apiInfo(apiInfo())
                .groupName(groupName)
                .select()
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(PathSelectors.any())
                .build()
                .extensions(openApiExtensionResolver.buildExtensions(groupName));
        return docket;
    }

    /**
     * 构建一个ApiInfo对象，该对象包含了API的标题、描述、服务条款网址、联系人信息和版本号
     * @return ApiInfo
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(title)
                .description(description)
                .termsOfServiceUrl(termsOfServiceUrl)
                .contact(new Contact(contactName, contactUrl, contactEmail))
                .version(version)
                .build();
    }
}

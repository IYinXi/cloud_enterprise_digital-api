package com.cloud.apis.approval.pojo.vo;

import com.cloud.apis.core.model.Dict;
import com.cloud.apis.user.pojo.vo.UserInfoVo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/22
 */
@Data
public class ApprovalVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 申请者信息
     */
    private UserInfoVo applyInfoVo;
    /**
     * 审批id
     */
    private Long id;

    /**
     * 申请者id
     */
    private Long applyId;

    /**
     * 审批者id
     */
    private Long approvalId;

    /**
     * 审批类型id
     */
    private Long approvalType;

    /**
     * 审批类型
     */
    private Dict approvalDict;

    /**
     * 相关附件路径
     */
    private String certificate;

    /**
     * 审批说明
     */
    private String approvalReason;
    /**
     * 申请说明
     */
    private String applyReason;
    /**
     * 审批状态（0,已提交;1,待审批;2,通过;3,未通过;4,已完成）
     */
    private Byte approvalStatus;

    /**
     * 是否撤销（0,否;1,是)
     */
    private Boolean isCut;

    /**
     * 申请日期
     */
    private Date applyTime;

    /**
     * 审批日期
     */
    private Date approvalTime;
}

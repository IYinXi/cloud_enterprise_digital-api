package com.cloud.apis.core.web;

import java.util.HashMap;
import java.util.Map;

/**
 * 统一响应类型
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/24
 */
public class R extends HashMap<String, Object> {

    /**
     * 构造默认R对象
     */
    public R() {
        // 默认状态码20000
        put("code", ServiceCode.OK.getValue());
        // 默认消息
        put("msg", "success");
    }

    /**
     * 添加数据
     */
    public R put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    /**
     * 创建一个OK的R对象
     */
    public static R ok() {
        return new R();
    }

    /**
     * 创建一个OK的R对象
     */
    public static R ok(String msg) {
        R r = new R();
        r.put("msg", msg);
        return r;
    }

    /**
     * 创建一个OK的R对象
     */
    public static R ok(Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        return r;
    }

    /**
     * 创建一个错误R对象
     */
    public static R error(Integer code, String msg) {
        R r = new R();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    /**
     * 创建一个错误R对象
     */
    public static R error(String msg) {
        return error(ServiceCode.ERROR_BAD_REQUEST.getValue(), msg);
    }

}
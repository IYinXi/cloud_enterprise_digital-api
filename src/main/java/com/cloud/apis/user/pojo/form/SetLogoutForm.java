package com.cloud.apis.user.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 指定用户id强制登出
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/26
 */
@Data
public class SetLogoutForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "用户id不能为空")
    @Range(min = 1, message = "请提交正确的用户id")
    @ApiModelProperty(value = "用户id", required = true)
    private Long id;
}

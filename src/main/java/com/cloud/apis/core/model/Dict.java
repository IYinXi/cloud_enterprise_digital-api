package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 通用数据字典
 * dict_common
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("dict_common")
public class Dict implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 通用字典id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 简称
     */
    private String abbreviation;

    /**
     * 父级id
     */
    @TableField("parent_id")
    private Long parentId;

    /**
     * 字典数据类型
     */
    private String type;

    /**
     * 字典数据说明
     */
    private String remarks;
    /**
     * 是否开启
     */
    @TableField("is_enable")
    private boolean isEnable;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField("modified_time")
    private Date modifiedTime;


}
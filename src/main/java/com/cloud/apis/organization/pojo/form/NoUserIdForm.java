package com.cloud.apis.organization.pojo.form;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/1
 */
@Data
public class NoUserIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
}

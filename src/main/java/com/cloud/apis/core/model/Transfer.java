package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 转岗记录表
 * document_transfer
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("document_transfer")
public class Transfer implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 转岗人id
     */
    @TableField("transfer_id")
    private Long transferId;

    /**
     * 原组织id
     */
    @TableField("old_organization_id")
    private Long oldOrganizationId;

    /**
     * 原岗位id
     */
    @TableField("old_post_id")
    private Long oldPostId;

    /**
     * 现组织id
     */
    @TableField("now_organization_id")
    private Long nowOrganizationId;

    /**
     * 现岗位id
     */
    @TableField("now_post_id")
    private String nowPostId;

    /**
     * 转岗理由
     */
    private String reason;
    /**
     * 其他材料
     */
    private String resources;

    /**
     * 是否可用
     */
    @TableField("is_enable")
    private Boolean isEnable;

    /**
     * 记录时间
     */
    @TableField("log_time")
    private Date logTime;

    private static final long serialVersionUID = 1L;
}
package com.cloud.apis.organization.pojo.vo;

import com.cloud.apis.core.model.Organization;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 组织架构树
 * 
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/14
 */
@Data
@Accessors(chain = true)
public class OrganizationTreeVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**组织架构id*/
    private Long value;
    /**组织架构名称*/
    private String label;
    /**子组织架构*/
    private ArrayList<OrganizationTreeVo> children;
}

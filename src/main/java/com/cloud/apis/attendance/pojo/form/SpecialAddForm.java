package com.cloud.apis.attendance.pojo.form;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/12
 */
@Data
public class SpecialAddForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String data;
    private String mark;
    private Boolean isWork;
    private Boolean isEnable;
}

package com.cloud.apis.organization.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/27
 */
@Data
public class PostListForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer pageNum;
    private Long organizationId;
}

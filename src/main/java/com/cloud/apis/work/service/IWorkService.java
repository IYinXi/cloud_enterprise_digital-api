package com.cloud.apis.work.service;

import com.cloud.apis.work.pojo.form.UpdateWorkForm;
import com.cloud.apis.work.pojo.form.WorkDailyForm;
import com.cloud.apis.work.pojo.form.WorkDateForm;
import com.cloud.apis.work.pojo.form.WorkDateIdForm;
import com.cloud.apis.work.pojo.vo.WorkDailyVo;
import com.cloud.apis.work.pojo.vo.WorkDateVo;

import java.util.HashMap;
import java.util.List;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/4
 */
public interface IWorkService {
    List<HashMap> searchWorkByPage(Long userId, long start, int length);

    WorkDailyVo setWorkDaily(WorkDailyForm form);

    WorkDailyVo searchWorkDailyDetialById(Long id);

    WorkDateVo setWorkDate(WorkDateForm form);

    WorkDateVo selectWorkDateByIdAndDate(WorkDateIdForm form);

    WorkDateVo updateWorkDate(UpdateWorkForm form);
}

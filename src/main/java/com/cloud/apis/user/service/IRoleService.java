package com.cloud.apis.user.service;

import com.cloud.apis.core.model.Dict;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.model.Permission;
import com.cloud.apis.core.model.Role;
import com.cloud.apis.dict.pojo.form.DictListForm;
import com.cloud.apis.dict.pojo.form.SearchDictForm;
import com.cloud.apis.user.pojo.form.*;

import java.util.ArrayList;

/**
 * 角色相关API接口业务层接口
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/21
 */
public interface IRoleService {
    /**
     * 根据角色id查询角色信息
     */
    Role searchRoleById(Long roleId);
    /**
     * 查询所有角色信息
     */
    ArrayList<Role> list();
    /**
     * 分页查询角色信息
     */
    PageData<Role> listRole(RoleListForm form);
    /**
     * 分页查询角色列表
     */
    PageData<Role> listPage(Integer pageNum, Integer pageSize);
    /**
     * 添加角色
     */
    Role addRole(AddRoleForm form);
    /**
     * 修改角色
     */
    void updateRole(UpdateRoleForm form);
    /**
     * 角色是否启用
     */
    void setEnable(EnableRoleForm form);
    /**
     * 根据角色名称查询角色信息列表
     */
    PageData<Role> searchByName(Integer pageNum, Integer pageSize, SearchRoleForm form);
    /**
     * 分页获取搜索列表
     */
    PageData<Role> listSearchResult(SearchRoleForm form);
    /**
     * 根据角色id分页查询其拥有的权限
     */
    PageData<Permission> listPermissions(RolePermissionPageForm form);
    /**
     * 分页根据角色id分页查询其拥有的权限列表
     */
    PageData<Permission> listRolePermission(Integer pageNum, Integer pageSize, RolePermissionPageForm form);
    /**
     * 添加角色权限
     */
    void addPermissions(RolePermissionForm form);
    /**
     * 删除角色-权限记录
     */
    void deleteRolePermission(RolePermissionIdForm form);

    /**
     *分页根据权限名称搜索该角色下的对应权限
     */
    PageData<Permission> searchByRolePermissionName(Integer pageNum, Integer pageSize,RolePermissionSearchForm form);
    /**
     * 根据权限名称搜索该角色下的对应权限
     */
    PageData<Permission> searchRolePermission(RolePermissionSearchForm form);
    /**
     * 根据用户id查询其未拥有的角色
     */
    ArrayList<Role> searchOtherRoles(Long id);

    /**
     *为指定用户添加一组角色
     */
    void setRoles(UserRoleForm form);
    /**
     * 根据用户id查询其拥有的角色
     */
    ArrayList<Role> getListRoles(Long id);
    /**
     * 根据用户id删除其拥有的角色
     */
    void deleteRoleUsers(UserRoleForm form);
}

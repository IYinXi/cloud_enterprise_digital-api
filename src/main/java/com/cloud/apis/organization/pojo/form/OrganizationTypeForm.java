package com.cloud.apis.organization.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 组织类型表单
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/25
 */
@Data
public class OrganizationTypeForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "组织类型不能为空")
    @ApiModelProperty(value = "组织类型", required = true)
    String type;
}

package com.cloud.apis.user.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 启用或禁用权限
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class EnablePermissionForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "权限id不能为空")
    private Long id;
    private boolean isEnable;
}

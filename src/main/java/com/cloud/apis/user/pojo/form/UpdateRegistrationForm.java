package com.cloud.apis.user.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/30
 */
@Data
public class UpdateRegistrationForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private boolean status;
}

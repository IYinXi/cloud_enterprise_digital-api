package com.cloud.apis.approval.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/22
 */
@Data
public class ApprovalListForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer pageNum;
    private String approvalType;
    private Integer approvalStatus;
}

package com.cloud.apis.user.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.cloud.apis.core.model.Dict;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.core.model.Permission;
import com.cloud.apis.core.model.Role;
import com.cloud.apis.core.web.R;
import com.cloud.apis.dict.pojo.form.SearchDictForm;
import com.cloud.apis.user.pojo.form.*;
import com.cloud.apis.user.service.IRoleService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 *  角色相关API接口
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/21
 */
@RestController
@RequestMapping("/role")
@Api(tags = "角色相关API接口")
@Slf4j
public class RoleController {
    @Autowired
    private IRoleService roleService;
    public RoleController() {
        log.info("【RoleController】");
    }

    @PostMapping("/info")
    @ApiOperation(value = "根据角色id查询角色信息")
    @ApiOperationSupport(order = 1)
    @SaCheckLogin
    public R searchRoleById(@Valid @RequestBody RoleIdForm form) {
        return R.ok().put("role", roleService.searchRoleById(form.getId()));
    }

    @GetMapping("/list")
    @ApiOperation(value = "查询所有角色信息")
    @ApiOperationSupport(order = 2)
    @SaCheckLogin
    public R list() {
        return R.ok().put("roles", roleService.list());
    }

    @PostMapping("")
    @ApiOperation(value = "分页查询角色列表")
    @ApiOperationSupport(order = 3)
    @SaCheckLogin
    public R getRoleList(@Valid @RequestBody RoleListForm form) {
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<Role> pageData = roleService.listRole(form);
        return R.ok().put("data", pageData);
    }
    @PostMapping("/add")
    @ApiOperation(value = "添加角色")
    @ApiOperationSupport(order = 4)
    @SaCheckLogin
    public R addRole(@Valid @RequestBody AddRoleForm form) {
        form.setEnable(true);
        return R.ok().put("role", roleService.addRole(form));
    }

    @PutMapping("/update")
    @ApiOperation(value = "修改角色")
    @ApiOperationSupport(order = 5)
    @SaCheckLogin
    public R updateRole(@Valid @RequestBody UpdateRoleForm form) {
        form.setEnable(true);
        roleService.updateRole(form);
        return R.ok();
    }

    @PutMapping("/enable")
    @ApiOperation(value = "启用或禁用角色")
    @ApiOperationSupport(order = 6)
    @SaCheckLogin
    public R enableRole(@Valid @RequestBody EnableRoleForm form) {
        roleService.setEnable(form);
        if(form.isEnable()){
            return R.ok().put("msg", "启用成功");
        }else {
            return R.ok().put("msg", "禁用成功");
        }
    }
    @PostMapping("/search")
    @ApiOperation("根据角色名称查询数据")
    @ApiOperationSupport(order = 8)
    @SaCheckLogin
    public R searchDict(@Valid @RequestBody SearchRoleForm form){
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<Role> pageData = roleService.listSearchResult(form);
        return R.ok().put("data",pageData);
    }

    @PostMapping("/permissions")
    @ApiOperation("根据角色id分页查询其拥有的权限")
    @ApiOperationSupport(order = 9)
    @SaCheckLogin
    public R searchPermissions(@Valid @RequestBody RolePermissionPageForm form){
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<Permission> pageData = roleService.listPermissions(form);
        return R.ok().put("data",pageData);
    }
    @PostMapping("/addPermissions")
    @ApiOperation("为角色批量添加权限")
    @ApiOperationSupport(order = 10)
    @SaCheckLogin
    public R addPermissions(@Valid @RequestBody RolePermissionForm form){
        roleService.addPermissions(form);
        return R.ok();
    }

    @DeleteMapping("/delete")
    @ApiOperation("根据(角色-权限)id删除记录")
    @ApiOperationSupport(order = 11)
    @SaCheckLogin
    public R deleteRolePermission(@Valid @RequestBody RolePermissionIdForm form){
        roleService.deleteRolePermission(form);
        return R.ok();
    }

    @PostMapping("/search/permissionList")
    @ApiOperation("根据权限名称搜索该角色下的对应权限")
    @ApiOperationSupport(order = 12)
    @SaCheckLogin
    public R searchRolePermissionList(@Valid @RequestBody RolePermissionSearchForm form){
        Integer pageNum = form.getPageNum() == null ? 1 : form.getPageNum();
        form.setPageNum(pageNum);
        PageData<Permission> pageData = roleService.searchRolePermission(form);
        return R.ok().put("data",pageData);
    }
    @PostMapping("/otherRoles")
    @ApiOperation("根据用户id查询其不拥有的角色")
    @ApiOperationSupport(order = 13)
    @SaCheckLogin
    public R searchOtherRoles(@Valid @RequestBody UserIdForm form){
        return R.ok().put("data",roleService.searchOtherRoles(form.getId()));
    }

    @PostMapping("/setRoles")
    @ApiOperation("为用户添加新的角色")
    @ApiOperationSupport(order = 14)
    @SaCheckLogin
    public R setRoles(@Valid @RequestBody UserRoleForm form){
        Long id = Long.parseLong((String) StpUtil.getLoginId());
        if(id.equals(form.getId())){
            return R.error("您不能操作您自己");
        }
        roleService.setRoles(form);
        return R.ok("添加成功");
    }

    @PostMapping("/roleList")
    @ApiOperation("获取当前用户的角色列表")
    @ApiOperationSupport(order = 15)
    @SaCheckLogin
    public R roleList(@Valid @RequestBody UserIdForm form){
        return R.ok().put("data",roleService.getListRoles(form.getId()));
    }
    @DeleteMapping("/deleteUserRole")
    @ApiOperation("删除用户角色关联")
    @ApiOperationSupport(order = 16)
    @SaCheckLogin
    public R deleteUserRole(@Valid @RequestBody UserRoleForm form){
        Long id = Long.parseLong((String) StpUtil.getLoginId());
        if(id.equals(form.getId())){
            return R.error("您不能操作您自己");
        }
        roleService.deleteRoleUsers(form);
        return R.ok("删除成功");
    }
}

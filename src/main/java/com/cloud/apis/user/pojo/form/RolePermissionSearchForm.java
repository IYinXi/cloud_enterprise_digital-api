package com.cloud.apis.user.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class RolePermissionSearchForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "角色id不能为空")
    private Long roleId;
    @NotNull(message = "搜索的权限名称不能为空")
    private String name;
    private Integer pageNum;
}

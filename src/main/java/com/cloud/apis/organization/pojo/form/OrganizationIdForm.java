package com.cloud.apis.organization.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 组织id表单
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/1
 */
@Data
public class OrganizationIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "组织id不能为空")
    @ApiModelProperty(value = "组织id", required = true)
    private Long id;
}

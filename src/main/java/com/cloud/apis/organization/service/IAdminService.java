package com.cloud.apis.organization.service;

import com.cloud.apis.core.model.Admin;
import com.cloud.apis.core.model.PageData;
import com.cloud.apis.organization.pojo.form.AdminIdForm;
import com.cloud.apis.organization.pojo.form.AssignForm;
import com.cloud.apis.organization.pojo.form.CancleAssignForm;
import com.cloud.apis.organization.pojo.form.OrganizationIdPageForm;
import com.cloud.apis.organization.pojo.vo.AdminInfoVo;
import com.cloud.apis.organization.pojo.vo.GroupOrganizationVo;
import com.cloud.apis.user.pojo.vo.UserInfoVo;

import java.util.ArrayList;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/29
 */
public interface IAdminService {
    /**
     * 根据管理者id获取管理的组织列表
     */
    ArrayList<AdminInfoVo> getAdminOrganizations(Long id);

    /**
     * 获取当前管理者id未管理的组织列表
     */
    ArrayList<GroupOrganizationVo> listGroupOrganization(AdminIdForm form);
    /**
     * 指派组织
     */
    AdminInfoVo assign(AssignForm form);
    /**
     * 取消指派组织
     */
    void cancleAssign(CancleAssignForm form);
    /**
     * 根据组织id获取其管理者
     */
    ArrayList<UserInfoVo> getAdmins(Long id);

    /**
     * 根据组织id分页获取其管理者
     */
    PageData<UserInfoVo> organizations(OrganizationIdPageForm form);
    /**
     * 根据组织id分页获取其管理者
     */
    PageData<UserInfoVo> organizationsPage(Integer pageNum, Integer pageSize,OrganizationIdPageForm form);
}

package com.cloud.apis.document.pojo.form;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/3
 */
@Data
public class setSealForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long userId;
    private Long typeId;
    private String reason;
    private String resources;
}

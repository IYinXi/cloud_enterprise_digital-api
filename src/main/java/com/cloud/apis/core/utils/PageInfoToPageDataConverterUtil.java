package com.cloud.apis.core.utils;


import com.cloud.apis.core.model.PageData;
import com.github.pagehelper.PageInfo;


/**
 * 将PageInfo转换成PageData的转换器工具类
 *
 */
public class PageInfoToPageDataConverterUtil {

    /**
     * 将PageHelper框架中的PageInfo类型对象转换成自定义的PageData类型对象
     *
     */
    public synchronized static <T> PageData<T> convert(PageInfo<T> pageInfo) {
        PageData<T> pageData = new PageData<>();
        pageData.setPageSize(pageInfo.getPageSize())
                .setTotal(pageInfo.getTotal())
                .setCurrentPage(pageInfo.getPageNum())
                .setMaxPage(pageInfo.getPages())
                .setList(pageInfo.getList());
        return pageData;
    }

}

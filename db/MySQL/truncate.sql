-- 使用数据库
use cloud_enterprise_digital;
-- 重置表数据
-- 用户表
truncate table tb_user;
-- 用户-角色表
truncate table tb_user_role;
-- 角色表
truncate table tb_role;
-- 角色-权限表
truncate table tb_role_permission;
-- 权限表
truncate table tb_permission;
-- 组织架构表
truncate table tb_organization;
-- 岗位表
truncate table tb_post;
-- 审批表
truncate table tb_approval;
-- 指派表（管理表）
truncate table tb_admin ;
-- 重置记录表
-- 转岗记录表
truncate table document_transfer ;
-- 离职记录表
truncate table document_dimission ;
-- 报销记录表
truncate table document_reimbursement ;
-- 用印记录表
truncate table document_seal ;
-- 补助记录表
truncate table document_subsidy ;
-- 请假记录表
truncate table document_leave ;
-- 出差记录表
truncate table document_travle ;
-- 重置工作表
-- 工作日报表
truncate table work_daily ;
-- 工作日程表
truncate table work_date ;
-- 重置考勤表
-- 考勤签到表
truncate table attendance_checkin ;
-- 特殊考勤表
truncate table attendance_special ;
-- 重置系统参数表
-- 参数模型表
truncate table model_sys ;
-- 重置人脸模型表
truncate table model_face ;
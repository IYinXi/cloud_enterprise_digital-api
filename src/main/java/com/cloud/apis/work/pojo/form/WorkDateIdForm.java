package com.cloud.apis.work.pojo.form;

import com.cloud.apis.core.model.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/5
 */
@Data
public class WorkDateIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long userId;
    @NotNull(message = "请提交创建日期")
    private String createTime;
}

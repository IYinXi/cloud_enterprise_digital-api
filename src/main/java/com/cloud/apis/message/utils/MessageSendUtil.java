package com.cloud.apis.message.utils;

import cn.hutool.core.util.IdUtil;
import com.cloud.apis.message.model.MessageEntity;
import com.cloud.apis.message.task.MessageTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 发送消息工具类
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/24
 */
public class MessageSendUtil {
    public static void sendSimpleMessage(MessageTask messageTask,Long listenerId,String title,String content){
        MessageEntity message = new MessageEntity();
        message.setSenderId(listenerId);
        message.setSenderName(title);
        message.setUuid(IdUtil.simpleUUID());
        message.setMsg(content);
        message.setSendTime(new Date());
        messageTask.send(listenerId+"",message);
    }
}

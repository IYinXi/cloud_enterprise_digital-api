package com.cloud.apis;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 项目测试
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/20
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CloudApplicationTests {

}

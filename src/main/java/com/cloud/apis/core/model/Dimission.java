package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 离职记录表
 * document_dimission
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("document_dimission")
public class Dimission implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 离职员工id
     */
    @TableId("user_id")
    private Long userId;

    /**
     * 离职原因
     */
    private String reason;

    /**
     * 相关材料
     */
    private String resources;

    /**
     * 离职时间
     */
    @TableId("out_time")
    private Date outTime;

    private static final long serialVersionUID = 1L;
}
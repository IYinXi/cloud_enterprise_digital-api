package com.cloud.apis.dict.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 修改字典数据
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/27
 */
@Data
public class UpdateDictForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private String abbreviation;
    private String type;
    private boolean isEnable;
    private String remarks;
    private Long parentId;
}

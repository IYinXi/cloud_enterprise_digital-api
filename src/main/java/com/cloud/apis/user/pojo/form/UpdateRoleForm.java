package com.cloud.apis.user.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class UpdateRoleForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "角色id不能为空")
    private Long id;
    @NotNull(message = "角色名称不能为空")
    private String name;
    @NotNull(message = "角色简称不能为空")
    private String abbreviation;
    private boolean isEnable;

}

package com.cloud.apis.core.web;

/**
 * 业务状态码
 */
public enum ServiceCode {

    /**
     * 操作成功
     */
    OK(20000),
    /**
     * 错误：请求参数格式错误
     */
    ERROR_BAD_REQUEST(40000),
    /**
     * 错误：未认证
     */
    ERROR_UNAUTHORIZED(40100),
    /**
     * 错误：数据不存在
     */
    ERROR_NOT_FOUND(40400),
    /**
     * 错误：数据冲突
     */
    ERROR_CONFLICT(40900),
    /**
     * 错误：未知的插入数据失败
     */
    ERROR_INSERT(50000),
    /**
     * 错误：未知的修改数据失败
     */
    ERROR_UPDATE(50200),
    /**
     * 错误：当前无图片
     */
    ERROR_NOT_IMAGE(5000),
    /**
     * 错误：当前无人脸
     */
    ERROR_NO_PEOPLE(5001),
    /**
     * 错误：当前地点错误
     */
    ERROR_BAD_ADDRESS(5002);

    private Integer value;

    ServiceCode(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

}

package com.cloud.apis.document.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/5/12
 */
@Data
public class LeaveSearchForm implements Serializable{
    private static final long serialVersionUID = 1L;
    private Long userId;
    private String searchTime;
    @NotNull(message = "页码不能为空")
    private Integer pageNum;
}

package com.cloud.apis.core.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 工资记录表
 * document_salary
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("document_salary")
public class Salary implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 员工id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 基础工资
     */
    @TableField("basic_salary")
    private Double basicSalary;

    /**
     * 绩效工资
     */
    private Double performance;

    /**
     * 保险缴纳
     */
    private Double insurances;

    /**
     * 基金缴纳
     */
    private Double fund;

    /**
     * 奖金
     */
    private Double reward;

    /**
     * 奖金理由
     */
    @TableField("reward_reason")
    private String rewardReason;

    /**
     * 扣款
     */
    private Double charge;

    /**
     * 扣款理由
     */
    @TableField("charge_reason")
    private String chargeReason;

    /**
     * 实发工资
     */
    private Double actual;

    /**
     * 发放日期
     */
    @TableField("create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}
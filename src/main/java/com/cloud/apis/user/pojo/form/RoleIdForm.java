package com.cloud.apis.user.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class RoleIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "请提交正确的角色id")
    @Range(min = 1, message = "请提交正确的角色id")
    private Long id;
}

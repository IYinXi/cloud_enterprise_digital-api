package com.cloud.apis.user.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 登录表单
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/3/20
 */
@Data
public class LoginForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "账号不能为空")
    @Pattern(regexp = "^1[3-9]\\d{9}$", message = "账号格式不正确")
    @ApiModelProperty(value = "账号", required = true)
    private String account;

    @NotNull(message = "密码不能为空")
    @Pattern(regexp = "^[a-zA-Z0-9]{4,8}$", message = "密码格式不正确,位数包含6-20位，只能是数字和字母组成")
    @ApiModelProperty(value = "密码", required = true)
    private String password;
}

package com.cloud.apis.user.pojo.form;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class PermissionListForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer pageNum;
}

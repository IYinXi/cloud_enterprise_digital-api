package com.cloud.apis.user.pojo.form;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
@AllArgsConstructor
public class RolePermission implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long roleId;
    private Long permissionId;
}

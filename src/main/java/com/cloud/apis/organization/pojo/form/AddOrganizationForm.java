package com.cloud.apis.organization.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 添加新组织表单
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/26
 */
@Data
public class AddOrganizationForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    @NotNull(message = "组织名称不能为空")
    private String name;
    @NotNull(message = "组织简称不能为空")
    private String abbreviation;
    @NotNull(message = "组织类型不能为空")
    private Long typeId;
    @NotNull(message = "组织父级id不能为空")
    private Long parentId;
    @NotNull(message = "组织是否启用不能为空")
    private Boolean isEnable;
}

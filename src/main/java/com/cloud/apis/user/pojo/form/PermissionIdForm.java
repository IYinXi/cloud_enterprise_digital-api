package com.cloud.apis.user.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 权限id表单
 * @Author YinXi
 * @Versin 1.0.0
 * @Date 2024/4/28
 */
@Data
public class PermissionIdForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "权限id不能为空")
    @Range(min = 1, message = "请提交正确的权限id")
    private Long id;
}
